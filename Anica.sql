﻿CREATE TABLE configuraciones (
  id serial  primary key,
  nombre_negocio varchar(60),
  nit_negocio varchar,
  representante varchar,
  ciudad varchar,
  eslogan varchar,
  email varchar,
  pie_legal varchar,
  servicios varchar,
  contacto_negocio varchar(15),
  contacto2_negocio varchar(15),
  direccion varchar,
  ruta_backup varchar,
  titulo_factura varchar,
  nombre_impresora varchar, -- la de hoja 17*12
  tipo_impresora varchar, -- termica/ 17*12 / carta / etc
  imprimir_factura integer,
  productos_repetidos integer, -- nuevo
  utilidad_venta integer,
  utilidad_mayorista integer,
  utilidad_credito integer,
  iva integer,
  ruta_imagenes varchar

);
--alter table configuraciones add column imprimir_factura varchar


CREATE TABLE perfiles (
  id serial  primary key,
  perfil varchar(30)
);

CREATE TABLE users (
  id serial  primary key,
  nombre varchar(60) NOT NULL,
  password varchar NOT NULL,
  user_name varchar(30) NOT NULL,
  direccion varchar(80),
  telefono varchar(50),
  telefono2 varchar(50),
  sitioweb varchar(50),
  estado varchar(8),
  email varchar(50),
  id_perfil int NOT NULL,
  constraint fk_perfil foreign key (id_perfil) references perfiles (id)
);

CREATE TABLE cajas(
  id serial NOT NULL,
  total double precision,
  retiro double precision,
  saldo double precision,
  fecha_cierre date,
  hora_cierre varchar(8),
  CONSTRAINT claveprimariacaja PRIMARY KEY (id)
);


CREATE TABLE fondos(
  id serial, 
  nombre varchar(500), 
  predeterminado int,
  CONSTRAINT pk_fondos_dinero PRIMARY KEY (id)
);



CREATE TABLE contactos
(
  id serial NOT NULL, -- Código del contacto
  nombre varchar(60) NOT NULL, -- Nombre del contacto
  cedula varchar(15), -- Número de C.I. del contacto
  direccion varchar(80), -- Dirección del contacto
  ciudad varchar(80), -- Dirección del contacto
  contacto varchar(50), -- Nº de teléfono del contacto
  contacto2 varchar(50), -- Nº de celular del contacto
  descuento double precision,
  email varchar(50), -- Email del contacto
  forma_pago varchar(20), 
  cuenta varchar(20),
  tipo_cuenta varchar(20),
  numero_cuenta varchar(20),
  observaciones varchar,
  proveedor integer,
  tecnico integer,
  CONSTRAINT claveprimaria_ PRIMARY KEY (id)
);


CREATE TABLE retiros(
  id serial NOT NULL,
  fecha date,
  hora varchar(8),
  monto double precision,
  detalle varchar(400),
  id_user integer,
  CONSTRAINT claveprimariaretiros PRIMARY KEY (id),
  CONSTRAINT fk_id_user_r FOREIGN KEY (id_user) REFERENCES users (id)
);

CREATE TABLE sucursales(
  id serial NOT NULL, -- Código del sucursal
  nombre varchar(20), -- Nombre o razón social de sucursal
  telefono varchar(20), -- Contacto
  responsable varchar(20), -- Nombre del responsable en la sucursal
  direccion varchar(30), -- Dirección de la sucursal
  CONSTRAINT sucursales_pkey PRIMARY KEY (id) 
);


CREATE TABLE facturas_cabeceras(
  id serial NOT NULL, -- Código de la factura cabecera
  codigo varchar(20),
  id_contacto integer, -- Código del contacto en la factura cabecera
  id_user integer, -- Código del contacto en la factura cabecera
  total double precision, -- Monto total a pagar
  fecha date, -- Fecha de emisión de la factura
  estado char(1), -- Estado de la factura
  hora varchar(8), -- Hora de emisión de la factura
  tiva1 double precision,
  porcentaje_descuento double precision, -- Monto entregado
  monto_descuento double precision, -- Monto entregado
  tipo_factura varchar(10),
  CONSTRAINT pkfacturacabecera PRIMARY KEY (id),
  CONSTRAINT fkcontactoc FOREIGN KEY (id_contacto) REFERENCES contactos (id),
  CONSTRAINT fk_id_userc FOREIGN KEY (id_user) REFERENCES users (id)
);

CREATE TABLE productos(
  id serial NOT NULL, -- Código del producto
  codigo_barras varchar unique,
  descripcion varchar(500), -- Descripcion del producto
  precio_costo double precision, -- Precio costo del producto
  precio_venta double precision, -- Precio mayorista del producto
  servicio integer,
  CONSTRAINT clave_primaria PRIMARY KEY (id)
  );


CREATE TABLE cuentas(
  id serial, 
  nombre varchar(50), 
  CONSTRAINT pk_cuentas PRIMARY KEY (id)
);

CREATE TABLE cuentas_prestamos( -- se creo la tabla cuentas_prestamos para ser asosciada a los prestamos
  id serial, 
  nombre varchar(50), 
  CONSTRAINT pk_cuentas_prestamos PRIMARY KEY (id)
);


CREATE TABLE egresos(
  id serial, 
  id_user integer,
  id_contacto integer, 
  id_cuenta integer not NULL, 
  descripcion varchar,
  total double precision, 
  fecha date, -- Fecha de emisión del gasto
  hora varchar(8), -- hora de emisión del gasto
  caja integer, -- hora de emisión del gasto
  CONSTRAINT pk_otros_egresos PRIMARY KEY (id),
  CONSTRAINT fk_cuenta_egresos FOREIGN KEY (id_cuenta) REFERENCES   cuentas (id),
  CONSTRAINT fk_user_oegre FOREIGN KEY (id_user) REFERENCES   users (id),
  CONSTRAINT fk_contacto_oegre FOREIGN KEY (id_contacto) REFERENCES contactos (id)
);

CREATE TABLE otros_ingresos(
  id serial, 
  id_user integer,
  id_contacto integer, 
  descripcion varchar,
  total double precision, 
  fecha date, -- Fecha de emisión del gasto
  hora varchar(8), -- hora de emisión del gasto
  id_fondo integer,
  CONSTRAINT pk_otros_ingresos PRIMARY KEY (id),
  CONSTRAINT fk_user_oingre FOREIGN KEY (id_user) REFERENCES   users (id),
  CONSTRAINT fk_contacto_oingre FOREIGN KEY (id_contacto) REFERENCES contactos (id),
  CONSTRAINT fk_fondo_otros_ingresos FOREIGN KEY (id_fondo) REFERENCES fondos (id) on delete cascade

);


CREATE TABLE prestamos(
  id serial, 
  id_user integer,
  id_contacto integer, 
  id_cuenta integer, 
  descripcion varchar,
  total double precision, 
  fecha date, 
  hora varchar(8), 
  caja integer, 
  CONSTRAINT pk_prestamos PRIMARY KEY (id),
  CONSTRAINT fk_user_prestamos FOREIGN KEY (id_user) REFERENCES   users (id),
  CONSTRAINT fk_cuenta_prestamos FOREIGN KEY (id_cuenta) REFERENCES  cuentas_prestamos (id),
  CONSTRAINT fk_contacto_prestamos FOREIGN KEY (id_contacto) REFERENCES contactos (id)
);


CREATE TABLE abonos_prestamos(
  id serial,
  id_prestamo integer,
  id_user integer,
  abono double precision,
  fecha date,
  hora varchar(8), 
  id_fondo INTEGER,
  CONSTRAINT pk_abono_prestamos PRIMARY KEY (id),
  CONSTRAINT fk_prestamos FOREIGN KEY (id_prestamo) REFERENCES   prestamos (id) ON DELETE CASCADE,
  CONSTRAINT fk_user_abono_prestamos FOREIGN KEY (id_user) REFERENCES   users (id),
  CONSTRAINT fk_fondo_abono_prestamo FOREIGN KEY (id_fondo) REFERENCES fondos (id) on delete cascade

);

CREATE TABLE transferencias(
  id serial, 
  id_user integer,
  id_fondo_origen integer,
  id_fondo_destino integer,
  descripcion varchar,
  total double precision, 
  fecha date, -- Fecha de emisión del gasto
  hora varchar(8), -- hora de emisión del gasto
  CONSTRAINT pk_transferencia PRIMARY KEY (id),
  CONSTRAINT fk_fondo_origen FOREIGN KEY (id_fondo_origen) REFERENCES fondos (id),
  CONSTRAINT fk_fondo_destino FOREIGN KEY (id_fondo_destino) REFERENCES fondos (id),
  CONSTRAINT fk_user_transferencia FOREIGN KEY (id_user) REFERENCES   users (id)
);

------------------------------- BASE DE SERVICIOS ---------------------------


CREATE TABLE tipos_equipos( 
  id serial NOT NULL,
  nombre varchar(50), 
  CONSTRAINT pk_tipos_equipos PRIMARY KEY (id) 
);

CREATE TABLE marcas_equipos(
  id serial NOT NULL,
  nombre varchar(50), 
  CONSTRAINT pkmarcas_equipos_s PRIMARY KEY (id)
);

CREATE TABLE equipos(
  id serial NOT NULL,
  modelo varchar(50), 
  id_marca int,
  id_tipo_equipo int,
  CONSTRAINT pk_modelo PRIMARY KEY (id),
  CONSTRAINT fk_tipo_equipo_modelo FOREIGN KEY (id_tipo_equipo) REFERENCES tipos_equipos(id),
  CONSTRAINT fk_marca_equipos FOREIGN KEY (id_marca) REFERENCES marcas_equipos (id)
);


CREATE TABLE ordenes(
  id serial NOT NULL, -- Código del equipo
  
  fecha_ingresado date,
  hora_ingresado varchar,
    
  fecha_compromiso date,
    
  fecha_entregado date,
  hora_entregado varchar,
  
  id_cliente integer NOT NULL,
  id_equipo integer NOT NULL,
  serial varchar,
  password varchar,
  problema varchar,
  informe varchar,
  
  id_tecnico integer,

  estado integer,

  total double precision,
  
  codigo_factura varchar,

  CONSTRAINT pk_servicio PRIMARY KEY (id),
  CONSTRAINT fk_equipo_ FOREIGN KEY (id_equipo) REFERENCES equipos(id),
  CONSTRAINT fk_cliente FOREIGN KEY (id_cliente) REFERENCES contactos (id)
  );
  
 CREATE TABLE fotos_ordenes(
  id serial NOT NULL,
  nombre varchar, 
  id_servicio integer,
  CONSTRAINT pkmarcas_equipos PRIMARY KEY (id),
  CONSTRAINT fk_id_service FOREIGN KEY (id_servicio) REFERENCES ordenes(id) on delete CASCADE
);




CREATE TABLE creditos(
  id serial, 
  id_user integer,
  id_servicio integer, 
  total double precision, 
  descuento double precision, 
  fecha date, 
  hora varchar(8), 
  CONSTRAINT pk_creditos PRIMARY KEY (id),
  CONSTRAINT fk_user_creditos FOREIGN KEY (id_user) REFERENCES   users (id),
  CONSTRAINT fk_servicio_creditos FOREIGN KEY (id_servicio) REFERENCES ordenes (id)
);


CREATE TABLE abonos_creditos(
  id serial,
  id_credito integer,
  id_user integer,
  abono double precision,
  fecha date,
  hora varchar(8), 
  id_fondo integer,
  id_anticipo integer,
  CONSTRAINT pk_abono_creditos PRIMARY KEY (id),
  CONSTRAINT fk_creditos FOREIGN KEY (id_credito) REFERENCES   creditos (id) ON DELETE CASCADE,
  CONSTRAINT fk_user_abono_creditos FOREIGN KEY (id_user) REFERENCES   users (id),
  CONSTRAINT fk_fondo_abono_creditos FOREIGN KEY (id_fondo) REFERENCES fondos (id) on delete cascade

);


CREATE TABLE anticipos_servicios(
  id serial,
  id_contacto integer,
  id_user integer,
  descripcion varchar(2000), -- Descripcion del anticipo
  fecha date,
  hora varchar(8), 
  id_fondo integer,
  CONSTRAINT pk_anticipo_servicios PRIMARY KEY (id),
  CONSTRAINT fk_id_contacto_anticipo_servicios FOREIGN KEY (id_contacto) REFERENCES contactos (id),
  CONSTRAINT fk_fondo_anticipo_servicios FOREIGN KEY (id_fondo) REFERENCES fondos (id) on delete cascade
);

CREATE TABLE ordenes_facturaciones
(
  id serial NOT NULL,
  id_orden integer,
  id_producto integer,
  cantidad double precision,
  valor_unitario double precision,
  CONSTRAINT pk_facturacion_servicio PRIMARY KEY (id),
  CONSTRAINT fkcabecera_servicio_f FOREIGN KEY (id_orden)
      REFERENCES ordenes (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE CASCADE,
  CONSTRAINT fkproducto_servicio_f FOREIGN KEY (id_producto)
      REFERENCES productos (id) MATCH SIMPLE
      ON UPDATE NO ACTION ON DELETE NO ACTION
)
WITH (
  OIDS=FALSE
);

-------------------------------------------------------------------------------------------------------------------------------------------------
--Inserts
-------------------------------------------------------------------------------------------------------------------------------------------------

insert into perfiles (id,perfil) values (1,'admin');
insert into perfiles (id,perfil) values (2,'cajero');
insert into users (id,nombre,password,user_name,id_perfil) values (1,'admin','6b86b273ff34fce19d6b804eff5a3f5747ada4eaa22f1d49c01e52ddb7875b4b','admin',1);
insert into users (id,nombre,password,user_name,id_perfil) values (2,'caja','000c285457fc971f862a79b786476c78812c8897063c6fa9c045f579a3b2d63f','caja',2);
insert into contactos (id,nombre,cedula,direccion) values (1,'Ventas Diarias','0000','Home');
insert into configuraciones (id,nombre_negocio,ruta_backup,imprimir_factura,productos_repetidos,nit_negocio,representante,ciudad,
eslogan,email,pie_legal,servicios,contacto_negocio,contacto2_negocio,direccion,nombre_impresora,utilidad_venta,utilidad_mayorista,utilidad_credito,iva) 
values (1,'contamonkey','vacio',0,0,'-','-','-','-','-','-','-','-','-','-','-',0,0,0,0);

insert into fondos (id,nombre,predeterminado) values (1,'Caja Diaria',1);
insert into fondos (id,nombre,predeterminado) values (2,'Caja Mayor',0);
insert into fondos (id,nombre,predeterminado) values (3,'Bancolombia',0);


-------------------------------------------------------------------------------------------------------------------------------------------------
--Funciones
-------------------------------------------------------------------------------------------------------------------------------------------------


-------------------------------------------------------------------------------------------------------------------------------------------------
--TRIGGERS
-------------------------------------------------------------------------------------------------------------------------------------------------



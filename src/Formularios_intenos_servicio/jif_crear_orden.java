/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios_intenos_servicio;

import Formularios_internos.jif_crear_producto;
import Formularios_servicio.frm_ordenes;
import JDBuscar.jd_buscar_contacto_servicio;
import Metodos.ImprimirTermicaEtiqueta2x5;
import Metodos.metodos;
import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import conexiondb.DB_Fotos_servicios;
import conexiondb.DB_Ordenes;
import conexiondb.DB_consultas_R_D;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import modelos.Contactos;
import modelos.Fotos_servicios;
import modelos.Marcas_equipos;
import modelos.Equipos;
import modelos.Ordenes;
import modelos.Tipo_equipos;

/**
 *
 * @author Monkeyelgrande
 */
public class jif_crear_orden extends javax.swing.JDialog {

    /**
     * Creates new form jif_crear_marca
     */
    public static DefaultTableModel modelo_fotos = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return false; //Con esto conseguimos que la tabla no se pueda editar
        }
    };

    public static DefaultTableModel modelo_facturar = new DefaultTableModel() { // modelo de la tabla
        @Override
        public boolean isCellEditable(int fila, int columna) { // solo se permiten editables la columan cantidad y precio
            if (columna == 3) { // Columna cantidad
                return columna == 3;
            }
            if (columna == 4) { // Columna precio
                return columna == 4;
            }
            return columna == 3;
        }

        @Override
        public Object getValueAt(int row, int col) { // Sobre escritura del metodo getValue

            if (col == 5) { // digo que la columna 5 (Total) sera igual a la siguiente opreacion
                Double i; // i sera igual a la cantidad
                try {
                    i = Double.parseDouble((String) getValueAt(row, 3)); // capturo la cantidad
                } catch (Exception e) {
                    i = 1.0;
                }

                Double d = Double.parseDouble(metodos.EliminaCaracteres((String) getValueAt(row, 4), ".")); // d sera igual al precio
                if (i != null && d != null) {
                    return metodos.formateador_dinero().format(i * d); // regreso el resultado de multiplicar la cantidad por el valor
                } else {
                    return 0d;
                }
            }

            return super.getValueAt(row, col);

        }

        @Override
        public void setValueAt(Object aValue, int row, int col) {
            super.setValueAt(aValue, row, col);
            calcular_total_facturados_tabla();
            fireTableDataChanged();
        }

    };

    Calendar fecha = new GregorianCalendar();

    public static String id_orden = "";
    public static int id_equipo = 0;
    public static int id_marca = 0;
    public static int id_tipo = 0;
    String ruta_foto = "";

    public jif_crear_orden() {
        initComponents();
        jdate_fecha.setCalendar(fecha);

        jdate_fecha_compromiso.setCalendar(fecha);

        id_orden = DB_consultas_R_D.cargarId("ordenes");
        this.setLocationRelativeTo(this);
        metodos.addEscapeListenerWindowDialog(this);

        Marcas_equipos.mostrarMarcas(jbox_marca);
        Tipo_equipos.mostrarTipoEquipos(jbox_tipo);

        Contactos.MostrarNombreTecnicos(jbox_user_trabajando);

        modelo_fotos.setColumnIdentifiers(new Object[]{"Ruta", "Nombre", "id_foto"});

        jtabla_fotos.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(KeyEvent me) {
                char num = me.getKeyChar();
                if (num == KeyEvent.VK_DELETE) {

                    quitar_fila(jtabla_fotos, modelo_fotos);

                }
            }
        });

        jtabla_fotos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.getClickCount() == 1) {
                    int fila = jtabla_fotos.getSelectedRow();

                    String ruta = (String) jtabla_fotos.getValueAt(fila, 0);
                    metodos.foto_a_label(ruta, lbl_foto_1);

                    ruta_foto = ruta;
                }
            }
        });

        jtabla_fotos.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    int fila = jtabla_fotos.getSelectedRow();

                    String ruta = (String) jtabla_fotos.getValueAt(fila, 0);
                    try {
                        DB_consultas_R_D.Abrir_Archivo(ruta);
                    } catch (Exception e) {
                        System.out.println(e);
                    }
                }
            }
        });

        jtabla_facturados.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(KeyEvent me) {
                char num = me.getKeyChar();
                if (num == KeyEvent.VK_DELETE) {

                    quitar_fila(jtabla_facturados, modelo_facturar);

                }
            }
        });

        try {
            for (int i = 0; i < modelo_facturar.getRowCount(); i++) {
                modelo_facturar.removeRow(i);
                i -= 1;
            }
        } catch (Exception m) {
        }
    }

    public void quitar_fila(JTable tabla, DefaultTableModel modelo) {
        if (modelo.getRowCount() > 0) {

            int fila = tabla.getSelectedRow();
            if (tabla.getSelectedRowCount() < 1) {
                JOptionPane.showMessageDialog(this, "Seleccione un registro");
            } else {
                modelo.removeRow(fila);
                calcular_total_facturados_tabla();

            }
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollPane1 = new javax.swing.JScrollPane();
        jTree1 = new javax.swing.JTree();
        jPanel2 = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        txt_nombre_cliente = new javax.swing.JTextField();
        btn_buscar_cliente = new javax.swing.JButton();
        btn_crear_cliente = new javax.swing.JButton();
        lbl_id_cliente = new javax.swing.JLabel();
        jdate_fecha = new com.toedter.calendar.JDateChooser();
        jPanel4 = new javax.swing.JPanel();
        jLabel8 = new javax.swing.JLabel();
        jbox_tipo = new javax.swing.JComboBox<>();
        btn_crear_modelo = new javax.swing.JButton();
        jbox_modelo = new javax.swing.JComboBox<>();
        jLabel10 = new javax.swing.JLabel();
        btn_crear_tipo_equipo = new javax.swing.JButton();
        jpanel_productos = new javax.swing.JPanel();
        jScrollPane7 = new javax.swing.JScrollPane();
        jtabla_facturados = new javax.swing.JTable();
        jLabel21 = new javax.swing.JLabel();
        txt_Total_servicio = new javax.swing.JTextField();
        btn_add_facturados = new javax.swing.JButton();
        jbox_user_trabajando = new javax.swing.JComboBox<>();
        jLabel42 = new javax.swing.JLabel();
        btn_crear_producto = new javax.swing.JButton();
        jLabel43 = new javax.swing.JLabel();
        jPanel6 = new javax.swing.JPanel();
        jdate_fecha_compromiso = new com.toedter.calendar.JDateChooser();
        jlabel = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        lbl_foto_1 = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtabla_fotos = new javax.swing.JTable();
        btn_cargar_foto = new javax.swing.JButton();
        btn_eliminar_foto = new javax.swing.JButton();
        lbl_foto_2 = new javax.swing.JLabel();
        btn_cargar_foto1 = new javax.swing.JButton();
        jPanel5 = new javax.swing.JPanel();
        txt_serial = new javax.swing.JTextField();
        jLabel9 = new javax.swing.JLabel();
        btn_crear_marca = new javax.swing.JButton();
        jbox_marca = new javax.swing.JComboBox<>();
        jlabel1 = new javax.swing.JLabel();
        jPanel7 = new javax.swing.JPanel();
        btn_guardar = new javax.swing.JButton();
        btn_imprimir = new javax.swing.JButton();
        jPanel8 = new javax.swing.JPanel();
        txt_Codigo_factura = new javax.swing.JTextField();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        txt_problema = new javax.swing.JTextArea();
        jLabel11 = new javax.swing.JLabel();
        txt_password = new javax.swing.JTextField();
        jlabel2 = new javax.swing.JLabel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txt_informe = new javax.swing.JTextArea();
        jLabel12 = new javax.swing.JLabel();
        jPanel11 = new javax.swing.JPanel();
        lbl_id_orden = new javax.swing.JLabel();

        jScrollPane1.setViewportView(jTree1);

        setTitle("Crear Servicio");
        setModal(true);

        jPanel2.setBackground(new java.awt.Color(139, 92, 246));
        jPanel2.setEnabled(false);

        jLabel3.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel3.setForeground(new java.awt.Color(255, 255, 255));
        jLabel3.setText("Cliente");

        txt_nombre_cliente.setEditable(false);
        txt_nombre_cliente.setFont(new java.awt.Font("Yu Gothic Medium", 3, 14)); // NOI18N
        txt_nombre_cliente.setDisabledTextColor(new java.awt.Color(255, 255, 255));

        btn_buscar_cliente.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btn_buscar_cliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/lupa.png"))); // NOI18N
        btn_buscar_cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_buscar_clienteActionPerformed(evt);
            }
        });

        btn_crear_cliente.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btn_crear_cliente.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
        btn_crear_cliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_crear_clienteActionPerformed(evt);
            }
        });

        lbl_id_cliente.setBackground(new java.awt.Color(139, 92, 246));
        lbl_id_cliente.setFont(new java.awt.Font("Segoe UI", 1, 8)); // NOI18N
        lbl_id_cliente.setForeground(new java.awt.Color(139, 92, 246));
        lbl_id_cliente.setText("-");

        jdate_fecha.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txt_nombre_cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 339, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_buscar_cliente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_crear_cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_id_cliente)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdate_fecha, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                        .addComponent(btn_crear_cliente)
                        .addComponent(btn_buscar_cliente)
                        .addComponent(lbl_id_cliente, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jdate_fecha, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_nombre_cliente, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));

        jLabel8.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(51, 51, 51));
        jLabel8.setText("Tipo de Equipo");

        jbox_tipo.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jbox_tipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbox_tipoActionPerformed(evt);
            }
        });

        btn_crear_modelo.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btn_crear_modelo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
        btn_crear_modelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_crear_modeloActionPerformed(evt);
            }
        });

        jbox_modelo.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jbox_modelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbox_modeloActionPerformed(evt);
            }
        });

        jLabel10.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("Modelo");

        btn_crear_tipo_equipo.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btn_crear_tipo_equipo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
        btn_crear_tipo_equipo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_crear_tipo_equipoActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jLabel10, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jbox_tipo, 0, 249, Short.MAX_VALUE)
                    .addComponent(jbox_modelo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_crear_tipo_equipo, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_crear_modelo, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel8)
                        .addComponent(jbox_tipo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_crear_tipo_equipo))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel10)
                        .addComponent(jbox_modelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_crear_modelo, javax.swing.GroupLayout.Alignment.TRAILING))
                .addContainerGap(10, Short.MAX_VALUE))
        );

        jpanel_productos.setBackground(new java.awt.Color(255, 255, 255));

        jtabla_facturados.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jtabla_facturados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla_facturados.setRowHeight(40);
        jtabla_facturados.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jtabla_facturados.getTableHeader().setReorderingAllowed(false);
        jScrollPane7.setViewportView(jtabla_facturados);

        jLabel21.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(51, 51, 51));
        jLabel21.setText("Total servicio ");

        txt_Total_servicio.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        txt_Total_servicio.setText("0");
        txt_Total_servicio.setDisabledTextColor(new java.awt.Color(0, 153, 0));
        txt_Total_servicio.setEnabled(false);
        txt_Total_servicio.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_Total_servicioFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_Total_servicioFocusLost(evt);
            }
        });
        txt_Total_servicio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_Total_servicioKeyTyped(evt);
            }
        });

        btn_add_facturados.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        btn_add_facturados.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/print_24.png"))); // NOI18N
        btn_add_facturados.setText("Agregar Productos / Servicios");
        btn_add_facturados.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_add_facturadosActionPerformed(evt);
            }
        });

        jbox_user_trabajando.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jbox_user_trabajando.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbox_user_trabajandoActionPerformed(evt);
            }
        });

        jLabel42.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel42.setForeground(new java.awt.Color(51, 51, 51));
        jLabel42.setText("Técnico Resposnable");

        btn_crear_producto.setFont(new java.awt.Font("Yu Gothic Medium", 0, 12)); // NOI18N
        btn_crear_producto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
        btn_crear_producto.setText("Crear Producto");
        btn_crear_producto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_crear_productoActionPerformed(evt);
            }
        });

        jLabel43.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel43.setForeground(new java.awt.Color(51, 51, 51));
        jLabel43.setText("Servicios y Productos");

        javax.swing.GroupLayout jpanel_productosLayout = new javax.swing.GroupLayout(jpanel_productos);
        jpanel_productos.setLayout(jpanel_productosLayout);
        jpanel_productosLayout.setHorizontalGroup(
            jpanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jpanel_productosLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jpanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane7, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jpanel_productosLayout.createSequentialGroup()
                        .addGroup(jpanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel43)
                            .addGroup(jpanel_productosLayout.createSequentialGroup()
                                .addComponent(jLabel21)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(txt_Total_servicio, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jpanel_productosLayout.createSequentialGroup()
                        .addComponent(btn_add_facturados)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_crear_producto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel42)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jbox_user_trabajando, javax.swing.GroupLayout.PREFERRED_SIZE, 237, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jpanel_productosLayout.setVerticalGroup(
            jpanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jpanel_productosLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel43)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jpanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel42)
                        .addComponent(jbox_user_trabajando, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jpanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(btn_add_facturados, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(btn_crear_producto, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane7, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jpanel_productosLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_Total_servicio, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel21))
                .addContainerGap())
        );

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));

        jdate_fecha_compromiso.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N

        jlabel.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jlabel.setForeground(new java.awt.Color(51, 51, 51));
        jlabel.setText("Fecha Compromiso");
        jlabel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlabelMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jlabel)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdate_fecha_compromiso, javax.swing.GroupLayout.PREFERRED_SIZE, 194, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jlabel)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jdate_fecha_compromiso, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())))
        );

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        lbl_foto_1.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lbl_foto_1.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        lbl_foto_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_foto_1MouseClicked(evt);
            }
        });

        jtabla_fotos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla_fotos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jtabla_fotos.getTableHeader().setReorderingAllowed(false);
        jScrollPane3.setViewportView(jtabla_fotos);

        btn_cargar_foto.setFont(new java.awt.Font("Yu Gothic Medium", 1, 12)); // NOI18N
        btn_cargar_foto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/photo_24px.png"))); // NOI18N
        btn_cargar_foto.setText("Cargar Foto");
        btn_cargar_foto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cargar_fotoActionPerformed(evt);
            }
        });

        btn_eliminar_foto.setFont(new java.awt.Font("Yu Gothic Medium", 1, 12)); // NOI18N
        btn_eliminar_foto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/delete.png"))); // NOI18N
        btn_eliminar_foto.setText("Eliminar Foto");
        btn_eliminar_foto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_eliminar_fotoActionPerformed(evt);
            }
        });

        lbl_foto_2.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        lbl_foto_2.setBorder(javax.swing.BorderFactory.createEtchedBorder());
        lbl_foto_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                lbl_foto_2MouseClicked(evt);
            }
        });

        btn_cargar_foto1.setFont(new java.awt.Font("Yu Gothic Medium", 1, 12)); // NOI18N
        btn_cargar_foto1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/photo_24px.png"))); // NOI18N
        btn_cargar_foto1.setText("Capturar Foto");
        btn_cargar_foto1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cargar_foto1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(btn_cargar_foto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_eliminar_foto)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_cargar_foto1)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 607, Short.MAX_VALUE)
                            .addComponent(lbl_foto_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(lbl_foto_1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addContainerGap())))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 83, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_cargar_foto, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_eliminar_foto, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_cargar_foto1, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_foto_1, javax.swing.GroupLayout.PREFERRED_SIZE, 221, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_foto_2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));

        txt_serial.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txt_serial.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_serialFocusLost(evt);
            }
        });
        txt_serial.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_serialKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_serialKeyReleased(evt);
            }
        });

        jLabel9.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("Marca");

        btn_crear_marca.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        btn_crear_marca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/mas.png"))); // NOI18N
        btn_crear_marca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_crear_marcaActionPerformed(evt);
            }
        });

        jbox_marca.setFont(new java.awt.Font("Segoe UI", 0, 14)); // NOI18N
        jbox_marca.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbox_marcaActionPerformed(evt);
            }
        });

        jlabel1.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jlabel1.setForeground(new java.awt.Color(51, 51, 51));
        jlabel1.setText("Número de Serie");
        jlabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlabel1MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(jlabel1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addComponent(jbox_marca, 0, 234, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_crear_marca, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(txt_serial))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_serial, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jlabel1))
                .addGap(11, 11, 11)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel9)
                        .addComponent(jbox_marca, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_crear_marca))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel7.setBackground(new java.awt.Color(255, 255, 255));

        btn_guardar.setBackground(new java.awt.Color(139, 92, 246));
        btn_guardar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_guardar.setForeground(new java.awt.Color(255, 255, 255));
        btn_guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/Save48.png"))); // NOI18N
        btn_guardar.setMnemonic('g');
        btn_guardar.setText("Guardar");
        btn_guardar.setBorder(null);
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });

        btn_imprimir.setBackground(new java.awt.Color(139, 92, 246));
        btn_imprimir.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_imprimir.setForeground(new java.awt.Color(255, 255, 255));
        btn_imprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/print_24.png"))); // NOI18N
        btn_imprimir.setMnemonic('g');
        btn_imprimir.setText("Adhesivo");
        btn_imprimir.setBorder(null);
        btn_imprimir.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_imprimirActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel7Layout = new javax.swing.GroupLayout(jPanel7);
        jPanel7.setLayout(jPanel7Layout);
        jPanel7Layout.setHorizontalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 550, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_imprimir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel7Layout.setVerticalGroup(
            jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel7Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_guardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_imprimir, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel8.setBackground(new java.awt.Color(204, 255, 204));

        txt_Codigo_factura.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        txt_Codigo_factura.setDisabledTextColor(new java.awt.Color(0, 153, 0));
        txt_Codigo_factura.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_Codigo_facturaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_Codigo_facturaFocusLost(evt);
            }
        });
        txt_Codigo_factura.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_Codigo_facturaKeyTyped(evt);
            }
        });

        javax.swing.GroupLayout jPanel8Layout = new javax.swing.GroupLayout(jPanel8);
        jPanel8.setLayout(jPanel8Layout);
        jPanel8Layout.setHorizontalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txt_Codigo_factura)
                .addContainerGap())
        );
        jPanel8Layout.setVerticalGroup(
            jPanel8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel8Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(txt_Codigo_factura, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        txt_problema.setColumns(20);
        txt_problema.setFont(new java.awt.Font("Yu Gothic Medium", 0, 14)); // NOI18N
        txt_problema.setRows(5);
        jScrollPane2.setViewportView(txt_problema);

        jLabel11.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel11.setText("Descripción del Problema");

        txt_password.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        txt_password.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_passwordFocusLost(evt);
            }
        });
        txt_password.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_passwordKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txt_passwordKeyReleased(evt);
            }
        });

        jlabel2.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jlabel2.setForeground(new java.awt.Color(51, 51, 51));
        jlabel2.setText("Password");
        jlabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jlabel2MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jlabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txt_password, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jScrollPane2))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_password, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jlabel2))
                    .addComponent(jLabel11))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));

        txt_informe.setColumns(20);
        txt_informe.setFont(new java.awt.Font("Yu Gothic Medium", 0, 14)); // NOI18N
        txt_informe.setRows(5);
        jScrollPane4.setViewportView(txt_informe);

        jLabel12.setFont(new java.awt.Font("Yu Gothic Medium", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(51, 51, 51));
        jLabel12.setText("Informe Final");

        javax.swing.GroupLayout jPanel9Layout = new javax.swing.GroupLayout(jPanel9);
        jPanel9.setLayout(jPanel9Layout);
        jPanel9Layout.setHorizontalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel9Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addComponent(jScrollPane4))
                .addContainerGap())
        );
        jPanel9Layout.setVerticalGroup(
            jPanel9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel9Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel12)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 107, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel11.setBackground(new java.awt.Color(255, 255, 0));

        lbl_id_orden.setFont(new java.awt.Font("Yu Gothic Medium", 1, 20)); // NOI18N
        lbl_id_orden.setText("Orden");

        javax.swing.GroupLayout jPanel11Layout = new javax.swing.GroupLayout(jPanel11);
        jPanel11.setLayout(jPanel11Layout);
        jPanel11Layout.setHorizontalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_id_orden)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel11Layout.setVerticalGroup(
            jPanel11Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel11Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_id_orden)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel7, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jpanel_productos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel3, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel9, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addContainerGap())
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel6, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel11, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jpanel_productos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel9, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel7, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    String tipo = "editar_terminado";
    public static boolean term = false;
    public static int dias_termina = 0;
    private void btn_buscar_clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_buscar_clienteActionPerformed
        jd_buscar_contacto_servicio buscar = new jd_buscar_contacto_servicio(null, rootPaneCheckingEnabled);
        buscar.formulario = "crear_servicio";
        buscar.show();
    }//GEN-LAST:event_btn_buscar_clienteActionPerformed

    private void btn_crear_clienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_crear_clienteActionPerformed
        jif_crear_contactos_servicio frm = new jif_crear_contactos_servicio();
        jif_crear_contactos_servicio.formulario = "servicio";
        frm.show();
        jif_crear_contactos_servicio.txt_nombre.requestFocus();
    }//GEN-LAST:event_btn_crear_clienteActionPerformed

    private void jbox_tipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbox_tipoActionPerformed
        consulta_equipos();
        try {
            id_tipo = jbox_tipo.getItemAt(jbox_tipo.getSelectedIndex()).getId();
        } catch (Exception e) {
        }
    }//GEN-LAST:event_jbox_tipoActionPerformed

    private void jbox_marcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbox_marcaActionPerformed
        consulta_equipos();
        try {
            id_marca = jbox_marca.getItemAt(jbox_marca.getSelectedIndex()).getId();
        } catch (Exception e) {
        }

    }//GEN-LAST:event_jbox_marcaActionPerformed
    DecimalFormat formatea = new DecimalFormat("###,###.##");

    private void btn_crear_tipo_equipoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_crear_tipo_equipoActionPerformed
        jif_crear_tipo_equipos frm = new jif_crear_tipo_equipos();
        jif_crear_tipo_equipos.formulario = "servicio";
        jif_crear_tipo_equipos.chk_cerrar.setEnabled(false);
        frm.show();
        jif_crear_tipo_equipos.txt_nombre.requestFocus();
    }//GEN-LAST:event_btn_crear_tipo_equipoActionPerformed

    private void btn_crear_marcaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_crear_marcaActionPerformed
        jif_crear_marca_equipo frm = new jif_crear_marca_equipo();
        jif_crear_marca_equipo.formulario = "servicio";
        jif_crear_marca_equipo.chk_cerrar.setEnabled(false);
        frm.show();
        jif_crear_marca_equipo.txt_nombre.requestFocus();
    }//GEN-LAST:event_btn_crear_marcaActionPerformed

    private void btn_crear_modeloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_crear_modeloActionPerformed
        if (jbox_tipo.getSelectedIndex() <= 0 || jbox_marca.getSelectedIndex() <= 0) {
            JOptionPane.showMessageDialog(this, "Por favor seleccione un tipo de equipo y una marca para crear el modelo");
        } else {
            jif_crear_modelos frm = new jif_crear_modelos();
            jif_crear_modelos.formulario = "servicio";
            jif_crear_modelos.chk_cerrar.setEnabled(false);

            jif_crear_modelos.jbox_tipo.setSelectedItem(jbox_tipo.getSelectedItem());
            jif_crear_modelos.jbox_marca.setSelectedItem(jbox_marca.getSelectedItem());

            jif_crear_modelos.jbox_tipo.setEnabled(false);
            jif_crear_modelos.jbox_marca.setEnabled(false);

            frm.show();
            jif_crear_modelos.txt_modelo.requestFocus();
        }
    }//GEN-LAST:event_btn_crear_modeloActionPerformed

    static double sin_iva2 = 0;

    private void txt_serialFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_serialFocusLost
//        cargar_equipo();
    }//GEN-LAST:event_txt_serialFocusLost

    private void txt_serialKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_serialKeyPressed
        char num = evt.getKeyChar();
        if ((num == KeyEvent.VK_ENTER)) {
            cargar_equipo();
        }
    }//GEN-LAST:event_txt_serialKeyPressed
    public static void cargar_equipo() {

        String serial = txt_serial.getText().toUpperCase();
        txt_serial.setText(serial);

        if (DB_consultas_R_D.consultar_existencia_campo_String("serial", serial, "equipos") == 1) {
            String consulta = "select e.id, t.nombre as tipo, ma.nombre as marca, mo.modelo as modelo, serial "
                    + "from equipos e, modelos mo, marcas ma, tipos_equipos t "
                    + "where e.id_modelo=mo.id and mo.id_marca=ma.id and mo.id_tipo_equipo=t.id and e.serial='" + serial + "' "
                    + "order by t.nombre";

            ResultSet rs = DB_consultas_R_D.getTabla(consulta);
            try {
                while (rs.next()) {
                    id_equipo = rs.getInt("id");
                    jbox_tipo.setSelectedItem(rs.getString("tipo"));
                    jbox_marca.setSelectedItem(rs.getString("marca"));
                    jbox_modelo.setSelectedItem(rs.getString("modelo"));
                }
                rs.close();
                jbox_tipo.setEnabled(false);
                jbox_marca.setEnabled(false);
                jbox_modelo.setEnabled(false);

            } catch (Exception e) {
                System.out.println(e);
            }
        } else {
            id_equipo = 0;
            jbox_tipo.setEnabled(true);
            jbox_marca.setEnabled(true);
            jbox_modelo.setEnabled(true);
        }

    }
    private void txt_serialKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_serialKeyReleased
        txt_serial.setText(txt_serial.getText().toUpperCase());
    }//GEN-LAST:event_txt_serialKeyReleased

    private void jlabelMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlabelMouseClicked

    }//GEN-LAST:event_jlabelMouseClicked
    String ruta_origen_imagen = "";
    public static String nombreArchivoImagen = "";
    String ruta_destino_nombre_archivo_imagen = "";
    int longitudBytes;


    private void btn_cargar_fotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cargar_fotoActionPerformed

        ruta_origen_imagen = "";
        nombreArchivoImagen = "";
        ruta_destino_nombre_archivo_imagen = "";

        lbl_foto_1.setIcon(null);

        JFileChooser j = new JFileChooser();
        j.setFileSelectionMode(JFileChooser.FILES_ONLY);//solo archivos y no carpetas
        FileNameExtensionFilter filtroImagen = new FileNameExtensionFilter("JPG, PNG & GIF", "jpg", "png", "gif");
        j.setFileFilter(filtroImagen);

        int estado = j.showOpenDialog(null);
        if (estado == JFileChooser.APPROVE_OPTION) {
            File fichero = j.getSelectedFile();
            ruta_origen_imagen = fichero.getAbsolutePath();
            nombreArchivoImagen = id_orden + "_" + DB_consultas_R_D.obtener_fecha() + DB_consultas_R_D.obtener_hora_con_guiones() + "_" + fichero.getName();
//            ruta_destino_nombre_archivo_imagen = DB_consultas_R_D.Ruta_Imagenes() + nombreArchivoImagen;
            //                fis = new FileInputStream(j.getSelectedFile());
            //necesitamos saber la cantidad de bytes
            this.longitudBytes = (int) j.getSelectedFile().length();

            try {

                Image img = ImageIO.read(j.getSelectedFile());
//                Image img = new ImageIcon(ruta).getImage();
                // Obtener el ancho y alto originales de la imagen
                int originalWidth = img.getWidth(null);
                int originalHeight = img.getHeight(null);

                // Calcular la escala para ajustar la imagen proporcionalmente
                double scaleFactor = Math.min(1.0 * lbl_foto_1.getWidth() / originalWidth, 1.0 * lbl_foto_1.getHeight() / originalHeight);

                // Redimensionar la imagen utilizando la escala calculada
                int scaledWidth = (int) (originalWidth * scaleFactor);
                int scaledHeight = (int) (originalHeight * scaleFactor);

                Image newimg = img.getScaledInstance(scaledWidth, scaledHeight, Image.SCALE_DEFAULT);

                // Crear un ImageIcon personalizado para centrar la imagen
                ImageIcon newicon = new ImageIcon(newimg) {
                    @Override
                    public synchronized void paintIcon(Component c, Graphics g, int x, int y) {
                        // Calcular las coordenadas para centrar la imagen en el JLabel
                        int offsetX = (lbl_foto_1.getWidth() - getIconWidth()) / 2;
                        int offsetY = (lbl_foto_1.getHeight() - getIconHeight()) / 2;

                        // Dibujar la imagen centrada
                        g.drawImage(newimg, x + offsetX, y + offsetY, c);
                    }
                };

                lbl_foto_1.setIcon(newicon);

                modelo_fotos.addRow(new Object[]{ruta_origen_imagen, nombreArchivoImagen, 0});

                jtabla_fotos.setModel(modelo_fotos);

            } catch (IOException ex) {
                JOptionPane.showMessageDialog(rootPane, "imagen: " + ex);
            }
        }
    }//GEN-LAST:event_btn_cargar_fotoActionPerformed

    private void btn_eliminar_fotoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_eliminar_fotoActionPerformed

        int fila = jtabla_fotos.getSelectedRow();
        if (fila == -1) {
            JOptionPane.showMessageDialog(null, "Seleccione un registro");
        } else {
            try {

                int dialogButton = JOptionPane.YES_NO_OPTION;
                int dialogResult = JOptionPane.showConfirmDialog(null, "Desea eliminar esta imagen de este servicio?\n", "Alerta", dialogButton);
                if (dialogResult == JOptionPane.YES_OPTION) {

                    String id_foto = jtabla_fotos.getValueAt(fila, 2).toString();
                    String ruta_foto = jtabla_fotos.getValueAt(fila, 0).toString();

                    DB_consultas_R_D.eliminar("fotos_servicios", id_foto);
                    DB_consultas_R_D.Eliminar_Archivo(ruta_foto);

                    for (int i = 0; i < modelo_fotos.getRowCount(); i++) {
                        if (modelo_fotos.getValueAt(i, 2).toString().equals(id_foto)) {
                            modelo_fotos.removeRow(i);
                            break;
                        }
                    }
                    jtabla_fotos.setModel(modelo_fotos);
                    lbl_foto_1.setIcon(null);
                    nombreArchivoImagen = "";
                    ruta_destino_nombre_archivo_imagen = "";
                    ruta_origen_imagen = "";

                }

            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "No se pudo realizar la eliminacion");
            }
        }
    }//GEN-LAST:event_btn_eliminar_fotoActionPerformed

    public static void calcular_total_facturados_tabla() {
        double total = 0;

        for (int i = 0; i < jtabla_facturados.getRowCount(); i++) {
            String dato = "";
            int posicion_coma = jtabla_facturados.getValueAt(i, 5).toString().indexOf(",") + 1;
            try {
                dato = jtabla_facturados.getValueAt(i, 5).toString().substring(0, posicion_coma - 1);
            } catch (Exception e) {
                dato = jtabla_facturados.getValueAt(i, 5).toString();
            }
//            System.out.println(dato);
            total += Double.parseDouble(metodos.EliminaCaracteres(dato, "."));
        }

        try {
            jd_facturar_productos.lbl_total_facturacion.setText(metodos.formateador_dinero().format(total) + "");

        } catch (Exception e) {
        }
    }

    private void txt_Total_servicioFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_Total_servicioFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_Total_servicioFocusGained

    private void txt_Total_servicioFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_Total_servicioFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_Total_servicioFocusLost

    private void txt_Total_servicioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_Total_servicioKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_Total_servicioKeyTyped

    private void btn_add_facturadosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_add_facturadosActionPerformed
        jd_facturar_productos frm = new jd_facturar_productos();
        jd_facturar_productos.jtabla_facturar.setModel(modelo_facturar);
        frm.show();
    }//GEN-LAST:event_btn_add_facturadosActionPerformed
    public static void TamanosTabla() {
        jtabla_facturados.getTableHeader().setReorderingAllowed(false);
        TableColumnModel columnModel = jtabla_facturados.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(0);
        columnModel.getColumn(1).setPreferredWidth(0);
        columnModel.getColumn(2).setPreferredWidth(600);
        columnModel.getColumn(3).setPreferredWidth(100);
        columnModel.getColumn(4).setPreferredWidth(150);
        columnModel.getColumn(5).setPreferredWidth(150);

    }
    private void jbox_user_trabajandoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbox_user_trabajandoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jbox_user_trabajandoActionPerformed

    private void lbl_foto_1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_foto_1MouseClicked
        try {
            DB_consultas_R_D.Abrir_Archivo(ruta_foto);
        } catch (Exception e) {
        }
    }//GEN-LAST:event_lbl_foto_1MouseClicked
    boolean edita_ingreso = true;
    boolean edita_cot = true;

    private void lbl_foto_2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_lbl_foto_2MouseClicked


    }//GEN-LAST:event_lbl_foto_2MouseClicked

    boolean edit_serial = true;
    private void btn_crear_productoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_crear_productoActionPerformed
        jif_crear_producto j = new jif_crear_producto();

        j.formulario = "crear_servicio";
        j.jtxt_descripcion.requestFocus();
        j.chk_cerrar.setEnabled(false);

        j.show();
    }//GEN-LAST:event_btn_crear_productoActionPerformed

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed

        if (lbl_id_cliente.getText().equals("-") || id_equipo == 0) {
            JOptionPane.showMessageDialog(this, "Por favor verifique que halla cargado los campos: \nCLIENTE\nEQUIPO");
        } else {

            DB_Ordenes db_orden = new DB_Ordenes();
            Ordenes orden = new Ordenes();

            orden.setId(Integer.parseInt(id_orden));
            orden.setId_equipo(id_equipo);
            orden.setId_cliente(Integer.parseInt(lbl_id_cliente.getText()));

            orden.setEstado(1);

            int dia, mes, ano;
            ano = jdate_fecha.getCalendar().get(Calendar.YEAR);
            mes = jdate_fecha.getCalendar().get(Calendar.MARCH) + 1;
            dia = jdate_fecha.getCalendar().get(Calendar.DAY_OF_MONTH);
            orden.setFecha_ingresado(ano + "-" + mes + "-" + dia);
            orden.setHora_ingresado(DB_consultas_R_D.obtener_hora());

            ano = jdate_fecha_compromiso.getCalendar().get(Calendar.YEAR);
            mes = jdate_fecha_compromiso.getCalendar().get(Calendar.MARCH) + 1;
            dia = jdate_fecha_compromiso.getCalendar().get(Calendar.DAY_OF_MONTH);
            orden.setFecha_compromiso(ano + "-" + mes + "-" + dia);

            orden.setPassword(txt_password.getText());
            orden.setTotal(Double.parseDouble(txt_Total_servicio.getText()));
            orden.setProblema(txt_problema.getText());
            orden.setInforme(txt_informe.getText());
            orden.setSerial(txt_serial.getText());
            try {
                orden.setId_tecnico(jbox_user_trabajando.getItemAt(jbox_user_trabajando.getSelectedIndex()).getId());

            } catch (Exception e) {
                orden.setId_tecnico(1);
            }
            int guardo = 0;
            if (DB_consultas_R_D.consultarId(lbl_id_orden.getText(), "ordenes") == 1) {
                guardo = db_orden.Actualizar(orden);
            } else {
                guardo = db_orden.Guardar(orden);
            }
            if (guardo > 0) {
                JOptionPane.showMessageDialog(this, "Se ha guardado el servicio");

                Connection con = DB_consultas_R_D.getConexion();
                PreparedStatement psql = null;

                int id_cotizacion_detalle = Integer.parseInt(DB_consultas_R_D.cargarId("ordenes_facturaciones"));
                String SSQL_productos_cot = "delete from ordenes_facturaciones where id_orden=" + orden.getId() + ";\n"
                        + "INSERT INTO ordenes_facturaciones (id,id_orden,id_producto,cantidad,valor_unitario) VALUES ";
                for (int i = 0; i < modelo_facturar.getRowCount(); i++) {

                    SSQL_productos_cot += "(" + id_cotizacion_detalle + "," + orden.getId() + ",'" + modelo_facturar.getValueAt(i, 0) + "',"
                            + modelo_facturar.getValueAt(i, 3).toString() + ","
                            + metodos.EliminaCaracteres("" + modelo_facturar.getValueAt(i, 4), ".") + "),\n";

                    id_cotizacion_detalle++;
                }

                try {
                    SSQL_productos_cot = SSQL_productos_cot.substring(0, SSQL_productos_cot.length() - 2);
                    System.out.println(SSQL_productos_cot);
                    psql = con.prepareStatement(SSQL_productos_cot);
                    psql.executeUpdate();

                } catch (Exception e) {
                    System.out.println(e);
                }

                btn_add_facturados.setEnabled(true);
                jbox_user_trabajando.setEnabled(true);

                // FOTOS
                if (modelo_fotos.getRowCount() > 0) {
                    for (int i = 0; i < modelo_fotos.getRowCount(); i++) {

                        metodos.copyFile_Java7(jtabla_fotos.getValueAt(i, 0).toString(), DB_consultas_R_D.Ruta_Imagenes() + jtabla_fotos.getValueAt(i, 1).toString());

                        Fotos_servicios foto = new Fotos_servicios(jtabla_fotos.getValueAt(i, 1).toString(), orden.getId(), DB_consultas_R_D.cargarId_return_int("fotos_servicios"));
                        DB_Fotos_servicios db = new DB_Fotos_servicios();
                        db.Guardar(foto);
                    }

                }

                frm_ordenes.btn_actualizar.doClick();

                btn_buscar_cliente.setEnabled(false);
                btn_crear_cliente.setEnabled(false);
                btn_crear_marca.setEnabled(false);
                btn_crear_modelo.setEnabled(false);
                btn_crear_tipo_equipo.setEnabled(false);
                btn_add_facturados.setEnabled(true);
                btn_cargar_foto.setEnabled(false);
                btn_eliminar_foto.setEnabled(false);
                jbox_tipo.setEnabled(false);
                jbox_marca.setEnabled(false);
                jbox_modelo.setEnabled(false);
                txt_serial.setEditable(false);

                jpanel_productos.setVisible(true);

            }
        }


    }//GEN-LAST:event_btn_guardarActionPerformed
    private void txt_Codigo_facturaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_Codigo_facturaFocusGained
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_Codigo_facturaFocusGained

    private void txt_Codigo_facturaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_Codigo_facturaFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_Codigo_facturaFocusLost

    private void txt_Codigo_facturaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_Codigo_facturaKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_Codigo_facturaKeyTyped

    private void btn_cargar_foto1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cargar_foto1ActionPerformed
        lanzar_camara();

    }//GEN-LAST:event_btn_cargar_foto1ActionPerformed

    private void jbox_modeloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbox_modeloActionPerformed
        try {

            int id_modelo = jbox_modelo.getItemAt(jbox_modelo.getSelectedIndex()).getId();
            String nombre_modelo = jbox_modelo.getSelectedItem().toString();

            int pos_tipo = jbox_tipo.getSelectedIndex();
            if (pos_tipo <= 0) {
                jbox_tipo.setSelectedItem(Equipos.TraerNombreTipoConIdModelo(id_modelo));
            }

            int pos_marca = jbox_marca.getSelectedIndex();
            if (pos_marca <= 0) {
                jbox_marca.setSelectedItem(Equipos.TraerNombreMarcaConIdModelo(id_modelo));
            }
            this.id_equipo = id_modelo;
            //            System.out.println(id_modelo);
            jbox_modelo.setSelectedItem(nombre_modelo);

        } catch (Exception e) {
            System.out.println(e);
        }
    }//GEN-LAST:event_jbox_modeloActionPerformed

    private void jlabel1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlabel1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jlabel1MouseClicked

    private void jlabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jlabel2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jlabel2MouseClicked

    private void txt_passwordFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_passwordFocusLost
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_passwordFocusLost

    private void txt_passwordKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_passwordKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_passwordKeyPressed

    private void txt_passwordKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_passwordKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txt_passwordKeyReleased

    private void btn_imprimirActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_imprimirActionPerformed
        ImprimirTermicaEtiqueta2x5 imprimir = new ImprimirTermicaEtiqueta2x5(txt_nombre_cliente.getText(),
                lbl_id_orden.getText(),
                jbox_tipo.getSelectedItem().toString() + " " + jbox_marca.getSelectedItem().toString() + " " + jbox_modelo.getSelectedItem().toString()
        );
        imprimir.imprime();
    }//GEN-LAST:event_btn_imprimirActionPerformed

    public void lanzar_camara() {
        // Configurar la webcam
        Webcam webcam = Webcam.getDefault();
        webcam.setViewSize(WebcamResolution.VGA.getSize());

        // Crear el panel de la webcam
        WebcamPanel panel = new WebcamPanel(webcam);
        panel.setFPSDisplayed(true);
        panel.setDisplayDebugInfo(true);
        panel.setImageSizeDisplayed(true);

        // Crear el botón
        JButton btnCaptura = new JButton("Capturar");

        // Crear un panel contenedor para organizar los componentes
        JPanel contenedor = new JPanel();

        // Configurar el diseño del panel contenedor (FlowLayout)
        contenedor.setLayout(new java.awt.FlowLayout());

        // Agregar el panel de la webcam y el botón al panel contenedor
        contenedor.add(panel);
        contenedor.add(btnCaptura);

        // Crear un JDialog en lugar de un JFrame
        JDialog dialog = new JDialog();

        dialog.setTitle("CAPTURAR IMÁGEN");
        dialog.setModalityType(JDialog.DEFAULT_MODALITY_TYPE);
        dialog.add(contenedor); // Agregar el panel contenedor al JDialog
        dialog.setResizable(true);
        dialog.setUndecorated(true);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.pack();

        // Manejar el evento de clic en el botón
        btnCaptura.addActionListener(e -> {
            // Capturar la imagen actual del panel de la cámara
            BufferedImage imagenCapturada = webcam.getImage();

            // Guardar la imagen en un archivo (puedes personalizar la ruta y el formato)
            String name = "c:/temp_camera/" + id_orden + "_" + DB_consultas_R_D.obtener_fecha() + DB_consultas_R_D.obtener_hora_con_guiones() + "_camara.png";

            File archivoImagen = new File(name);
            try {
                ImageIO.write(imagenCapturada, "PNG", archivoImagen);
                System.out.println("Imagen guardada exitosamente en: " + archivoImagen.getAbsolutePath());

                modelo_fotos.addRow(new Object[]{archivoImagen.getAbsolutePath(), archivoImagen.getName(), 0});
                jtabla_fotos.setModel(modelo_fotos);

            } catch (IOException ex) {
                ex.printStackTrace();
            }

        });

        // Agregar el KeyListener al panel contenedor
        contenedor.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {
                // No se utiliza en este caso
            }

            @Override
            public void keyPressed(KeyEvent e) {
                // Verificar si la tecla presionada es la tecla "Escape"
                if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                    // Realizar alguna acción al presionar la tecla "Escape"
                    System.out.println("Tecla Escape presionada");
                    webcam.close();
                    dialog.dispose();

                }
            }

            @Override
            public void keyReleased(KeyEvent e) {
                // No se utiliza en este caso
            }
        });
        contenedor.setFocusable(true);
        contenedor.requestFocusInWindow();

        dialog.setVisible(true);
    }

    public void consulta_equipos() {
        int id_tipo = 0, id_marca = 0;

        try {
            id_tipo = jbox_tipo.getItemAt(jbox_tipo.getSelectedIndex()).getId();
        } catch (Exception e) {
            id_tipo = 0;
        }
        try {
            id_marca = jbox_marca.getItemAt(jbox_marca.getSelectedIndex()).getId();
        } catch (Exception e) {
            id_marca = 0;
        }

        Equipos.mostrarModelosXTipoYMarca(jbox_modelo, id_tipo, id_marca);
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btn_add_facturados;
    public static javax.swing.JButton btn_buscar_cliente;
    public static javax.swing.JButton btn_cargar_foto;
    public static javax.swing.JButton btn_cargar_foto1;
    public static javax.swing.JButton btn_crear_cliente;
    public static javax.swing.JButton btn_crear_marca;
    public static javax.swing.JButton btn_crear_modelo;
    public static javax.swing.JButton btn_crear_producto;
    public static javax.swing.JButton btn_crear_tipo_equipo;
    public static javax.swing.JButton btn_eliminar_foto;
    public static javax.swing.JButton btn_guardar;
    public static javax.swing.JButton btn_imprimir;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel43;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel11;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JPanel jPanel7;
    private javax.swing.JPanel jPanel8;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane7;
    private javax.swing.JTree jTree1;
    public static javax.swing.JComboBox<Marcas_equipos> jbox_marca;
    public static javax.swing.JComboBox<modelos.Equipos> jbox_modelo;
    public static javax.swing.JComboBox<Tipo_equipos> jbox_tipo;
    public static javax.swing.JComboBox<Contactos> jbox_user_trabajando;
    public static com.toedter.calendar.JDateChooser jdate_fecha;
    public static com.toedter.calendar.JDateChooser jdate_fecha_compromiso;
    public static javax.swing.JLabel jlabel;
    public static javax.swing.JLabel jlabel1;
    public static javax.swing.JLabel jlabel2;
    public static javax.swing.JPanel jpanel_productos;
    public static javax.swing.JTable jtabla_facturados;
    public static javax.swing.JTable jtabla_fotos;
    public static javax.swing.JLabel lbl_foto_1;
    public static javax.swing.JLabel lbl_foto_2;
    public static javax.swing.JLabel lbl_id_cliente;
    public static javax.swing.JLabel lbl_id_orden;
    public static javax.swing.JTextField txt_Codigo_factura;
    public static javax.swing.JTextField txt_Total_servicio;
    public static javax.swing.JTextArea txt_informe;
    public static javax.swing.JTextField txt_nombre_cliente;
    public static javax.swing.JTextField txt_password;
    public static javax.swing.JTextArea txt_problema;
    public static javax.swing.JTextField txt_serial;
    // End of variables declaration//GEN-END:variables
}

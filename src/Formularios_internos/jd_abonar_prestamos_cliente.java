/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios_internos;

import Formularios.frm_main;
import Formularios.frm_prestamos;
import Metodos.CellRendererFacturas;
import Metodos.TextPrompt;
import Metodos.metodos;
import conexiondb.DB_consultas_R_D;
import conexiondb.DBabonos_prestamos;
import java.awt.Color;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import modelos.Abonos_prestamos;
import modelos.Fondos;

/**
 *
 * @author Monkeyelgrande
 */
public class jd_abonar_prestamos_cliente extends javax.swing.JDialog {

    /**
     * Creates new form jd_ver_devolucion
     */
    CellRendererFacturas myRenderer = new CellRendererFacturas();
    public static DefaultTableModel modelo_facturas = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return false; //Con esto conseguimos que la tabla no se pueda editar
        }
    };
    public static DefaultTableModel modelo_abonos = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return false; //Con esto conseguimos que la tabla no se pueda editar
        }
    };

    public jd_abonar_prestamos_cliente(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(parent);
        metodos.addEscapeListenerWindowDialog(this);
        TextPrompt prompt = new TextPrompt("Valor abono", txt_abono);
        jtabla_facturas.setDefaultRenderer(Object.class, myRenderer);
        jtabla_abonos.setDefaultRenderer(Object.class, myRenderer);
        Calendar fecha = new GregorianCalendar();
        jdate_fecha_creacion.setCalendar(fecha);
        jtabla_facturas.setModel(modelo_facturas);
        Doble_clic_tablas_CC(jtabla_facturas, modelo_facturas, jtabla_abonos, modelo_abonos);
        Doble_clic_tablas_CC(jtabla_abonos, modelo_abonos, jtabla_facturas, modelo_facturas);
        Fondos.mostrarFondos(jbox_Fondos);

        txt_abono.requestFocus();
    }

    public void Doble_clic_tablas_CC(JTable origen, DefaultTableModel modeloOrigen, JTable destino, DefaultTableModel modeloDestino) {
        origen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.getClickCount() == 2) {

                    int fila = origen.getSelectedRow();

                    modeloDestino.setColumnIdentifiers(new Object[]{"ID factura", "Total Factura", "Abono", "Saldo", "Fecha Creación", "Fecha Vencimiento", "Dias vencidos"});
                    modeloDestino.addRow(new Object[]{origen.getValueAt(fila, 0), origen.getValueAt(fila, 1), origen.getValueAt(fila, 2), origen.getValueAt(fila, 3),
                        origen.getValueAt(fila, 4), origen.getValueAt(fila, 5), origen.getValueAt(fila, 6)});
                    destino.setModel(modeloDestino);
                    modeloOrigen.removeRow(fila);
                    TamanosTablaAbonos(destino);
                    sumar_totales_filtro();
                    sumar_totales();
                }
            }
        });
    }

    public static void TamanosTablaAbonos(JTable tabla) {
        tabla.getTableHeader().setReorderingAllowed(false);
        TableColumnModel columnModel = tabla.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(40);
        columnModel.getColumn(1).setPreferredWidth(80);
        columnModel.getColumn(2).setPreferredWidth(100);
        columnModel.getColumn(3).setPreferredWidth(100);
        columnModel.getColumn(4).setPreferredWidth(100);
        columnModel.getColumn(5).setPreferredWidth(100);
        columnModel.getColumn(6).setPreferredWidth(30);

    }

    public static void sumar_totales() {
        double total_facturas = 0;
        double total_abonos = 0;
        double total_saldo = 0;
        for (int i = 0; i < jd_abonar_prestamos_cliente.jtabla_facturas.getRowCount(); i++) {
            total_facturas += Double.parseDouble(metodos.EliminaCaracteres(jd_abonar_prestamos_cliente.jtabla_facturas.getValueAt(i, 2).toString(), ","));
            total_abonos += Double.parseDouble(metodos.EliminaCaracteres(jd_abonar_prestamos_cliente.jtabla_facturas.getValueAt(i, 3).toString(), ","));
            total_saldo += Double.parseDouble(metodos.EliminaCaracteres(jd_abonar_prestamos_cliente.jtabla_facturas.getValueAt(i, 4).toString(), ","));
        }
        lbl_total_factura.setText(metodos.formateador().format(total_facturas));
        lbl_total_abonos.setText(metodos.formateador().format(total_abonos));
        lbl_total_saldo.setText(metodos.formateador().format(total_saldo));
    }

    public static void sumar_totales_filtro() {
        double total_facturas = 0;
        double total_abonos = 0;
        double total_saldo = 0;
        for (int i = 0; i < jd_abonar_prestamos_cliente.jtabla_abonos.getRowCount(); i++) {
            total_facturas += Double.parseDouble(metodos.EliminaCaracteres(jd_abonar_prestamos_cliente.jtabla_abonos.getValueAt(i, 2).toString(), ","));
            total_abonos += Double.parseDouble(metodos.EliminaCaracteres(jd_abonar_prestamos_cliente.jtabla_abonos.getValueAt(i, 3).toString(), ","));
            total_saldo += Double.parseDouble(metodos.EliminaCaracteres(jd_abonar_prestamos_cliente.jtabla_abonos.getValueAt(i, 4).toString(), ","));
        }
        lbl_total_factura_filtro.setText(metodos.formateador().format(total_facturas));
        lbl_total_abonos_filtro.setText(metodos.formateador().format(total_abonos));
        lbl_total_saldo_filtro.setText(metodos.formateador().format(total_saldo));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel4 = new javax.swing.JLabel();
        lbl_cliente = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lbl_total_factura = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lbl_total_abonos = new javax.swing.JLabel();
        lbl_total_saldo = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        lbl_cedula = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        lbl_id_cliente = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jdate_fecha_creacion = new com.toedter.calendar.JDateChooser();
        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        txt_abono = new javax.swing.JTextField();
        btn_limpiar1 = new javax.swing.JButton();
        btn_guardar = new javax.swing.JButton();
        btn_abonar = new javax.swing.JButton();
        jbox_Fondos = new javax.swing.JComboBox<>();
        lbl_cliente1 = new javax.swing.JLabel();
        lbl_total_saldo1 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbl_total_factura_filtro = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        lbl_total_saldo_filtro = new javax.swing.JLabel();
        lbl_total_abonos_filtro = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtabla_facturas = new javax.swing.JTable();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtabla_abonos = new javax.swing.JTable();

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Abonos");
        setModal(true);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jLabel4.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(51, 51, 51));
        jLabel4.setText("Cliente");

        lbl_cliente.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lbl_cliente.setForeground(new java.awt.Color(0, 102, 102));
        lbl_cliente.setText("Cliente");

        jLabel5.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel5.setForeground(new java.awt.Color(51, 51, 51));
        jLabel5.setText("Total facturas");

        lbl_total_factura.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lbl_total_factura.setForeground(new java.awt.Color(0, 102, 204));
        lbl_total_factura.setText("total");

        jLabel9.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel9.setForeground(new java.awt.Color(51, 51, 51));
        jLabel9.setText("Total abonos");

        lbl_total_abonos.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lbl_total_abonos.setForeground(new java.awt.Color(0, 204, 0));
        lbl_total_abonos.setText("total");

        lbl_total_saldo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_total_saldo.setForeground(new java.awt.Color(204, 0, 0));
        lbl_total_saldo.setText("total");

        jLabel10.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel10.setForeground(new java.awt.Color(51, 51, 51));
        jLabel10.setText("Total saldo");

        lbl_cedula.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lbl_cedula.setForeground(new java.awt.Color(0, 102, 102));
        lbl_cedula.setText("cedula");

        jLabel6.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(51, 51, 51));
        jLabel6.setText("Cédula");

        lbl_id_cliente.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lbl_id_cliente.setForeground(new java.awt.Color(255, 255, 242));
        lbl_id_cliente.setText("id_cliente");

        jLabel12.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jLabel12.setForeground(new java.awt.Color(0, 102, 102));
        jLabel12.setText("Fecha nuevo abono");

        jdate_fecha_creacion.setForeground(new java.awt.Color(0, 153, 51));
        jdate_fecha_creacion.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_total_factura)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_total_abonos)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_total_saldo))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_cliente)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel6)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_cedula)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_id_cliente)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel12)
                    .addComponent(jdate_fecha_creacion, javax.swing.GroupLayout.PREFERRED_SIZE, 214, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel6)
                                .addComponent(lbl_cedula)
                                .addComponent(lbl_id_cliente))
                            .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel4)
                                .addComponent(lbl_cliente)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(lbl_total_factura)
                            .addComponent(jLabel9)
                            .addComponent(lbl_total_abonos)
                            .addComponent(jLabel10)
                            .addComponent(lbl_total_saldo)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel12)
                        .addGap(11, 11, 11)
                        .addComponent(jdate_fecha_creacion, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));

        jPanel2.setBackground(new java.awt.Color(45, 54, 76));

        txt_abono.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_abono.setForeground(new java.awt.Color(0, 153, 153));
        txt_abono.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                txt_abonoFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                txt_abonoFocusLost(evt);
            }
        });
        txt_abono.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_abonoKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_abonoKeyTyped(evt);
            }
        });

        btn_limpiar1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_limpiar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/limpiar.png"))); // NOI18N
        btn_limpiar1.setMnemonic('l');
        btn_limpiar1.setText("Limpiar");
        btn_limpiar1.setBorder(null);
        btn_limpiar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_limpiar1ActionPerformed(evt);
            }
        });

        btn_guardar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_guardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/guardar.png"))); // NOI18N
        btn_guardar.setMnemonic('g');
        btn_guardar.setText("Guardar");
        btn_guardar.setBorder(null);
        btn_guardar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_guardarActionPerformed(evt);
            }
        });
        btn_guardar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                btn_guardarKeyPressed(evt);
            }
        });

        btn_abonar.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btn_abonar.setText("Abonar todo");
        btn_abonar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_abonarActionPerformed(evt);
            }
        });

        jbox_Fondos.setFont(new java.awt.Font("Segoe UI", 1, 14)); // NOI18N
        jbox_Fondos.setForeground(new java.awt.Color(51, 51, 51));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(btn_guardar, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(btn_limpiar1, javax.swing.GroupLayout.PREFERRED_SIZE, 138, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(btn_abonar)
                    .addComponent(txt_abono))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jbox_Fondos, javax.swing.GroupLayout.PREFERRED_SIZE, 383, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_abonar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_abono, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jbox_Fondos, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(btn_guardar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_limpiar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        lbl_cliente1.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lbl_cliente1.setForeground(new java.awt.Color(0, 102, 102));
        lbl_cliente1.setText("Facturas");

        lbl_total_saldo1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_total_saldo1.setForeground(new java.awt.Color(204, 0, 0));
        lbl_total_saldo1.setText("Facturas a abonar");

        jLabel11.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel11.setForeground(new java.awt.Color(51, 51, 51));
        jLabel11.setText("Total abonos");

        jLabel7.setFont(new java.awt.Font("Segoe UI", 0, 18)); // NOI18N
        jLabel7.setForeground(new java.awt.Color(51, 51, 51));
        jLabel7.setText("Total facturas");

        lbl_total_factura_filtro.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lbl_total_factura_filtro.setForeground(new java.awt.Color(0, 102, 204));
        lbl_total_factura_filtro.setText("total");

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(51, 51, 51));
        jLabel13.setText("Total saldo");

        lbl_total_saldo_filtro.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_total_saldo_filtro.setForeground(new java.awt.Color(204, 0, 0));
        lbl_total_saldo_filtro.setText("total");

        lbl_total_abonos_filtro.setFont(new java.awt.Font("Segoe UI", 1, 18)); // NOI18N
        lbl_total_abonos_filtro.setForeground(new java.awt.Color(0, 204, 0));
        lbl_total_abonos_filtro.setText("total");

        jtabla_facturas.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jtabla_facturas.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla_facturas.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jtabla_facturas);

        jtabla_abonos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jtabla_abonos.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla_abonos.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(jtabla_abonos);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_cliente1)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbl_total_factura_filtro)
                                .addGap(18, 18, 18)
                                .addComponent(jLabel11)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbl_total_abonos_filtro))
                            .addComponent(lbl_total_saldo1))
                        .addGap(18, 18, 18)
                        .addComponent(jLabel13)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_total_saldo_filtro)))
                .addContainerGap(717, Short.MAX_VALUE))
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addComponent(jScrollPane2)
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lbl_cliente1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 198, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(lbl_total_saldo1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(1, 1, 1)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lbl_total_factura_filtro)
                    .addComponent(jLabel11)
                    .addComponent(lbl_total_abonos_filtro)
                    .addComponent(jLabel13)
                    .addComponent(lbl_total_saldo_filtro))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txt_abonoFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_abonoFocusGained
        if (!txt_abono.getText().equals("")) {
            String texto = metodos.EliminaCaracteres(txt_abono.getText(), ",");
            txt_abono.setText(texto);
        }
    }//GEN-LAST:event_txt_abonoFocusGained

    private void txt_abonoFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_txt_abonoFocusLost
        if (!txt_abono.getText().equals("")) {
            try {
                double to = Double.parseDouble(txt_abono.getText());
                String nuevo = metodos.formateador().format(to);
                txt_abono.setText(nuevo);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(this, "Por favor verifique se que halla ingresado un valor correcto");
            }
        }
    }//GEN-LAST:event_txt_abonoFocusLost

    private void txt_abonoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_abonoKeyPressed
        char num = evt.getKeyChar();
        if ((num == KeyEvent.VK_ENTER)) {
            btn_guardar.requestFocus();
        }
    }//GEN-LAST:event_txt_abonoKeyPressed

    private void txt_abonoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_abonoKeyTyped
        char num = evt.getKeyChar();
        DB_consultas_R_D.validar_numeros(evt, num);
    }//GEN-LAST:event_txt_abonoKeyTyped

    private void btn_guardarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_guardarActionPerformed
        if (txt_abono.getText().isEmpty() || Double.parseDouble(metodos.EliminaCaracteres(txt_abono.getText(), ",")) <= 0) {
            txt_abono.setBackground(Color.pink);
            JOptionPane.showMessageDialog(this, "El valor ingresado no puede estar vacio, ser 0 o menor de 0\nVerifique los campos de abono y descuento");
        } else {
            int cantidad_facturas = modelo_facturas.getRowCount();
            int cantidad_abonos = modelo_abonos.getRowCount();
            if (validaSaldo(cantidad_facturas, cantidad_abonos)) {

                if (cantidad_abonos > 0) {

                    pagar_prestamos(jtabla_abonos, cantidad_abonos);

                } else {

                    pagar_prestamos(jtabla_facturas, cantidad_facturas);

                }

            }
        }
    }//GEN-LAST:event_btn_guardarActionPerformed
    public void pagar_prestamos(JTable tabla, int cantidad) {

        double totalAbonado = Double.parseDouble(metodos.EliminaCaracteres(txt_abono.getText(), ","));
        Abonos_prestamos abono = null;
        DBabonos_prestamos dbabono = null;
        int fila = 0;
        do {
            abono = new Abonos_prestamos();
            dbabono = new DBabonos_prestamos();
            double saldo_factura_actual = Double.parseDouble(metodos.EliminaCaracteres("" + tabla.getValueAt(fila, 4), ","));

            abono.setId(Integer.parseInt(DB_consultas_R_D.cargarId("abonos_prestamos")));
            abono.setId_user(frm_main.id_user);
            abono.setId_prestamo(Integer.parseInt("" + tabla.getValueAt(fila, 0)));

            if (totalAbonado >= saldo_factura_actual) {

                abono.setAbono(saldo_factura_actual);
                totalAbonado -= saldo_factura_actual;
            } else {
                abono.setAbono(totalAbonado);
                totalAbonado -= saldo_factura_actual;
            }

            //fondo
            try {
                abono.setId_fondo(jbox_Fondos.getItemAt(jbox_Fondos.getSelectedIndex()).getId());

            } catch (Exception e) {
                abono.setId_fondo(Fondos.TraerPredeterminado());
            }
            
            int dia, mes, ano;
            ano = jdate_fecha_creacion.getCalendar().get(Calendar.YEAR);
            mes = jdate_fecha_creacion.getCalendar().get(Calendar.MARCH) + 1;
            dia = jdate_fecha_creacion.getCalendar().get(Calendar.DAY_OF_MONTH);
            abono.setFecha(ano + "-" + mes + "-" + dia);
            dbabono.Guardar(abono);
            fila++;
        } while (totalAbonado > 0 && fila < cantidad);
        frm_prestamos.btn_actualizar.doClick();
        this.dispose();
    }

    public boolean validaSaldo(int cant_facturas, int cant_abonos) {

        double saldo = Double.parseDouble(metodos.EliminaCaracteres(lbl_total_saldo.getText(), ","));
        double abono = Double.parseDouble(metodos.EliminaCaracteres(txt_abono.getText(), ","));
        if (cant_abonos > 0) {
            saldo = Double.parseDouble(metodos.EliminaCaracteres(lbl_total_saldo_filtro.getText(), ","));
        }
        if (abono > saldo) {
            JOptionPane.showMessageDialog(this, "El abono ingresado supera el saldo de la factura \nVerifique los valores ingresados");
            txt_abono.setText("");
            txt_abono.requestFocus();
            return false;
        }
        return true;
    }
    private void btn_limpiar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_limpiar1ActionPerformed
        limpiar();
    }//GEN-LAST:event_btn_limpiar1ActionPerformed

    private void btn_guardarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_btn_guardarKeyPressed
        char num = evt.getKeyChar();
        if ((num == KeyEvent.VK_ENTER)) {
            btn_guardarActionPerformed(null);
        }
    }//GEN-LAST:event_btn_guardarKeyPressed

    private void btn_abonarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_abonarActionPerformed
        int cantidad_abonos = modelo_abonos.getRowCount();
        if (cantidad_abonos > 0) {
            txt_abono.setText(lbl_total_saldo_filtro.getText());
        } else {
            txt_abono.setText(lbl_total_saldo.getText());
        }

    }//GEN-LAST:event_btn_abonarActionPerformed
    public void limpiar() {
        txt_abono.setText("");
        txt_abono.setBackground(Color.white);
        txt_abono.requestFocus();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jd_abonar_prestamos_cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jd_abonar_prestamos_cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jd_abonar_prestamos_cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jd_abonar_prestamos_cliente.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jd_abonar_prestamos_cliente dialog = new jd_abonar_prestamos_cliente(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_abonar;
    public static javax.swing.JButton btn_guardar;
    public static javax.swing.JButton btn_limpiar1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JComboBox<modelos.Fondos> jbox_Fondos;
    public static com.toedter.calendar.JDateChooser jdate_fecha_creacion;
    public static javax.swing.JTable jtabla_abonos;
    public static javax.swing.JTable jtabla_facturas;
    public static javax.swing.JLabel lbl_cedula;
    public static javax.swing.JLabel lbl_cliente;
    public static javax.swing.JLabel lbl_cliente1;
    public static javax.swing.JLabel lbl_id_cliente;
    public static javax.swing.JLabel lbl_total_abonos;
    public static javax.swing.JLabel lbl_total_abonos_filtro;
    public static javax.swing.JLabel lbl_total_factura;
    public static javax.swing.JLabel lbl_total_factura_filtro;
    public static javax.swing.JLabel lbl_total_saldo;
    public static javax.swing.JLabel lbl_total_saldo1;
    public static javax.swing.JLabel lbl_total_saldo_filtro;
    public static javax.swing.JTextField txt_abono;
    // End of variables declaration//GEN-END:variables
}

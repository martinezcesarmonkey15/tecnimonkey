/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReportesCodigo;

import Metodos.ConvertToFraction;
import Metodos.ExportarExcel;
import Metodos.metodos;
import conexiondb.ConsultasSQL;
import conexiondb.DB_consultas_R_D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.sql.ResultSet;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;

/**
 *
 * @author Monkeyelgrande
 */
public class jd_Balance_productosDiario extends javax.swing.JDialog {

    /**
     * Creates new form jd_Ventas_diarias
     */
    static DefaultTableModel modeloBalance = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return false; //Con esto conseguimos que la tabla no se pueda editar
        }
    };

    DecimalFormat formatea = new DecimalFormat("###,###.######");
    TableColumnModel columnModelBalance = null;

    public jd_Balance_productosDiario(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        this.setLocationRelativeTo(parent);
        poner_fechas();
        columnModelBalance = jtabla.getColumnModel();
        metodos.addEscapeListenerWindowDialog(this);
        Metodos.metodos.TablaAptaParaBusquedaAndSSM(jtabla);
        btn_consultar.doClick();
    }

    public void LimpiarModelos() {
        try {
            for (int i = 0; i < jtabla.getRowCount(); i++) {
                modeloBalance.removeRow(i);
                i -= 1;
            }
        } catch (Exception e) {
        }
    }

    public void poner_fechas() {
        try {
            Calendar fecha = new GregorianCalendar();
            jdate_fecha1.setCalendar(fecha);
        } catch (Exception e) {
        }
    }

    public void TamanosTablaVentas(TableColumnModel cm) {
        cm.getColumn(0).setPreferredWidth(100);
        cm.getColumn(1).setPreferredWidth(400);
        cm.getColumn(2).setPreferredWidth(100);
        cm.getColumn(3).setPreferredWidth(100);
        cm.getColumn(4).setPreferredWidth(100);
        cm.getColumn(5).setPreferredWidth(100);
        cm.getColumn(6).setPreferredWidth(100);
        cm.getColumn(7).setPreferredWidth(100);
        cm.getColumn(8).setPreferredWidth(100);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpop_1 = new javax.swing.JPopupMenu();
        jmenu_VerFactura1 = new javax.swing.JMenuItem();
        jpop_2 = new javax.swing.JPopupMenu();
        jmenu_2 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jdate_fecha1 = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        btn_consultar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtabla = new org.jdesktop.swingx.JXTable();
        txt_Filtro = new javax.swing.JTextField();
        jButton1 = new javax.swing.JButton();

        jmenu_VerFactura1.setText("Ver factura");
        jpop_1.add(jmenu_VerFactura1);

        jmenu_2.setText("jMenuItem1");
        jpop_2.add(jmenu_2);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Ingreso productos diarios");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jdate_fecha1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Fecha:");

        btn_consultar.setBackground(new java.awt.Color(37, 116, 169));
        btn_consultar.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_consultar.setForeground(new java.awt.Color(255, 255, 255));
        btn_consultar.setMnemonic('r');
        btn_consultar.setText("Consultar");
        btn_consultar.setToolTipText("ATL+R");
        btn_consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_consultarActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(34, 49, 63));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Tabla de balance por producto", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(255, 255, 255))); // NOI18N

        jtabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jtabla.setRowHeight(32);
        jtabla.setSelectionBackground(new java.awt.Color(0, 153, 153));
        jScrollPane1.setViewportView(jtabla);

        txt_Filtro.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_Filtro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_FiltroKeyTyped(evt);
            }
        });

        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("Exportar a excel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 1258, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(txt_Filtro, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(txt_Filtro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jButton1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 473, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jdate_fecha1, javax.swing.GroupLayout.PREFERRED_SIZE, 242, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_consultar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jLabel1)
                    .addComponent(btn_consultar, javax.swing.GroupLayout.DEFAULT_SIZE, 32, Short.MAX_VALUE)
                    .addComponent(jdate_fecha1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public Date sumarRestarDiasFecha(Date fecha, int dias) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(fecha); // Configuramos la fecha que se recibe
        calendar.add(Calendar.DAY_OF_YEAR, dias); // numero de días a añadir, o restar en caso de días<0
        return calendar.getTime(); // Devuelve el objeto Date con los nuevos días añadidos

    }
    private void btn_consultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_consultarActionPerformed
        LimpiarModelos();
        String fecha1 = "";
//        String fecha2 = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        modeloBalance.setColumnIdentifiers(new Object[]{"Codigo_barras", "Descripción", "Saldo Ayer", "Ingresos", "Devoluciones", "Ventas", "Eliminaciones", "Servicios", "Total Decimal", "Total Fración"});

        try {
            fecha1 = sdf.format(jdate_fecha1.getDate());
            String consulta = "with reporte as (\n"
                    + "	with tabla as (\n"
                    + "		with \n"
                    + "		i_ingresos as (select p.cant_paquete,p.id,p.id_padre, p.codigo_barras, max(p.descripcion) as descripcion, coalesce(sum(i.cantidad),0) as cantidad \n"
                    + "		from ingresos_mercancias_detalle i, ingresos_mercancias_cabecera ic, productos p where i.id_ingreso_cabecera=ic.id and i.id_producto=p.id and ic.fecha = '" + fecha1 + "' group by p.cant_paquete,p.id,p.id_padre,codigo_barras order by codigo_barras),\n"
                    + "\n"
                    + "		i_ingresos_ante as (select p.cant_paquete,p.id,p.id_padre, p.codigo_barras, max(p.descripcion) as descripcion, coalesce(sum(i.cantidad),0) as cantidad \n"
                    + "		from ingresos_mercancias_detalle i, ingresos_mercancias_cabecera ic, productos p where i.id_ingreso_cabecera=ic.id and i.id_producto=p.id and ic.fecha < '" + fecha1 + "' group by p.cant_paquete,p.id,p.id_padre,codigo_barras order by codigo_barras),\n"
                    + "\n"
                    + "		i_devoluciones as (select p.cant_paquete,p.id,p.id_padre, p.codigo_barras, max(p.descripcion) as descripcion, coalesce(sum(dd.cantidad),0) as cantidad \n"
                    + "		from productos p, devoluciones dc, devoluciones_detalles dd where dd.id_cabecera_devolucion=dc.id and dd.id_producto=p.id and dc.fecha = '" + fecha1 + "' group by p.cant_paquete,p.id,p.id_padre, p.codigo_barras),\n"
                    + "\n"
                    + "		i_devoluciones_ante as (select p.cant_paquete,p.id,p.id_padre, p.codigo_barras, max(p.descripcion) as descripcion, coalesce(sum(dd.cantidad),0) as cantidad \n"
                    + "		from productos p, devoluciones dc, devoluciones_detalles dd where dd.id_cabecera_devolucion=dc.id and dd.id_producto=p.id and dc.fecha < '" + fecha1 + "' group by p.cant_paquete,p.id,p.id_padre, p.codigo_barras),\n"
                    + "\n"
                    + "		s_ventas as (select p.cant_paquete,p.id,p.id_padre, p.codigo_barras, max(p.descripcion) as descripcion, coalesce(sum(fd.cantidad),0) as cantidad\n"
                    + "		from facturas_cabeceras fc, facturas_detalles fd, productos p where fd.id_cabecera=fc.id and fd.id_producto=p.id and fc.estado='1' and fc.fecha = '" + fecha1 + "' group by p.cant_paquete,p.id,p.id_padre, p.codigo_barras),\n"
                    + "\n"
                    + "		s_ventas_ante as (select p.cant_paquete,p.id,p.id_padre, p.codigo_barras, max(p.descripcion) as descripcion, coalesce(sum(fd.cantidad),0) as cantidad\n"
                    + "		from facturas_cabeceras fc, facturas_detalles fd, productos p where fd.id_cabecera=fc.id and fd.id_producto=p.id and fc.estado='1' and fc.fecha < '" + fecha1 + "' group by p.cant_paquete,p.id,p.id_padre, p.codigo_barras),\n"
                    + "\n"
                    + "		s_eliminacion as (select p.cant_paquete,p.id,p.id_padre, p.codigo_barras, max(p.descripcion) as descripcion, coalesce(sum(c.cantidad),0) as cantidad \n"
                    + "		from cambios_eliminar c, productos p where c.id_producto=p.id and c.fecha = '" + fecha1 + "' group by p.cant_paquete,p.id,p.id_padre, p.codigo_barras),\n"
                    + "\n"
                    + "		s_eliminacion_ante as (select p.cant_paquete,p.id,p.id_padre, p.codigo_barras, max(p.descripcion) as descripcion, coalesce(sum(c.cantidad),0) as cantidad \n"
                    + "		from cambios_eliminar c, productos p where c.id_producto=p.id and c.fecha < '" + fecha1 + "' group by p.cant_paquete,p.id,p.id_padre, p.codigo_barras),\n"
                    + "\n"
                    + "		s_servicio as (select p.cant_paquete,p.id,p.id_padre, p.codigo_barras, max(p.descripcion) as descripcion, coalesce(sum(c.cantidad),0) as cantidad \n"
                    + "		from servicio_facturaciones c, productos p, servicios s where c.id_servicio=s.id and c.id_producto=p.id and s.estado>2 and s.fecha_entregado = '" + fecha1 + "' group by p.cant_paquete,p.id,p.id_padre, p.codigo_barras),\n"
                    + "\n"
                    + "		s_servicio_ante as (select p.cant_paquete,p.id,p.id_padre, p.codigo_barras, max(p.descripcion) as descripcion, coalesce(sum(c.cantidad),0) as cantidad \n"
                    + "		from servicio_facturaciones c, productos p, servicios s where c.id_servicio=s.id and c.id_producto=p.id and s.estado>2 and  s.fecha_entregado < '" + fecha1 + "' group by p.cant_paquete,p.id,p.id_padre, p.codigo_barras)\n"
                    + "\n"
                    + "\n"
                    + "		select\n"
                    + "\n"
                    + "		coalesce(ii.id,id.id,sv.id,se.id, ss.id,iia.id,ida.id,sva.id,sea.id, ssa.id) as id_producto,\n"
                    + "		coalesce(ii.id_padre,id.id_padre,sv.id_padre,se.id_padre, ss.id_padre,iia.id_padre,ida.id_padre,sva.id_padre,sea.id_padre, ssa.id_padre) as id_padre,\n"
                    + "		coalesce(ii.cant_paquete,id.cant_paquete,sv.cant_paquete,se.cant_paquete, ss.cant_paquete,iia.cant_paquete,ida.cant_paquete,sva.cant_paquete,sea.cant_paquete, ssa.cant_paquete) as cant_paquete,\n"
                    + "		coalesce(ii.codigo_barras,id.codigo_barras,sv.codigo_barras,se.codigo_barras, ss.codigo_barras,iia.codigo_barras,ida.codigo_barras,sva.codigo_barras,sea.codigo_barras, ssa.codigo_barras ) as codigo_barras,\n"
                    + "		coalesce(ii.descripcion,id.descripcion,sv.descripcion,se.descripcion, ss.descripcion,iia.descripcion,ida.descripcion,sva.descripcion,sea.descripcion, ssa.descripcion) as descripcion,\n"
                    + "\n"
                    + "		sum(coalesce(iia.cantidad,0)+coalesce(ida.cantidad,0)-coalesce(sva.cantidad,0)-coalesce(sea.cantidad,0)-coalesce(ssa.cantidad,0)) as saldo_anterior,\n"
                    + "\n"
                    + "		coalesce(sum(ii.cantidad),0) as ingresos, \n"
                    + "		coalesce(sum(id.cantidad),0) as devoluciones, \n"
                    + "		coalesce(sum(sv.cantidad),0) as salidas_ventas,\n"
                    + "		coalesce(sum(se.cantidad),0) as salidas_eliminaciones,\n"
                    + "		coalesce(sum(ss.cantidad),0) as salidas_servicios,\n"
                    + "\n"
                    + "		sum( coalesce(iia.cantidad,0)+coalesce(ida.cantidad,0)-coalesce(sva.cantidad,0)-coalesce(sea.cantidad,0)-coalesce(ssa.cantidad,0) +   coalesce(ii.cantidad,0)+coalesce(id.cantidad,0)-coalesce(sv.cantidad,0)-coalesce(se.cantidad,0)-coalesce(ss.cantidad,0)  ) as total\n"
                    + "\n"
                    + "		from \n"
                    + "\n"
                    + "		i_ingresos as ii\n"
                    + "\n"
                    + "		full outer join i_devoluciones as id on ii.codigo_barras=id.codigo_barras\n"
                    + "		full outer join s_ventas as sv on ii.codigo_barras=sv.codigo_barras\n"
                    + "		full outer join s_eliminacion se on ii.codigo_barras=se.codigo_barras\n"
                    + "		full outer join s_servicio ss on ii.codigo_barras=ss.codigo_barras\n"
                    + "		full outer join i_ingresos_ante as iia on ii.codigo_barras=iia.codigo_barras\n"
                    + "		full outer join i_devoluciones_ante as ida on ii.codigo_barras=ida.codigo_barras\n"
                    + "		full outer join s_ventas_ante as sva on ii.codigo_barras=sva.codigo_barras\n"
                    + "		full outer join s_eliminacion_ante sea on ii.codigo_barras=sea.codigo_barras\n"
                    + "		full outer join s_servicio_ante ssa on ii.codigo_barras=ssa.codigo_barras\n"
                    + "\n"
                    + "		group by coalesce(ii.codigo_barras,id.codigo_barras,sv.codigo_barras,se.codigo_barras, ss.codigo_barras,iia.codigo_barras,ida.codigo_barras,sva.codigo_barras,sea.codigo_barras, ssa.codigo_barras),coalesce(ii.descripcion,id.descripcion,sv.descripcion,se.descripcion, ss.descripcion,iia.descripcion,ida.descripcion,sva.descripcion,sea.descripcion, ssa.descripcion),coalesce(ii.id,id.id,sv.id,se.id, ss.id,iia.id,ida.id,sva.id,sea.id, ssa.id),coalesce(ii.id_padre,id.id_padre,sv.id_padre,se.id_padre, ss.id_padre,iia.id_padre,ida.id_padre,sva.id_padre,sea.id_padre, ssa.id_padre),coalesce(ii.cant_paquete,id.cant_paquete,sv.cant_paquete,se.cant_paquete, ss.cant_paquete,iia.cant_paquete,ida.cant_paquete,sva.cant_paquete,sea.cant_paquete, ssa.cant_paquete)\n"
                    + "		order by coalesce(ii.codigo_barras,id.codigo_barras,sv.codigo_barras,se.codigo_barras, ss.codigo_barras,iia.codigo_barras,ida.codigo_barras,sva.codigo_barras,sea.codigo_barras, ssa.codigo_barras),coalesce(ii.descripcion,id.descripcion,sv.descripcion,se.descripcion, ss.descripcion,iia.descripcion,ida.descripcion,sva.descripcion,sea.descripcion, ssa.descripcion),coalesce(ii.id,id.id,sv.id,se.id, ss.id,iia.id,ida.id,sva.id,sea.id, ssa.id),coalesce(ii.id_padre,id.id_padre,sv.id_padre,se.id_padre, ss.id_padre,iia.id_padre,ida.id_padre,sva.id_padre,sea.id_padre, ssa.id_padre),coalesce(ii.cant_paquete,id.cant_paquete,sv.cant_paquete,se.cant_paquete, ss.cant_paquete,iia.cant_paquete,ida.cant_paquete,sva.cant_paquete,sea.cant_paquete, ssa.cant_paquete))\n"
                    + "\n"
                    + "	select t.id_producto,t.codigo_barras, t.descripcion,round(avg(t.saldo_anterior)::numeric,3) as saldo_anterior,t.ingresos,t.devoluciones,t.salidas_ventas,t.salidas_eliminaciones, t.salidas_servicios, round(avg(t.total)::numeric,3) as total,t.id_padre,t.cant_paquete,\n"
                    + "\n"
                    + "	coalesce(trunc(((total*cant_paquete)+(select total from tabla where t.id_producto=id_padre))/cant_paquete),(((select cant_paquete from tabla where id_producto=t.id_padre)*(select total from tabla where id_producto=t.id_padre))+total),total) as nuevo_total\n"
                    + "\n"
                    + "	from tabla t\n"
                    + "\n"
                    + "	group by id_producto,t.codigo_barras, t.descripcion,t.ingresos,t.devoluciones,t.salidas_ventas,t.salidas_eliminaciones,t.salidas_servicios,t.total,t.id_padre,t.cant_paquete order by id_producto\n"
                    + "	)\n"
                    + "select id_producto, r.codigo_barras, r.descripcion, total as total_viejo, r.id_padre, r.cant_paquete, nuevo_total,saldo_anterior, ingresos, devoluciones, salidas_ventas, salidas_eliminaciones, salidas_servicios, \n"
                    + "\n"
                    + "round((coalesce((select nuevo_total-(select nuevo_total*cant_paquete from reporte where id_producto=r.id_padre)),nuevo_total))::numeric,3) as total, p.estado\n"
                    + "\n"
                    + "\n"
                    + "from reporte r, productos p where r.id_producto=p.id and p.estado=1 order by codigo_barras";
            System.out.println(consulta);
            ResultSet rs = DB_consultas_R_D.getTabla(consulta);
            try {

                while (rs.next()) {

                    modeloBalance.addRow(new Object[]{rs.getString("codigo_barras"), rs.getString("descripcion"), metodos.formateador_decimal().format(rs.getDouble("saldo_anterior")),
                        metodos.formateador_decimal().format(rs.getDouble("ingresos")), metodos.formateador_decimal().format(rs.getDouble("devoluciones")),
                        metodos.formateador_decimal().format(rs.getDouble("salidas_ventas")), metodos.formateador_decimal().format(rs.getDouble("salidas_eliminaciones")),
                        metodos.formateador_decimal().format(rs.getDouble("salidas_servicios")), metodos.formateador_decimal().format(rs.getDouble("total")), new ConvertToFraction(rs.getDouble("total"))});
                }
                rs.close();
                // asigna el modelo a la tabla
                jtabla.setModel(modeloBalance);
                TamanosTablaVentas(columnModelBalance);
            } catch (Exception e) {
                System.out.println(e);
            }

        } catch (Exception e) {
            JOptionPane.showMessageDialog(this, "Verifique las fechas", "Alerta", WIDTH);
        }

    }//GEN-LAST:event_btn_consultarActionPerformed
    private TableRowSorter trsFiltro;

    private void txt_FiltroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_FiltroKeyTyped
        txt_Filtro.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                repaint();
                trsFiltro.setRowFilter(RowFilter.regexFilter("(?i)" + txt_Filtro.getText(), 1));
            }
        });
        trsFiltro = new TableRowSorter(jtabla.getModel());
        jtabla.setRowSorter(trsFiltro);
    }//GEN-LAST:event_txt_FiltroKeyTyped

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ExportarExcel obj;

        try {
            obj = new ExportarExcel();
            obj.exportarExcel(jtabla);
        } catch (IOException ex) {
            System.out.println("" + ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jd_Balance_productosDiario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jd_Balance_productosDiario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jd_Balance_productosDiario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jd_Balance_productosDiario.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>


        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jd_Balance_productosDiario dialog = new jd_Balance_productosDiario(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_consultar;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private com.toedter.calendar.JDateChooser jdate_fecha1;
    private javax.swing.JMenuItem jmenu_2;
    private javax.swing.JMenuItem jmenu_VerFactura1;
    private javax.swing.JPopupMenu jpop_1;
    private javax.swing.JPopupMenu jpop_2;
    private org.jdesktop.swingx.JXTable jtabla;
    private javax.swing.JTextField txt_Filtro;
    // End of variables declaration//GEN-END:variables
}

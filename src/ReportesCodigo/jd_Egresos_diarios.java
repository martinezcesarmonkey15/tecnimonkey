/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReportesCodigo;

import Formularios_internos.jd_ver_devolucion;
import Formularios_internos.jd_ver_in_egre;
import Metodos.metodos;
import conexiondb.ConsultasSQL;
import conexiondb.DB_consultas_R_D;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import modelos.Egresos;
import modelos.Egresos;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Monkeyelgrande
 */
public class jd_Egresos_diarios extends javax.swing.JDialog {

    /**
     * Creates new form jd_Ventas_diarias
     */
    static DefaultTableModel modelo_egresos = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return false; //Con esto conseguimos que la tabla no se pueda editar
        }
    };
    static DefaultTableModel modelo_devoluciones = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return false; //Con esto conseguimos que la tabla no se pueda editar
        }
    };
    static DefaultTableModel modelo_prestamos = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return false; //Con esto conseguimos que la tabla no se pueda editar
        }
    };
    static DefaultTableModel modelo4_anticipos_pagos = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return false; //Con esto conseguimos que la tabla no se pueda editar
        }
    };
    DecimalFormat formatea = new DecimalFormat("###,###.##");

    TableColumnModel columnModelVentas = null;
    TableColumnModel columnModelAbonos = null;
    TableColumnModel columnModelPrestamos = null;
    TableColumnModel columnModelAnticiposPagos = null;
    Egresos oe = new Egresos();

    public jd_Egresos_diarios(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        columnModelVentas = jtabla_1.getColumnModel();
        columnModelAbonos = jtabla_2.getColumnModel();
        columnModelPrestamos = jtabla_3.getColumnModel();
        columnModelAnticiposPagos = jtabla_4.getColumnModel();
        setTitle("Egresos");
        this.setLocationRelativeTo(parent);
        metodos.addEscapeListenerWindowDialog(this);
        Calendar fecha = new GregorianCalendar();
        jdate_buscar.setCalendar(fecha);
        doble_clic_tablas();
    }

    public void TamanosTablaVentas(TableColumnModel cm) {
        cm.getColumn(0).setPreferredWidth(15);
        cm.getColumn(1).setPreferredWidth(100);
        cm.getColumn(2).setPreferredWidth(80);
    }

    public void doble_clic_tablas() {
        jtabla_1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    verGastos();

                }
            }
        });
        jtabla_2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.getClickCount() == 2) {

                    ver_devoluciones();
                }
            }
        });

    }

    public void verGastos() {
        int fila = jtabla_1.getSelectedRow();
        jd_ver_in_egre frm = new jd_ver_in_egre();
        if (fila < 0) {
            JOptionPane.showMessageDialog(this, "Seleccione un registro");
        } else {
            String id = "" + jtabla_1.getValueAt(fila, 0);
            Egresos g = new Egresos();
            g = Egresos.traer_egreso(id);

            jd_ver_in_egre.lbl_user.setText(g.getNombre_user());
            jd_ver_in_egre.lbl_nombre_contacto.setText(g.getNombre_contacto());
            jd_ver_in_egre.lbl_cedula_contacto.setText(g.getCedula_contacto());
            jd_ver_in_egre.lbl_total.setText("$ " + formatea.format(g.getTotal()));
            jd_ver_in_egre.lbl_fecha.setText(g.getFecha());
            jd_ver_in_egre.lbl_hora.setText(g.getHora());
            jd_ver_in_egre.jtxa_descripcion.setText(g.getDescripcion());
            jd_ver_in_egre.lbl_cuenta_nombre.setText(g.getNombre_cuenta());

        }
        jd_ver_in_egre.lbl_cuenta_nombre.setVisible(true);
        jd_ver_in_egre.lbl_cuenta_titulo.setVisible(true);

        frm.show();
    }

    public void ver_devoluciones() {
        jd_ver_devolucion frm = new jd_ver_devolucion(null, rootPaneCheckingEnabled);

        int fila = jtabla_2.getSelectedRow();
        if (fila < 0) {
            JOptionPane.showMessageDialog(this, "Seleccione un registro");
        } else {
            String id = "" + jtabla_2.getValueAt(fila, 0);
            ResultSet rs;
            rs = DB_consultas_R_D.getTabla("select d.id,id_factura,c.nombre as cliente,u.nombre,d.Total,d.Fecha,d.Hora "
                    + "from devoluciones d, users u,contactos c, facturas_cabeceras f "
                    + "where d.id_user=u.id and f.id_contacto=c.id and d.id_factura=f.id and d.id=" + id);
            try {
                while (rs.next()) {
                    jd_ver_devolucion.lbl_cliente.setText(rs.getString("cliente"));
                    jd_ver_devolucion.lbl_fecha.setText(rs.getString("fecha"));
                    jd_ver_devolucion.lbl_hora.setText(rs.getString("hora"));
                    jd_ver_devolucion.lbl_id_devolucion.setText(rs.getString("id"));
                    jd_ver_devolucion.lbl_id_factura.setText(rs.getString("id_factura"));
                    jd_ver_devolucion.lbl_total_devuelto.setText(formatea.format(rs.getDouble("total")));
                    jd_ver_devolucion.lbl_user_devolucion.setText(rs.getString("nombre"));
                }
                rs.close();

            } catch (SQLException ex) {
                System.out.println(ex);
            }

            rs = DB_consultas_R_D.getTabla("select p.codigo_barras, p.descripcion, d.cantidad, d.valor_unitario, d.total "
                    + "from devoluciones_detalles d, productos p where d.id_producto=p.id and d.id_cabecera_devolucion=" + jd_ver_devolucion.lbl_id_devolucion.getText());

            DefaultTableModel modelo_ver_devo = new DefaultTableModel() {
                @Override
                public boolean isCellEditable(int fila, int columna) {
                    return false; //Con esto conseguimos que la tabla no se pueda editar
                }
            };
            modelo_ver_devo.setColumnIdentifiers(new Object[]{"CODIGO", "DESCRIPCIÓN", "CANTIDAD", "PRECIO", "TOTAL"});
            try {
                while (rs.next()) {
                    modelo_ver_devo.addRow(new Object[]{rs.getString("codigo_barras"), rs.getString("descripcion"),
                        rs.getString("cantidad"), "" + formatea.format(rs.getDouble("valor_unitario")), formatea.format(rs.getDouble("total"))});
                }
                rs.close();
                jd_ver_devolucion.jtabla_ver_devoluciones.setModel(modelo_ver_devo);
            } catch (SQLException ex) {
                Logger.getLogger(jd_Egresos_diarios.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        frm.show();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpop_1 = new javax.swing.JPopupMenu();
        jmenu_VerFactura1 = new javax.swing.JMenuItem();
        jpop_2 = new javax.swing.JPopupMenu();
        jmenu_2 = new javax.swing.JMenuItem();
        jPanel1 = new javax.swing.JPanel();
        jdate_buscar = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        btn_consultar = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtabla_1 = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        lbl_Total_Gastos = new javax.swing.JLabel();
        btn_imprimir_egresos = new javax.swing.JButton();
        jPanel3 = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtabla_2 = new javax.swing.JTable();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        lbl_Total_Devoluciones = new javax.swing.JLabel();
        lbl_Total_3 = new javax.swing.JLabel();
        lbl_Total_egresos = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtabla_3 = new javax.swing.JTable();
        jLabel6 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        lbl_Total_Prestamos = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jScrollPane4 = new javax.swing.JScrollPane();
        jtabla_4 = new javax.swing.JTable();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        lbl_Total_anticipos_y_pagos = new javax.swing.JLabel();

        jmenu_VerFactura1.setText("Ver factura");
        jpop_1.add(jmenu_VerFactura1);

        jmenu_2.setText("jMenuItem1");
        jpop_2.add(jmenu_2);

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Egresos diarios");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        jdate_buscar.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel1.setText("Seleccione fecha:");

        btn_consultar.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        btn_consultar.setMnemonic('r');
        btn_consultar.setText("Consultar");
        btn_consultar.setToolTipText("ATL+R");
        btn_consultar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_consultarActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(255, 153, 153));

        jtabla_1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jtabla_1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla_1.setComponentPopupMenu(jpop_1);
        jtabla_1.setRowHeight(22);
        jtabla_1.setSelectionBackground(new java.awt.Color(0, 153, 153));
        jtabla_1.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jtabla_1);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Egresos");

        jLabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel3.setText("Total egresos");

        lbl_Total_Gastos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbl_Total_Gastos.setText("0");

        btn_imprimir_egresos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/print_pequeno.png"))); // NOI18N
        btn_imprimir_egresos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_imprimir_egresosActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 494, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(btn_imprimir_egresos))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jLabel3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbl_Total_Gastos)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel2)
                    .addComponent(btn_imprimir_egresos))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 198, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel3)
                    .addComponent(lbl_Total_Gastos))
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(204, 255, 204));

        jtabla_2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jtabla_2.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla_2.setComponentPopupMenu(jpop_2);
        jtabla_2.setRowHeight(22);
        jtabla_2.setSelectionBackground(new java.awt.Color(0, 153, 153));
        jtabla_2.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(jtabla_2);

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Devoluciones");

        jLabel5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel5.setText("Total devoluciones");

        lbl_Total_Devoluciones.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbl_Total_Devoluciones.setText("0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel4)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbl_Total_Devoluciones)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(lbl_Total_Devoluciones))
                .addContainerGap())
        );

        lbl_Total_3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbl_Total_3.setText("Total Egresos:");

        lbl_Total_egresos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbl_Total_egresos.setForeground(new java.awt.Color(153, 0, 0));
        lbl_Total_egresos.setText("0");

        jPanel4.setBackground(new java.awt.Color(204, 255, 255));

        jtabla_3.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jtabla_3.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla_3.setComponentPopupMenu(jpop_2);
        jtabla_3.setRowHeight(22);
        jtabla_3.setSelectionBackground(new java.awt.Color(0, 153, 153));
        jtabla_3.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane3.setViewportView(jtabla_3);

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Prestamos");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel7.setText("Total prestamos");

        lbl_Total_Prestamos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbl_Total_Prestamos.setText("0");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane3, javax.swing.GroupLayout.DEFAULT_SIZE, 460, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel6)
                            .addGroup(jPanel4Layout.createSequentialGroup()
                                .addComponent(jLabel7)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbl_Total_Prestamos)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel7)
                    .addComponent(lbl_Total_Prestamos))
                .addContainerGap())
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 102));

        jtabla_4.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jtabla_4.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla_4.setComponentPopupMenu(jpop_1);
        jtabla_4.setRowHeight(22);
        jtabla_4.setSelectionBackground(new java.awt.Color(0, 153, 153));
        jtabla_4.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane4.setViewportView(jtabla_4);

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Anticipos y pagos de facturas");

        jLabel9.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel9.setText("Total anticipos y pagos de facturas");

        lbl_Total_anticipos_y_pagos.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbl_Total_anticipos_y_pagos.setText("0");

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 494, Short.MAX_VALUE)
                    .addGroup(jPanel5Layout.createSequentialGroup()
                        .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel8)
                            .addGroup(jPanel5Layout.createSequentialGroup()
                                .addComponent(jLabel9)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(lbl_Total_anticipos_y_pagos)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel8)
                .addGap(22, 22, 22)
                .addComponent(jScrollPane4, javax.swing.GroupLayout.DEFAULT_SIZE, 156, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel9)
                    .addComponent(lbl_Total_anticipos_y_pagos))
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jdate_buscar, javax.swing.GroupLayout.PREFERRED_SIZE, 163, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_consultar)
                        .addGap(18, 18, 18)
                        .addComponent(lbl_Total_3)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbl_Total_egresos)
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jLabel1)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(lbl_Total_3)
                            .addComponent(lbl_Total_egresos))
                        .addComponent(jdate_buscar, javax.swing.GroupLayout.DEFAULT_SIZE, 31, Short.MAX_VALUE))
                    .addComponent(btn_consultar, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(11, 11, 11)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public void LimpiarModelos() {
        try {
            for (int i = 0; i < modelo_egresos.getRowCount(); i++) {
                modelo_egresos.removeRow(i);
                i -= 1;
            }
        } catch (Exception e) {
        }

        try {
            for (int i = 0; i < modelo_devoluciones.getRowCount(); i++) {
                modelo_devoluciones.removeRow(i);
                i -= 1;
            }
        } catch (Exception e) {
        }

        try {
            for (int i = 0; i < modelo_prestamos.getRowCount(); i++) {
                modelo_prestamos.removeRow(i);
                i -= 1;
            }
        } catch (Exception e) {
        }

        try {
            for (int i = 0; i < modelo4_anticipos_pagos.getRowCount(); i++) {
                modelo4_anticipos_pagos.removeRow(i);
                i -= 1;
            }
        } catch (Exception e) {
        }

    }
    private void btn_consultarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_consultarActionPerformed
        LimpiarModelos();
        String fecha = "";
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        fecha = sdf.format(jdate_buscar.getDate());

        ResultSet rs = DB_consultas_R_D.getTabla(ConsultasSQL.ConsultaEgresosPorFecha_reporte(fecha));

        modelo_egresos.setColumnIdentifiers(new Object[]{"ID egreso", "Contacto", "Cuenta", "Total"});
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                modelo_egresos.addRow(new Object[]{rs.getString("id"), rs.getString("contacto"), rs.getString("nombre"), "$ " + formatea.format(rs.getDouble("total"))});
            }
            rs.close();
            // asigna el modelo a la tabla
            jtabla_1.setModel(modelo_egresos);
            TamanosTablaVentas(columnModelVentas);
        } catch (Exception e) {
            System.out.println(e);
        }

        rs = DB_consultas_R_D.getTabla(ConsultasSQL.ConsultaDevolucionesPorFecha_reporte(fecha));

        modelo_devoluciones.setColumnIdentifiers(new Object[]{"ID devolucion", "Nombre cliente", "Total"});
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                modelo_devoluciones.addRow(new Object[]{rs.getString("id"), rs.getString("nombre"), "$ " + formatea.format(rs.getDouble("total"))});
            }
            rs.close();
            // asigna el modelo a la tabla
            jtabla_2.setModel(modelo_devoluciones);
            TamanosTablaVentas(columnModelAbonos);

        } catch (Exception e) {
            System.out.println(e);
        }

        rs = DB_consultas_R_D.getTabla(ConsultasSQL.ConsultaPrestamosPorFecha_reporte(fecha));

        modelo_prestamos.setColumnIdentifiers(new Object[]{"ID prestamo", "Contacto", "Total"});
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                modelo_prestamos.addRow(new Object[]{rs.getString("id"), rs.getString("nombre"), "$ " + formatea.format(rs.getDouble("total"))});
            }
            rs.close();
            // asigna el modelo a la tabla
            jtabla_3.setModel(modelo_prestamos);
            TamanosTablaVentas(columnModelPrestamos);

        } catch (Exception e) {
            System.out.println(e);
        }

        // egresos por anticipos y pagos de factturas
        double anticipos_pagos = 0;
        String consulta = "select p.total, p.fecha,fo.nombre as fondo from pagos_facturas p, fondos fo where p.tipo_pago=fo.id and p.fecha='" + fecha + "' \n"
                + "UNION \n"
                + "select total,fecha,'anticipo' as tipo from anticipos where fecha='" + fecha + "'\n"
                + "order by fecha";
        System.out.println(consulta);
        rs = DB_consultas_R_D.getTabla(consulta);

        modelo4_anticipos_pagos.setColumnIdentifiers(new Object[]{"Tipo", "Total", "Fecha"});
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                modelo4_anticipos_pagos.addRow(new Object[]{rs.getString("fondo"), "$ " + formatea.format(rs.getDouble("total")), rs.getDate("fecha")});
                anticipos_pagos += rs.getDouble("total");
            }
            rs.close();
            // asigna el modelo a la tabla
            jtabla_4.setModel(modelo4_anticipos_pagos);
            TamanosTablaVentas(columnModelAnticiposPagos);

        } catch (Exception e) {
            System.out.println(e);
        }

        double Tgastos, Tdevoluciones, TPrestamos;
        Tgastos = DB_consultas_R_D.SumarCampoDoubleConSQL("select sum(total) from egresos where fecha='" + fecha + "'");
        Tdevoluciones = DB_consultas_R_D.SumarCampoDoubleConSQL("select sum(total) from devoluciones where fecha='" + fecha + "'");
        TPrestamos = DB_consultas_R_D.SumarCampoDoubleConSQL("select sum(total) from prestamos where fecha='" + fecha + "'");

        lbl_Total_Gastos.setText("$ " + formatea.format(Tgastos));
        lbl_Total_Devoluciones.setText("$ " + formatea.format(Tdevoluciones));
        lbl_Total_Prestamos.setText("$ " + formatea.format(TPrestamos));
        lbl_Total_anticipos_y_pagos.setText("$ " + formatea.format(anticipos_pagos));

        lbl_Total_egresos.setText("$ " + formatea.format(Tgastos + Tdevoluciones + TPrestamos + anticipos_pagos));
    }//GEN-LAST:event_btn_consultarActionPerformed

    private void btn_imprimir_egresosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_imprimir_egresosActionPerformed
        String cad = metodos.TamanoHoja("/src/reportes/Egresos_carta.jrxml", "/src/reportes/Egresos_oficio.jrxml");
        if (!cad.equals("")) {
            Connection cn = DB_consultas_R_D.getConexion();
            JasperReport report = null;
            Map p = new HashMap();
            p.put("fecha1", jdate_buscar.getDate());
            p.put("fecha2", jdate_buscar.getDate());
            p.put("SUBREPORT_DIR", new File("").getAbsolutePath() + "/src/reportes/");

            try {
                try {
                    report = JasperCompileManager.compileReport(cad);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, e);
                }
                JasperPrint print = JasperFillManager.fillReport(report, p, cn);
                JasperViewer view = new JasperViewer(print, false);
                cn.close();
                JDialog dialog = new JDialog(this);//the owner
                dialog.setContentPane(view.getContentPane());
                dialog.setSize(view.getSize());
                dialog.setModal(true);
                dialog.setLocationRelativeTo(this);
                dialog.setTitle("Reporte de otros egresos");
                dialog.setVisible(true);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_btn_imprimir_egresosActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jd_Egresos_diarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jd_Egresos_diarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jd_Egresos_diarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jd_Egresos_diarios.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jd_Egresos_diarios dialog = new jd_Egresos_diarios(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_consultar;
    private javax.swing.JButton btn_imprimir_egresos;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JScrollPane jScrollPane4;
    private com.toedter.calendar.JDateChooser jdate_buscar;
    private javax.swing.JMenuItem jmenu_2;
    private javax.swing.JMenuItem jmenu_VerFactura1;
    private javax.swing.JPopupMenu jpop_1;
    private javax.swing.JPopupMenu jpop_2;
    private javax.swing.JTable jtabla_1;
    private javax.swing.JTable jtabla_2;
    private javax.swing.JTable jtabla_3;
    private javax.swing.JTable jtabla_4;
    private javax.swing.JLabel lbl_Total_3;
    private javax.swing.JLabel lbl_Total_Devoluciones;
    private javax.swing.JLabel lbl_Total_Gastos;
    private javax.swing.JLabel lbl_Total_Prestamos;
    private javax.swing.JLabel lbl_Total_anticipos_y_pagos;
    private javax.swing.JLabel lbl_Total_egresos;
    // End of variables declaration//GEN-END:variables
}

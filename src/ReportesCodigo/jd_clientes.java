/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ReportesCodigo;

import Formularios_internos.jif_crear_contactos;
import Metodos.metodos;
import conexiondb.ConsultasSQL;
import conexiondb.DB_consultas_R_D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JDialog;
import javax.swing.JOptionPane;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author Monkeyelgrande
 */
public class jd_clientes extends javax.swing.JDialog {

    /**
     * Creates new form jd_buscar_cliente
     */
    public jd_clientes(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        mostrar();
        this.setLocationRelativeTo(parent);
        txt_Filtro.requestFocus();
        metodos.addEscapeListenerWindowDialog(this);

        jtabla_clientes.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    ver_contacto();
                }
            }
        });
    }
    jif_crear_contactos jif_crear_contacto = new jif_crear_contactos();

    public void ver_contacto() {
        jif_crear_contacto = new jif_crear_contactos();
        int fila = jtabla_clientes.getSelectedRow();
        if (fila < 0) {
            JOptionPane.showMessageDialog(this, "Seleccione un registro");
        } else {
            String id = "" + jtabla_clientes.getValueAt(fila, 0);

            ResultSet rs = DB_consultas_R_D.getTabla("select * from contactos where id =" + id);
            try {
                while (rs.next()) {
                    jif_crear_contactos.txt_id_cliente.setText(rs.getString("id"));
                    jif_crear_contactos.txt_nombre.setText(rs.getString("nombre"));
                    jif_crear_contactos.txt_cedula.setText(rs.getString("cedula"));
                    jif_crear_contactos.txt_contacto.setText(rs.getString("contacto"));
                    jif_crear_contactos.txt_contacto2.setText(rs.getString("contacto2"));
                    jif_crear_contactos.txt_direccion.setText(rs.getString("direccion"));
                    jif_crear_contactos.txt_descuento.setText(rs.getString("descuento"));
                    jif_crear_contactos.txt_email.setText(rs.getString("email"));
                    jif_crear_contactos.txt_ciudad.setText(rs.getString("ciudad"));
                    jif_crear_contactos.txt_forma_pago.setText(rs.getString("forma_pago"));
                    jif_crear_contactos.txt_banco_cuenta.setText(rs.getString("cuenta"));
                    jif_crear_contactos.txt_tipo_cuenta.setText(rs.getString("tipo_cuenta"));
                    jif_crear_contactos.txt_Numero_cuenta.setText(rs.getString("numero_cuenta"));
                    jif_crear_contactos.jtxa_observaciones.setText(rs.getString("observaciones"));

                }
                rs.close();

            } catch (SQLException ex) {
                Logger.getLogger(jif_crear_contactos.class.getName()).log(Level.SEVERE, null, ex);
            }

            jif_crear_contacto.txt_nombre.setEditable(false);
            jif_crear_contacto.txt_cedula.setEditable(false);
            jif_crear_contacto.txt_contacto.setEditable(false);
            jif_crear_contacto.txt_contacto2.setEditable(false);
            jif_crear_contacto.txt_direccion.setEditable(false);
            jif_crear_contacto.txt_ciudad.setEditable(false);
            jif_crear_contacto.txt_descuento.setEditable(false);
            jif_crear_contacto.txt_email.setEditable(false);
            jif_crear_contacto.txt_forma_pago.setEditable(false);
            jif_crear_contacto.txt_banco_cuenta.setEditable(false);
            jif_crear_contacto.txt_tipo_cuenta.setEditable(false);
            jif_crear_contacto.txt_Numero_cuenta.setEditable(false);
            jif_crear_contacto.jtxa_observaciones.setEditable(false);
            jif_crear_contacto.btn_guardar.setEnabled(false);
            jif_crear_contacto.btn_limpiar.setEnabled(false);
            jif_crear_contacto.chk_descuento.setEnabled(false);
            jif_crear_contacto.chk_cerrar.setEnabled(false);
            jif_crear_contacto.btn_editar.setVisible(false);

            jif_crear_contacto.show();

            jif_crear_contactos.txt_nombre.requestFocus();

        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        comboFiltro = new javax.swing.JComboBox();
        txt_Filtro = new javax.swing.JTextField();
        btn_imprimir_oi = new javax.swing.JButton();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtabla_clientes = new javax.swing.JTable();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("Clientes");
        setModal(true);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        comboFiltro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        comboFiltro.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "Nombre", "Cedula" }));
        comboFiltro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                comboFiltroActionPerformed(evt);
            }
        });

        txt_Filtro.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txt_Filtro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_FiltroKeyTyped(evt);
            }
        });

        btn_imprimir_oi.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/print_pequeno.png"))); // NOI18N
        btn_imprimir_oi.setMnemonic('p');
        btn_imprimir_oi.setToolTipText("ATL+P");
        btn_imprimir_oi.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_imprimir_oiActionPerformed(evt);
            }
        });

        jPanel2.setBackground(new java.awt.Color(34, 49, 63));

        jtabla_clientes.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jtabla_clientes.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla_clientes.setRowHeight(22);
        jtabla_clientes.setSelectionBackground(new java.awt.Color(0, 153, 153));
        jtabla_clientes.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane1.setViewportView(jtabla_clientes);

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 689, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 373, Short.MAX_VALUE)
                .addContainerGap())
        );

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, 118, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txt_Filtro, javax.swing.GroupLayout.PREFERRED_SIZE, 177, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(btn_imprimir_oi)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_imprimir_oi)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(txt_Filtro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(comboFiltro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    private TableRowSorter trsFiltro;

    private void comboFiltroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_comboFiltroActionPerformed
        txt_Filtro.setText("");
        txt_Filtro.requestFocus();
    }//GEN-LAST:event_comboFiltroActionPerformed

    private void txt_FiltroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_FiltroKeyTyped
        txt_Filtro.addKeyListener(new KeyAdapter() {
            public void keyReleased(final KeyEvent e) {
                String cadena = (txt_Filtro.getText());
                //                txt_Filtro.setText(cadena);
                repaint();
                filtro();
            }
        });
        trsFiltro = new TableRowSorter(jtabla_clientes.getModel());
        jtabla_clientes.setRowSorter(trsFiltro);
    }//GEN-LAST:event_txt_FiltroKeyTyped

    private void btn_imprimir_oiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_imprimir_oiActionPerformed
        String cad = metodos.TamanoHoja("/src/reportes/ClientesCarta.jrxml", "/src/reportes/Clientes.jrxml");
        if (!cad.equals("")) {
            Connection cn = DB_consultas_R_D.getConexion();
            JasperReport report = null;
            Map p = new HashMap();
            p.put("SUBREPORT_DIR", new File("").getAbsolutePath() + "/src/reportes/");

            try {
                try {
                    report = JasperCompileManager.compileReport(cad);
                } catch (Exception e) {
                    JOptionPane.showMessageDialog(this, e);
                }
                JasperPrint print = JasperFillManager.fillReport(report, p, cn);
                JasperViewer view = new JasperViewer(print, false);
                cn.close();
                JDialog dialog = new JDialog(this);//the owner
                dialog.setContentPane(view.getContentPane());
                dialog.setSize(view.getSize());
                dialog.setModal(true);
                dialog.setLocationRelativeTo(this);
                dialog.setTitle("Reporte Clientes");
                dialog.setVisible(true);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_btn_imprimir_oiActionPerformed
    DefaultTableModel modelo = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return false; //Con esto conseguimos que la tabla no se pueda editar
        }
    };

    public void mostrar() {

        ResultSet rs = DB_consultas_R_D.getTabla(ConsultasSQL.ConsultaClientes());
        modelo.setColumnIdentifiers(new Object[]{"id", "Nombre", "Cedula", "Celular", "Ciudad", "Dirección"});
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                modelo.addRow(new Object[]{rs.getString("id"), rs.getString("nombre"), rs.getString("cedula"), rs.getString("contacto"),
                    rs.getString("ciudad"), rs.getString("direccion")});
            }
            rs.close();
            // asigna el modelo a la tabla
            jtabla_clientes.setModel(modelo);
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void filtro() {
        int columnaABuscar = 0;
        switch (comboFiltro.getSelectedItem() + "") {
            case "Nombre":
                columnaABuscar = 1;
                break;
            case "Cedula":
                columnaABuscar = 2;
                break;
        }
        trsFiltro.setRowFilter(RowFilter.regexFilter(txt_Filtro.getText(), columnaABuscar));
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(jd_clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(jd_clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(jd_clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(jd_clientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the dialog */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                jd_clientes dialog = new jd_clientes(new javax.swing.JFrame(), true);
                dialog.addWindowListener(new java.awt.event.WindowAdapter() {
                    @Override
                    public void windowClosing(java.awt.event.WindowEvent e) {
                        System.exit(0);
                    }
                });
                dialog.setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_imprimir_oi;
    private javax.swing.JComboBox comboFiltro;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JTable jtabla_clientes;
    private javax.swing.JTextField txt_Filtro;
    // End of variables declaration//GEN-END:variables
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos;

import conexiondb.DB_consultas_R_D;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.ResultSet;
import java.util.ArrayList;
import modelos.Contactos;
import modelos.ProductoImprimir;

/**
 *
 * @author mktec
 */
public class ImprimirTermica80MM {

    String nombre_negocio, nit, direccion, telefono, no_factura, codigo_factura, fecha_hora, nombre_vendedor, subtotal, descuento, total, tipo, abono, saldo, importe, vuelto;
    Contactos cliente;
    ArrayList<ProductoImprimir> productos;
    double middleHeight = 0.0;

    public ImprimirTermica80MM(String fecha, String no_factura, String codigo_factura, String nombre_vendedor, String subtotal, String descuento, String total,
            Contactos cliente, ArrayList<ProductoImprimir> productos, String tipo, String abono, String saldo, String importe, String vuelto) {

        this.fecha_hora = fecha;
        this.no_factura = no_factura;
        this.codigo_factura = codigo_factura;
        this.nombre_vendedor = nombre_vendedor;
        this.subtotal = subtotal;
        this.descuento = descuento;
        this.total = total;
        this.cliente = cliente;
        this.nombre_vendedor = nombre_vendedor;
        this.productos = productos;
        this.tipo = tipo;
        this.abono = abono;
        this.saldo = saldo;
        this.importe = importe;
        this.vuelto = vuelto;

        middleHeight = productos.size() + 12.0;
        String cadena = "select * from configuraciones";
        ResultSet rs = DB_consultas_R_D.getTabla(cadena);
        try {
            while (rs.next()) {
                this.nombre_negocio = rs.getString("nombre_negocio");
                this.nit = rs.getString("nit_negocio");
                this.telefono = rs.getString("contacto_negocio");
                this.direccion = rs.getString("direccion");
            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public PageFormat getPageFormat(PrinterJob pj) {

        PageFormat pf = pj.defaultPage();
        Paper paper = pf.getPaper();

        double headerHeight = 2.0;
        double footerHeight = 3.0;
        double width = convert_CM_To_PPI(8);      //printer know only point per inch.default value is 72ppi aca se define el ancho de la impresion
        double height = convert_CM_To_PPI(headerHeight + middleHeight + footerHeight);
        paper.setSize(width, height);
        paper.setImageableArea(
                0,
                10,
                width,
                height - convert_CM_To_PPI(1)
        );   //define boarder size    after that print area width is about 180 points

        pf.setOrientation(PageFormat.PORTRAIT);           //select orientation portrait or landscape but for this time portrait
        pf.setPaper(paper);

        return pf;
    }

    protected static double convert_CM_To_PPI(double cm) {
        return toPPI(cm * 0.393600787);
    }

    protected static double toPPI(double inch) {
        return inch * 72d;
    }

    public void imprime() {
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setCopies(1);
        pj.setPrintable(new Printables(), getPageFormat(pj));
        try {
            pj.print();
        } catch (PrinterException ex) {
            ex.printStackTrace();
        }
    }

    public class Printables implements Printable {

        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)
                throws PrinterException {

            int result = NO_SUCH_PAGE;
            if (pageIndex == 0) {

                Graphics2D g2d = (Graphics2D) graphics;

                double width = pageFormat.getImageableWidth();
                g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());

                FontMetrics metrics = g2d.getFontMetrics(new Font("Arial", Font.BOLD, 10));

                int nombrenegocio_posicion = (int) ((width - metrics.stringWidth(nombre_negocio)) / 2);;
                int nit_posicion = (int) ((width - metrics.stringWidth("NIT/RUT. " + nit)) / 2);;
                int direccion_posicion = (int) ((width - metrics.stringWidth(direccion)) / 2);;
                int telefono_posicion = (int) ((width - metrics.stringWidth(telefono)) / 2);;
                int fecha_hora_posicion = (int) ((width - metrics.stringWidth(fecha_hora)) / 2);;
                int no_fac_posicion = (int) ((width - metrics.stringWidth("Factura No. " + no_factura)) / 2);;
                int cod_fac_posicion = (int) ((width - metrics.stringWidth(codigo_factura)) / 2);;
                int tipo_posicion = (int) ((width - metrics.stringWidth(tipo)) / 2);;

                try {
                    /*Draw Header*/
                    int y = 20;
                    int yShift = 15;
                    int yProducto = 7;

                    g2d.setFont(new Font("Arial", Font.BOLD, 12));
                    g2d.drawString("_____________________________________", 12, y);
                    y += yShift;
                    g2d.drawString(nombre_negocio, nombrenegocio_posicion, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 10));
                    g2d.drawString("NIT/RUT. " + nit, nit_posicion, y);
                    y += yShift;
                    g2d.drawString(direccion, direccion_posicion, y);
                    y += yShift;
                    g2d.drawString(telefono, telefono_posicion, y);
                    y += yShift;
                    g2d.drawString(fecha_hora, fecha_hora_posicion, y);
                    g2d.setFont(new Font("Arial", Font.BOLD, 10));
                    y += yShift;
                    g2d.drawString("Factura No. " + no_factura, no_fac_posicion, y);
                    y += yShift;
                    g2d.drawString(codigo_factura, cod_fac_posicion, y);
//                    y += yShift;
                    g2d.drawString(tipo, tipo_posicion, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 12));
                    g2d.drawString("-------------------------------------------------------", 12, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 10));
                    g2d.drawString("Cliente: " + cliente.getNombre(), 12, y);
                    y += yShift;
                    g2d.drawString("Documento: " + cliente.getCedula(), 12, y);
                    y += yShift;
                    g2d.drawString("Dirección: " + cliente.getDireccion(), 12, y);
                    y += yShift;
                    g2d.drawString("Tel.: " + cliente.getContacto(), 12, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 12));
                    g2d.drawString("_____________________________________", 12, y);
                    g2d.setFont(new Font("Arial", Font.BOLD, 8));
                    y += yShift;
                    g2d.drawString("Artículo", 12, y);
                    g2d.drawString("Precio", 120, y);
                    g2d.drawString("Cant.", 155, y);
                    g2d.drawString("Total", 185, y);
                    g2d.setFont(new Font("Arial", Font.BOLD, 12));
                    g2d.drawString("_____________________________________", 10, y);
                    g2d.setFont(new Font("Arial", Font.BOLD, 7)); // aca se define el tamaño del font
                    y += yShift;
                    for (int i = 0; i < productos.size(); i++) {

                        String nombre = productos.get(i).getNombre().toString();
                        String nombre_L1 = productos.get(i).getNombre().toString();
                        String nombre_L2 = "";
                        try {
                            nombre_L1 = nombre.substring(0, 20);
                            try {
                                nombre_L2 = nombre.substring(20, 40);
                            } catch (Exception e) {
                                nombre_L2 = nombre.substring(27, nombre.length());
                            }
                        } catch (Exception e) {

                        }
                        g2d.drawString(nombre_L1, 12, y); // el 12 indica el margen que hay en la izquierda
                        if (!nombre_L2.equals("")) {
                            y += yProducto;
                            g2d.drawString(nombre_L2, 12, y); // el 12 indica el margen que hay en la izquierda

                        }

                        g2d.drawString("$ " + productos.get(i).getPunitario(), 120, y);
                        g2d.drawString(productos.get(i).getCantidad(), 158, y);
                        g2d.drawString("$ " + productos.get(i).getPtotal(), 173, y);
                        y += yShift;
                    }
                    g2d.setFont(new Font("Arial", Font.PLAIN, 12));
                    g2d.drawString("_____________________________________", 12, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 8));
                    g2d.drawString("Subtotal", 120, y);
                    g2d.drawString("$ " + subtotal, 160, y);
                    y += yShift;
                    g2d.drawString("Descuento", 117, y);
                    g2d.drawString("$ " + descuento, 160, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.BOLD, 8));
                    g2d.drawString("TOTAL FACTURA", 80, y);
                    g2d.setFont(new Font("Arial", Font.PLAIN, 8));
                    g2d.drawString("$ " + total, 160, y);
                    y += yShift;

                    if (tipo.equals("Venta")) {

                        g2d.drawString("Importe", 117, y);
                        g2d.drawString("$ " + importe, 160, y);
                        y += yShift;
                        g2d.drawString("Cambio", 117, y);
                        g2d.drawString("$ " + vuelto, 160, y);
                        y += yShift;
                    }

                    if (!tipo.equals("Venta")) {
                        g2d.setFont(new Font("Arial", Font.PLAIN, 8));
                        g2d.drawString("Abono", 121, y);
                        g2d.drawString("$ " + abono, 160, y);
                        y += yShift;
                        g2d.setFont(new Font("Arial", Font.PLAIN, 8));
                        g2d.drawString("Saldo", 121, y);
                        g2d.drawString("$ " + (saldo), 160, y);
                        y += yShift;
                    }
                    g2d.setFont(new Font("Arial", Font.PLAIN, 12));
                    y += yShift;
                    g2d.drawString("***********************************************", 10, y);
                    y += yShift;
                    g2d.drawString("GRACIAS POR SU COMPRA", 35, y);
                    y += yShift;
                    g2d.drawString("***********************************************", 10, y);
                    y += yShift;
                    y += yShift;

                } catch (Exception r) {
                    r.printStackTrace();
                }

                result = PAGE_EXISTS;
            }
            return result;
        }
    }
}

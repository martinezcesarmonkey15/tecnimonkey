/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos;

import conexiondb.DB_consultas_R_D;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.ResultSet;
import java.util.ArrayList;
import modelos.Contactos;
import modelos.ProductoImprimir;

/**
 *
 * @author mktec
 */
public class ImprimirTermica58MM {

    String nombre_negocio, nit, direccion, telefono, no_factura, codigo_factura, fecha_hora, nombre_vendedor, subtotal, descuento, total;
    Contactos cliente;
    ArrayList<ProductoImprimir> productos;
    double middleHeight = 0.0;

    public ImprimirTermica58MM(String fecha, String no_factura, String codigo_factura, String nombre_vendedor, String subtotal, String descuento, String total,
            Contactos cliente, ArrayList<ProductoImprimir> productos) {

        this.fecha_hora = fecha;
        this.no_factura = no_factura;
        this.codigo_factura = codigo_factura;
        this.nombre_vendedor = nombre_vendedor;
        this.subtotal = subtotal;
        this.descuento = descuento;
        this.total = total;
        this.cliente = cliente;
        this.nombre_vendedor = nombre_vendedor;
        this.productos = productos;
        middleHeight = productos.size() + 12.0;
        String cadena = "select * from configuraciones";
        ResultSet rs = DB_consultas_R_D.getTabla(cadena);
        try {
            while (rs.next()) {
                this.nombre_negocio = rs.getString("nombre_negocio");
                this.nit = rs.getString("nit_negocio");
                this.telefono = rs.getString("contacto_negocio");
                this.direccion = rs.getString("direccion");

            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public PageFormat getPageFormat(PrinterJob pj) {

        PageFormat pf = pj.defaultPage();
        Paper paper = pf.getPaper();

        double headerHeight = 2.0;
        double footerHeight = 3.0;
        double width = convert_CM_To_PPI(5.8);      //printer know only point per inch.default value is 72ppi aca se define el ancho de la impresion
        double height = convert_CM_To_PPI(headerHeight + middleHeight + footerHeight);
        paper.setSize(width, height);
        paper.setImageableArea(
                0,
                10,
                width,
                height - convert_CM_To_PPI(1)
        );   //define boarder size    after that print area width is about 180 points

        pf.setOrientation(PageFormat.PORTRAIT);           //select orientation portrait or landscape but for this time portrait
        pf.setPaper(paper);

        return pf;
    }

    protected static double convert_CM_To_PPI(double cm) {
        return toPPI(cm * 0.393600787);
    }

    protected static double toPPI(double inch) {
        return inch * 72d;
    }

    public void imprime() {
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setCopies(1);
        pj.setPrintable(new Printables(), getPageFormat(pj));
        try {
            pj.print();
        } catch (PrinterException ex) {
            ex.printStackTrace();
        }
    }

    public class Printables implements Printable {

        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)
                throws PrinterException {

            int result = NO_SUCH_PAGE;
            if (pageIndex == 0) {

                Graphics2D g2d = (Graphics2D) graphics;

                double width = pageFormat.getImageableWidth();
                g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());

                FontMetrics metrics = g2d.getFontMetrics(new Font("Arial", Font.BOLD, 8));

                int nombrenegocio_posicion = (int) ((width - metrics.stringWidth(nombre_negocio)) / 2);;
                int nit_posicion = (int) ((width - metrics.stringWidth("NIT/RUT. " + nit)) / 2);;
                int direccion_posicion = (int) ((width - metrics.stringWidth(direccion)) / 2);;
                int telefono_posicion = (int) ((width - metrics.stringWidth(telefono)) / 2);;
                int fecha_hora_posicion = (int) ((width - metrics.stringWidth(fecha_hora)) / 2);;
                int no_fac_posicion = (int) ((width - metrics.stringWidth("Factura No. " + no_factura)) / 2);;
                int cod_fac_posicion = (int) ((width - metrics.stringWidth(codigo_factura)) / 2);;

                try {
                    /*Draw Header*/
                    int y = 20;
                    int yShift = 15;

                    g2d.setFont(new Font("Arial", Font.BOLD, 10));
                    g2d.drawString("___________________________", 4, y);
                    y += yShift;
                    g2d.drawString(nombre_negocio, nombrenegocio_posicion, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 10));
                    g2d.drawString("NIT/RUT. " + nit, nit_posicion, y);
                    y += yShift;
                    g2d.drawString(direccion, direccion_posicion, y);
                    y += yShift;
                    g2d.drawString(telefono, telefono_posicion, y);
                    y += yShift;
                    g2d.drawString(fecha_hora, fecha_hora_posicion, y);
                    g2d.setFont(new Font("Arial", Font.BOLD, 10));
                    y += yShift;
                    g2d.drawString("Factura No. " + no_factura, no_fac_posicion, y);
                    y += yShift;
                    g2d.drawString(codigo_factura, cod_fac_posicion, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 10));
                    g2d.drawString("----------------------------------------", 8, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 9));
                    g2d.drawString("Cliente: " + cliente.getNombre(), 8, y);
                    y += yShift;
                    g2d.drawString("Documento: " + cliente.getCedula(), 8, y);
                    y += yShift;
                    g2d.drawString("Dirección: " + cliente.getDireccion(), 8, y);
                    y += yShift;
                    g2d.drawString("Tel.: " + cliente.getContacto(), 8, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 10));
                    g2d.drawString("_______________________________", 4, y);
                    g2d.setFont(new Font("Arial", Font.BOLD, 7));
                    y += yShift;
                    g2d.drawString("Artículo", 4, y);
                    g2d.drawString("Precio", 70, y);
                    g2d.drawString("Cant.", 96, y);
                    g2d.drawString("Total", 115, y);
                    g2d.setFont(new Font("Arial", Font.BOLD, 12));
                    g2d.drawString("________________________________", 4, y);
                    g2d.setFont(new Font("Arial", Font.PLAIN, 6)); // aca se define el tamaño del font
                    y += yShift;
                    for (int i = 0; i < productos.size(); i++) {
                        g2d.drawString(productos.get(i).getNombre(), 4, y); // el 4 indica el margen que hay en la izquierda
                        g2d.drawString("$" + productos.get(i).getPunitario(), 70, y);
                        g2d.drawString(productos.get(i).getCantidad(), 96, y);
                        g2d.drawString("$" + productos.get(i).getPtotal(), 110, y);
                        y += yShift;
                    }
                    g2d.setFont(new Font("Arial", Font.PLAIN, 12));
                    g2d.drawString("________________________________", 4, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 7));
                    g2d.drawString("Subtotal", 50, y);
                    g2d.drawString("$ " + subtotal, 90, y);
                    y += yShift;
                    g2d.drawString("Descuento", 40, y);
                    g2d.drawString("$ " + descuento, 90, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.BOLD, 7));
                    g2d.drawString("TOTAL FACTURA", 12, y);
                    g2d.setFont(new Font("Arial", Font.PLAIN, 7));
                    g2d.drawString("$ " + total, 90, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 8));
                    y += yShift;
                    g2d.drawString("*************************************", 4, y);
                    y += yShift;
                    g2d.drawString("GRACIAS POR SU COMPRA", 10, y);
                    y += yShift;
                    g2d.drawString("*************************************", 4, y);
                    y += yShift;

                    y += yShift;
                } catch (Exception r) {
                    r.printStackTrace();
                }

                result = PAGE_EXISTS;
            }
            return result;
        }
    }
}

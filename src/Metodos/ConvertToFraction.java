/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos;

import java.util.Scanner;

/**
 *
 * @author mktec
 */
public class ConvertToFraction {

    private int numerator, denominator, entero;

    public static void main(String[] args) {
//        System.out.println(new ConvertToFraction(1234567.03125));
    }

    public ConvertToFraction(double decimal) {

        String decimalString = decimal + "";
        int posicion_punto = decimalString.indexOf(".") + 1;
        entero = Integer.parseInt(decimalString.substring(0, posicion_punto - 1));
        decimal = Double.parseDouble(decimalString.substring(posicion_punto - 1, decimalString.length()));

        if (decimal == 0.0) {

        } else {
            String stringNumber = String.valueOf(decimal);
            int numberDigitsDecimals = stringNumber.length() - 1 - stringNumber.indexOf('.');
            int denominator = 1;
            for (int i = 0; i < numberDigitsDecimals; i++) {
                decimal *= 10;
                denominator *= 10;
            }

            int numerator = (int) Math.round(decimal);
            int greatestCommonFactor = greatestCommonFactor(numerator, denominator);
            this.numerator = numerator / greatestCommonFactor;
            this.denominator = denominator / greatestCommonFactor;
        }

    }

    public static int greatestCommonFactor(int num, int denom) {
        if (denom == 0) {
            return num;
        }
        return greatestCommonFactor(denom, num % denom);
    }

    public String toString() {
        if (denominator == 0) {
            return String.valueOf(entero);

        }
        return String.valueOf(entero) + "  " + String.valueOf(numerator) + "/" + String.valueOf(denominator);

    }
}

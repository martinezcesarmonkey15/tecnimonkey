/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos;

import javax.swing.JOptionPane;

/**
 *
 * @author mktec
 */
public class ConvertFractionToDecimal {

    String fracion;
    double decimal;

    public static void main(String[] args) {
        System.out.println(new ConvertFractionToDecimal("11/64"));
    }

    public ConvertFractionToDecimal(String fraccion) {
        int posicion_slash = fraccion.indexOf("/");
        double numerador = Double.parseDouble(fraccion.substring(0, posicion_slash));
        double denominador = Double.parseDouble(fraccion.substring(posicion_slash + 1, fraccion.length()));

        decimal = numerador / denominador;
        System.out.println("numerador " + numerador);
        System.out.println("denominador " + denominador);
    }

    public String toString() {
        return String.valueOf(decimal);

    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos;

import conexiondb.DB_consultas_R_D;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.sql.ResultSet;
import java.util.ArrayList;
import modelos.Contactos;
import modelos.ProductoImprimir;

/**
 *
 * @author mktec
 */
public class ImprimirTermica80MM_Servicio {

    String no_factura, fecha_hora, cliente;
    double middleHeight = 0.0;

    public ImprimirTermica80MM_Servicio(String fecha, String no_factura, String cliente) {

        this.fecha_hora = fecha;
        this.no_factura = no_factura;
        this.cliente = cliente;

    }

    public PageFormat getPageFormat(PrinterJob pj) {

        PageFormat pf = pj.defaultPage();
        Paper paper = pf.getPaper();

        double headerHeight = 2.0;
        double footerHeight = 3.0;
        double width = convert_CM_To_PPI(8);      //printer know only point per inch.default value is 72ppi aca se define el ancho de la impresion
        double height = convert_CM_To_PPI(headerHeight + middleHeight + footerHeight);
        paper.setSize(width, height);
        paper.setImageableArea(
                0,
                10,
                width,
                height - convert_CM_To_PPI(1)
        );   //define boarder size    after that print area width is about 180 points

        pf.setOrientation(PageFormat.PORTRAIT);           //select orientation portrait or landscape but for this time portrait
        pf.setPaper(paper);

        return pf;
    }

    protected static double convert_CM_To_PPI(double cm) {
        return toPPI(cm * 0.393600787);
    }

    protected static double toPPI(double inch) {
        return inch * 72d;
    }

    public void imprime() {
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setCopies(1);
        pj.setPrintable(new Printables(), getPageFormat(pj));
        try {
            pj.print();
        } catch (PrinterException ex) {
            ex.printStackTrace();
        }
    }

    public class Printables implements Printable {

        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)
                throws PrinterException {

            int result = NO_SUCH_PAGE;
            if (pageIndex == 0) {

                Graphics2D g2d = (Graphics2D) graphics;

                double width = pageFormat.getImageableWidth();
                g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());

                FontMetrics metrics = g2d.getFontMetrics(new Font("Arial", Font.BOLD, 10));

                int fecha_hora_posicion = (int) ((width - metrics.stringWidth(fecha_hora)) / 2);;
                int no_fac_posicion = (int) ((width - metrics.stringWidth("Factura No. " + no_factura)) / 2);;

                try {
                    /*Draw Header*/
                    int y = 20;
                    int yShift = 15;
                    g2d.setFont(new Font("Arial", Font.BOLD, 40));
                    y += yShift;
                    g2d.drawString(no_factura, no_fac_posicion, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.PLAIN, 10));
//                    g2d.drawString(fecha_hora, fecha_hora_posicion, y);
                    y += yShift;
                    g2d.drawString(fecha_hora + " - " + cliente, 12, y);

                } catch (Exception r) {
                    r.printStackTrace();
                }

                result = PAGE_EXISTS;
            }
            return result;
        }
    }
}

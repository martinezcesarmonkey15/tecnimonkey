/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos;

import static Formularios.frm_main.cerra;
import static Formularios.frm_main.escritorio;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import java.awt.Component;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import javax.swing.ImageIcon;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.RowFilter;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableRowSorter;
import org.jdesktop.swingx.JXFindBar;
import org.jdesktop.swingx.JXTable;

/**
 *
 * @author Monkeyelgrande
 */
public class metodos {

    /**
     * Eliminar caracteres
     *
     * Este metodo recibe un primer campo tipo String con la cadena que desea
     * eliminar los caracteres y un segundo parametro con el caracter que desea
     * eliminar de toda la cadena
     *
     */
    public static void foto_a_label(String ruta, JLabel lbl_foto) {
        try {

            Image img = new ImageIcon(ruta).getImage();

            // Obtener el ancho y alto originales de la imagen
            int originalWidth = img.getWidth(null);
            int originalHeight = img.getHeight(null);

            // Calcular la escala para ajustar la imagen proporcionalmente
            double scaleFactor = Math.min(1.0 * lbl_foto.getWidth() / originalWidth, 1.0 * lbl_foto.getHeight() / originalHeight);

            // Redimensionar la imagen utilizando la escala calculada
            int scaledWidth = (int) (originalWidth * scaleFactor);
            int scaledHeight = (int) (originalHeight * scaleFactor);
            
            Image newimg = img.getScaledInstance(scaledWidth, scaledHeight, Image.SCALE_DEFAULT);

            // Crear un ImageIcon personalizado para centrar la imagen
            ImageIcon newicon = new ImageIcon(newimg) {
                @Override
                public synchronized void paintIcon(Component c, Graphics g, int x, int y) {
                    // Calcular las coordenadas para centrar la imagen en el JLabel
                    int offsetX = (lbl_foto.getWidth() - getIconWidth()) / 2;
                    int offsetY = (lbl_foto.getHeight() - getIconHeight()) / 2;

                    // Dibujar la imagen centrada
                    g.drawImage(newimg, x + offsetX, y + offsetY, c);
                }
            };

            lbl_foto.setIcon(newicon);

        } catch (Exception e) {
            System.out.println("NO se cargo la imagen");
        }
    }

    public static void BuscarEnTabla(JTextField txt_Filtro, JTable jtabla) {
        txt_Filtro.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void insertUpdate(DocumentEvent e) {
                filterTable();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                filterTable();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                filterTable();
            }

            private void filterTable() {
                String searchText = txt_Filtro.getText().toLowerCase();

                DefaultTableModel model = (DefaultTableModel) jtabla.getModel();
                TableRowSorter<DefaultTableModel> rowSorter = new TableRowSorter<>(model);
                jtabla.setRowSorter(rowSorter);

                RowFilter<DefaultTableModel, Object> rowFilter = RowFilter.regexFilter("(?i)" + searchText); // Ignora mayúsculas y minúsculas
                rowSorter.setRowFilter(rowFilter);
            }
        });
    }

    public static void copyFile_Java7(String origen, String destino) {
        try {
            Path FROM = Paths.get(origen);
            Path TO = Paths.get(destino);
            CopyOption[] options = new CopyOption[]{
                StandardCopyOption.REPLACE_EXISTING,
                StandardCopyOption.COPY_ATTRIBUTES
            };

            Files.copy(FROM, TO, options);
            System.out.println("Imagen copiada correctamente");
        } catch (IOException e) {
            JOptionPane.showMessageDialog(null, "Error al Guardar Imagen: " + e);
            System.err.println(e.toString());
        }
    }

    public static String EliminaCaracteres(String s_cadena, String s_caracteres) {
        String nueva_cadena = "";
        Character caracter = null;
        boolean valido = true;

        /* Va recorriendo la cadena s_cadena y copia a la cadena que va a regresar,
         sólo los caracteres que no estén en la cadena s_caracteres */
        for (int i = 0; i < s_cadena.length(); i++) {
            valido = true;
            for (int j = 0; j < s_caracteres.length(); j++) {
                caracter = s_caracteres.charAt(j);

                if (s_cadena.charAt(i) == caracter) {
                    valido = false;
                    break;
                }
            }
            if (valido) {
                nueva_cadena += s_cadena.charAt(i);
            }
        }

        return nueva_cadena;
    }

    public static String ReemplazarCaracteres(String s_cadena, String s_caracter_a_reemplazar, String nuevo_caracter) {
        String nueva_cadena = "";

        nueva_cadena = s_cadena.replace(s_caracter_a_reemplazar, nuevo_caracter);

        return nueva_cadena;
    }

    public static boolean estacerrado(Object obj) {
        JInternalFrame[] activos = escritorio.getAllFrames();
        boolean cerrado = true;
        int i = 0;
        while (i < activos.length && cerrado) {
            if (activos[i] == obj) {
                cerrado = false;
                cerra = false;
            }
            i++;
        }
        return cerrado;
    }

    public static void addEscapeListenerWindowDialog(final JDialog windowDialog) {
        ActionListener escAction = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                windowDialog.dispose();
            }
        };
        windowDialog.getRootPane().registerKeyboardAction(escAction,
                KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0),
                JComponent.WHEN_IN_FOCUSED_WINDOW);
    }

    /**
     * Evitar tabulado en JTextArea
     *
     * Este metodo evita que se tabule en los jtext area y en reemplazo cambia
     * al siguente foco recibe como parametro el un elemento tipo JTextArea
     */
    public static void EvitarTabEnJTextArea(JTextArea area) {
        area.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_TAB) {
                    if (e.getModifiers() > 0) {
                        area.transferFocusBackward();
                    } else {
                        area.transferFocus();
                    }
                    e.consume();
                }
            }
        });
    }

    /**
     *
     * @param carta
     * @param oficio
     * @return
     */
    public static String TamanoHoja(String carta, String oficio) {
        String cad = "";
        String[] botones = {"Carta", "Oficio"};
        ImageIcon icono = new ImageIcon("src/imagenes/page.png");
        int variable = JOptionPane.showOptionDialog(null, "Seleccione tamaño de impresión", "Tamaño hoja",
                JOptionPane.YES_NO_CANCEL_OPTION, JOptionPane.WARNING_MESSAGE, icono, botones, null);
        if (variable >= 0) {

            if (variable == 0) {
                cad = new File("").getAbsolutePath() + carta; // opcion carta
            }
            if (variable == 1) {
                cad = new File("").getAbsolutePath() + oficio; // opcion oficio
            }
        }

        return cad;
    }

    public static DecimalFormat formateador() {
        DecimalFormat formatea = null;
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.'); // este es el de decimales esta bien
        simbolos.setGroupingSeparator(',');
        formatea = new DecimalFormat("###,###", simbolos); // se le retiro el .## para que salieran sin decimales
        return formatea;
    }

    public static DecimalFormat formateador_decimal() {
        DecimalFormat formatea = new DecimalFormat("###,###.##");
        return formatea;
    }

    public static DecimalFormat formateador_dinero() {
        DecimalFormat formatea = new DecimalFormat("###,###");
        return formatea;
    }

    public static DecimalFormat formateador_un_decimal() {
        DecimalFormat formatea = null;
        DecimalFormatSymbols simbolos = new DecimalFormatSymbols();
        simbolos.setDecimalSeparator('.'); // este es el de decimales esta bien
        simbolos.setGroupingSeparator(',');
        formatea = new DecimalFormat("###,###.0", simbolos); // se le retiro el .## para que salieran sin decimales
        return formatea;
    }

    /**
     * Este metodo permite que las tablas de tipo JXtable tengan la opcion del
     * ctrl+f par busquedas y filtros de clic derecho sobre el nombre de las
     * columnas Libreria swing-bits-0.5.0.jar
     *
     * @param tabla
     * @autor monkeyelgrande
     */
    public static void TablaAptaParaBusquedaAndSSM(JXTable tabla) {
        TableRowFilterSupport.forTable(tabla).searchable(true).apply();
        JXFindBar findBar = new JXFindBar(tabla.getSearchable());
        tabla.setSelectionModel(new ForcedListSelectionModel());
    }
}

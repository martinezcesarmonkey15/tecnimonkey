/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author Monkeyelgrande
 */
public class CellRendererServicios extends DefaultTableCellRenderer {

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component c = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        String valor = (String) value;

        if (!table.isRowSelected(row)) {
            if (column == 7) {
                if (valor.equals("Ingresado")) {
                    c.setBackground(Color.YELLOW);
                }
                if (valor.equals("Cotizado")) {
                    c.setBackground(Color.CYAN);
                }
                if (valor.equals("Trabajando")) {
                    c.setBackground(Color.GREEN);
                }
                if (valor.equals("Terminado")) {
                    c.setBackground(Color.GRAY);
                    c.setForeground(Color.WHITE);
                }
                if (valor.equals("Entregado")) {
                    c.setBackground(Color.PINK);
                }

            } else if (column == 1) {
                c.setBackground(Color.YELLOW);

            } else if (column == 6) {
                if (valor.contains("-")) {
                    c.setBackground(Color.RED);
                }
                if (valor.contains("Pagado")) {
                    c.setBackground(Color.GREEN);
                }

            } else if (column == 5) {
                
                if (valor.contains(".")) {
                    c.setBackground(Color.GREEN);
                }

            } else {
                c.setBackground(table.getBackground());
                c.setForeground(table.getForeground());
            }
        }
        return c;
    }

}

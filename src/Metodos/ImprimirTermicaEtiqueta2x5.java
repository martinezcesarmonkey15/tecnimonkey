/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Metodos;

import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.Printable;
import static java.awt.print.Printable.NO_SUCH_PAGE;
import static java.awt.print.Printable.PAGE_EXISTS;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
/**
 *
 * @author mktec
 */
public class ImprimirTermicaEtiqueta2x5 {

    String cliente, codigo, equipo;

    public ImprimirTermicaEtiqueta2x5(String cliente, String codigo, String equipo) {

        this.cliente = cliente;
        this.codigo = codigo;
        this.equipo = equipo;

    }

    public PageFormat getPageFormat(PrinterJob pj) {

        PageFormat pf = pj.defaultPage();
        Paper paper = pf.getPaper();


        double width = convert_CM_To_PPI(3);      //printer know only point per inch.default value is 72ppi aca se define el ancho de la impresion
        double height = convert_CM_To_PPI(5);
        paper.setSize(width, height);
        paper.setImageableArea(
                0,
                0,
                width,
                height - convert_CM_To_PPI(1)
        );   //define boarder size after that print area width is about 180 points

        pf.setOrientation(PageFormat.LANDSCAPE);           //select orientation portrait or landscape but for this time portrait
        pf.setPaper(paper);

        return pf;
    }

    protected static double convert_CM_To_PPI(double cm) {
        return toPPI(cm * 0.393600787);
    }

    protected static double toPPI(double inch) {
        return inch * 72d;
    }

    public void imprime() {
        PrinterJob pj = PrinterJob.getPrinterJob();
        pj.setCopies(1);
        pj.setPrintable(new Printables(), getPageFormat(pj));
        try {
            pj.print();
        } catch (PrinterException ex) {
            ex.printStackTrace();
        }
    }

    public class Printables implements Printable {

        public int print(Graphics graphics, PageFormat pageFormat, int pageIndex)
                throws PrinterException {

            int result = NO_SUCH_PAGE;
            if (pageIndex == 0) {

                Graphics2D g2d = (Graphics2D) graphics;

                g2d.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());


                try {
                    /*Draw Header*/
                    int y = 20;
                    int yShift = 15;

                    g2d.setFont(new Font("Arial", Font.BOLD, 10));
                    g2d.drawString("Monkeys Technology", 10, y);
                    y += yShift;

                    g2d.setFont(new Font("Arial", Font.PLAIN, 10));
                    g2d.drawString("Cliente: " + cliente, 10, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.BOLD, 15));
                    g2d.drawString("Código: " + codigo, 10, y);
                    y += yShift;
                    g2d.setFont(new Font("Arial", Font.BOLD, 10));
                    g2d.drawString(equipo, 10, y);

                } catch (Exception r) {
                    r.printStackTrace();
                    System.out.println(r);
                }

                result = PAGE_EXISTS;
            }
            return result;
        }
    }
}

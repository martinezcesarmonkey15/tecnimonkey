/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios_servicio;

import Metodos.CellRendererAbonos;
import Metodos.ExportarExcel;
import Metodos.metodos;
import com.ezware.oxbow.swingbits.table.filter.TableRowFilterSupport;
import conexiondb.ConsultasSQL;
import conexiondb.DB_Ordenes;
import conexiondb.DB_consultas_R_D;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.RowFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableRowSorter;
import modelos.Contactos;

/**
 *
 * @author Monkeyelgrande
 */
public class frm_Creditos extends javax.swing.JInternalFrame {

    /**
     * Creates new form frm_clientes
     */
    private TableRowSorter trsFiltro;

    DecimalFormat formatea = new DecimalFormat("###,###.##");
    Calendar fecha = new GregorianCalendar();

    CellRendererAbonos myRenderer = new CellRendererAbonos();
    SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yyyy");

    public frm_Creditos() {
        initComponents();
        mostrar();
        TamanosTablaAbonos();
        jtabla.setDefaultRenderer(Object.class, myRenderer);
        jtabla.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(MouseEvent me) {
                if (me.getClickCount() == 2) {
                    VerAbonos();
                }
            }
        });
        TableRowFilterSupport.forTable(jtabla).searchable(true).apply();
        metodos.BuscarEnTabla(txt_Filtro, jtabla);
    }

    public void TamanosTablaAbonos() {
        jtabla.getTableHeader().setReorderingAllowed(false);
        TableColumnModel columnModel = jtabla.getColumnModel();
        columnModel.getColumn(0).setPreferredWidth(40);
        columnModel.getColumn(1).setPreferredWidth(80);
        columnModel.getColumn(2).setPreferredWidth(250);
        columnModel.getColumn(3).setPreferredWidth(100);
        columnModel.getColumn(4).setPreferredWidth(100);
        columnModel.getColumn(5).setPreferredWidth(100);
        columnModel.getColumn(6).setPreferredWidth(100);
        columnModel.getColumn(7).setPreferredWidth(150);
        columnModel.getColumn(8).setPreferredWidth(80);
    }
    DefaultTableModel modelo = new DefaultTableModel() {
        @Override
        public boolean isCellEditable(int fila, int columna) {
            return false; //Con esto conseguimos que la tabla no se pueda editar
        }
    };

    public void mostrar() {
        try {
            for (int i = 0; i < modelo.getRowCount(); i++) {
                modelo.removeRow(i);
                i -= 1;
            }
        } catch (Exception e) {
        }
        String estado = "";
        String dias_vence = "";
        String consulta = "select cr.id, cr.id_servicio, c.nombre, c.cedula, cr.total, \n"
                + "(select coalesce(sum(abono),0) from abonos_creditos where id_credito=cr.id) as abono, \n"
                + "(cr.total-(select coalesce(sum(abono),0) from abonos_creditos where id_credito=cr.id) - cr.descuento) as saldo, \n"
                + "v.serial, (current_date - cr.fecha) as dias_vencidos \n"
                + "from creditos cr, servicios a, contactos c, equipos v \n"
                + "where cr.id_servicio=a.id and a.id_cliente=c.id and a.id_equipo=v.id";
//        System.out.println(consulta);
        ResultSet rs = DB_consultas_R_D.getTabla(consulta);
        modelo.setColumnIdentifiers(new Object[]{"ID credito", "ID servicio", "Cliente", "Cédula", "Total", "Abono", "Saldo",
            "Equipo", "Dias vencidos", "Estado"});
        try {
            while (rs.next()) {
                estado = "";
                dias_vence = rs.getString("dias_vencidos");
                if (rs.getDouble("saldo") == 0) {
                    estado = "Pagado";
                    dias_vence = "0";
                } else {
                    estado = "Pendiente";
                }
                modelo.addRow(new Object[]{rs.getString("id"), rs.getString("id_servicio"), rs.getString("nombre"), rs.getString("cedula"),
                    metodos.formateador_dinero().format(rs.getDouble("total")), metodos.formateador_dinero().format(rs.getDouble("abono")),
                    metodos.formateador_dinero().format(rs.getDouble("saldo")), rs.getString("serial"), dias_vence, estado});
            }
            rs.close();
            // asigna el modelo a la tabla
            jtabla.setModel(modelo);
            TamanosTablaAbonos();
        } catch (Exception e) {
            System.out.println(e);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpop_crap = new javax.swing.JPopupMenu();
        jpm_Abonar = new javax.swing.JMenuItem();
        lpm_verFa = new javax.swing.JMenuItem();
        jPanel2 = new javax.swing.JPanel();
        lbl_cant_clientes = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txt_Filtro = new javax.swing.JTextField();
        jScrollPane2 = new javax.swing.JScrollPane();
        jtabla = new javax.swing.JTable();
        btn_total = new javax.swing.JButton();
        jlabel = new javax.swing.JLabel();
        lbl_suma_saldo = new javax.swing.JLabel();
        jlabel1 = new javax.swing.JLabel();
        lbl_suma_abonos = new javax.swing.JLabel();
        jlabel2 = new javax.swing.JLabel();
        lbl_suma_facturas = new javax.swing.JLabel();
        jlabel3 = new javax.swing.JLabel();
        lbl_pendiente = new javax.swing.JLabel();
        jlabel4 = new javax.swing.JLabel();
        lbl_vencido = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        btn_crear = new javax.swing.JButton();
        btn_actualizar = new javax.swing.JButton();
        btn_VerFactura = new javax.swing.JButton();
        btn_VerAbonos = new javax.swing.JButton();
        btn_crear1 = new javax.swing.JButton();
        jPanel4 = new javax.swing.JPanel();
        jLabel13 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        btn_VerAbonos1 = new javax.swing.JButton();
        jButton1 = new javax.swing.JButton();

        jpm_Abonar.setText("Realiar Abono");
        jpm_Abonar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jpm_AbonarActionPerformed(evt);
            }
        });
        jpop_crap.add(jpm_Abonar);

        lpm_verFa.setText("Ver factura");
        lpm_verFa.setToolTipText("");
        lpm_verFa.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                lpm_verFaActionPerformed(evt);
            }
        });
        jpop_crap.add(lpm_verFa);

        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setTitle("Créditos servicios");

        lbl_cant_clientes.setBackground(new java.awt.Color(33, 33, 33));

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(255, 255, 255));
        jLabel1.setText("Buscar cliente:");

        txt_Filtro.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txt_Filtro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txt_FiltroKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txt_FiltroKeyTyped(evt);
            }
        });

        jtabla.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jtabla.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jtabla.setComponentPopupMenu(jpop_crap);
        jtabla.setRowHeight(32);
        jtabla.setSelectionBackground(new java.awt.Color(0, 153, 153));
        jtabla.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jScrollPane2.setViewportView(jtabla);

        btn_total.setBackground(new java.awt.Color(0, 153, 51));
        btn_total.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        btn_total.setForeground(new java.awt.Color(255, 255, 255));
        btn_total.setText("Sumar Total");
        btn_total.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_totalActionPerformed(evt);
            }
        });

        jlabel.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jlabel.setForeground(new java.awt.Color(255, 255, 255));
        jlabel.setText("Saldo $");

        lbl_suma_saldo.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_suma_saldo.setForeground(new java.awt.Color(255, 0, 0));
        lbl_suma_saldo.setText("0");

        jlabel1.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jlabel1.setForeground(new java.awt.Color(255, 255, 255));
        jlabel1.setText("Abonos $");

        lbl_suma_abonos.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_suma_abonos.setForeground(new java.awt.Color(102, 255, 102));
        lbl_suma_abonos.setText("0");

        jlabel2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jlabel2.setForeground(new java.awt.Color(255, 255, 255));
        jlabel2.setText("Total Factura $");

        lbl_suma_facturas.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_suma_facturas.setForeground(new java.awt.Color(102, 255, 204));
        lbl_suma_facturas.setText("0");

        jlabel3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jlabel3.setForeground(new java.awt.Color(255, 255, 255));
        jlabel3.setText("Total pendiente $");

        lbl_pendiente.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_pendiente.setForeground(new java.awt.Color(255, 255, 0));
        lbl_pendiente.setText("0");

        jlabel4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jlabel4.setForeground(new java.awt.Color(255, 255, 255));
        jlabel4.setText("Total vencido $");

        lbl_vencido.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lbl_vencido.setForeground(new java.awt.Color(255, 0, 0));
        lbl_vencido.setText("0");

        javax.swing.GroupLayout lbl_cant_clientesLayout = new javax.swing.GroupLayout(lbl_cant_clientes);
        lbl_cant_clientes.setLayout(lbl_cant_clientesLayout);
        lbl_cant_clientesLayout.setHorizontalGroup(
            lbl_cant_clientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lbl_cant_clientesLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(lbl_cant_clientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2)
                    .addGroup(lbl_cant_clientesLayout.createSequentialGroup()
                        .addComponent(txt_Filtro, javax.swing.GroupLayout.PREFERRED_SIZE, 269, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(btn_total)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jlabel2)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_suma_facturas)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jlabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_suma_abonos)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jlabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(lbl_suma_saldo))
                    .addGroup(lbl_cant_clientesLayout.createSequentialGroup()
                        .addGroup(lbl_cant_clientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel1)
                            .addGroup(lbl_cant_clientesLayout.createSequentialGroup()
                                .addComponent(jlabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbl_pendiente)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jlabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(lbl_vencido)))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        lbl_cant_clientesLayout.setVerticalGroup(
            lbl_cant_clientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(lbl_cant_clientesLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(lbl_cant_clientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(txt_Filtro, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(btn_total)
                    .addComponent(jlabel)
                    .addComponent(lbl_suma_saldo)
                    .addComponent(jlabel1)
                    .addComponent(lbl_suma_abonos)
                    .addComponent(jlabel2)
                    .addComponent(lbl_suma_facturas))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(lbl_cant_clientesLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jlabel3)
                    .addComponent(lbl_pendiente)
                    .addComponent(jlabel4)
                    .addComponent(lbl_vencido))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 417, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBackground(new java.awt.Color(33, 33, 33));

        btn_crear.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_crear.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo.png"))); // NOI18N
        btn_crear.setMnemonic('n');
        btn_crear.setText("Abonar");
        btn_crear.setBorder(null);
        btn_crear.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_crearActionPerformed(evt);
            }
        });

        btn_actualizar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_actualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/actualizar.png"))); // NOI18N
        btn_actualizar.setText("Actualizar");
        btn_actualizar.setBorder(null);
        btn_actualizar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_actualizarActionPerformed(evt);
            }
        });

        btn_VerFactura.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_VerFactura.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ver.png"))); // NOI18N
        btn_VerFactura.setText("Ver servicio");
        btn_VerFactura.setBorder(null);
        btn_VerFactura.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_VerFacturaActionPerformed(evt);
            }
        });

        btn_VerAbonos.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_VerAbonos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ver.png"))); // NOI18N
        btn_VerAbonos.setText("Ver abonos");
        btn_VerAbonos.setBorder(null);
        btn_VerAbonos.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_VerAbonosActionPerformed(evt);
            }
        });

        btn_crear1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_crear1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/nuevo.png"))); // NOI18N
        btn_crear1.setMnemonic('n');
        btn_crear1.setText("Abonar a cliente");
        btn_crear1.setBorder(null);
        btn_crear1.setEnabled(false);
        btn_crear1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_crear1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btn_crear1, javax.swing.GroupLayout.DEFAULT_SIZE, 185, Short.MAX_VALUE)
                    .addComponent(btn_crear, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_actualizar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_VerFactura, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(btn_VerAbonos, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(btn_crear)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_crear1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_actualizar)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_VerFactura)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btn_VerAbonos)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        jPanel4.setBackground(new java.awt.Color(0, 116, 214));
        jPanel4.setPreferredSize(new java.awt.Dimension(146, 80));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setText("Créditos de servicios");

        jButton2.setBackground(new java.awt.Color(102, 0, 0));
        jButton2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jButton2.setForeground(new java.awt.Color(255, 255, 255));
        jButton2.setMnemonic('w');
        jButton2.setText("Cerrar");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        btn_VerAbonos1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        btn_VerAbonos1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/ver.png"))); // NOI18N
        btn_VerAbonos1.setText("Ver con pagadas");
        btn_VerAbonos1.setBorder(null);
        btn_VerAbonos1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_VerAbonos1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addGap(31, 31, 31)
                .addComponent(jLabel13)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 515, Short.MAX_VALUE)
                .addComponent(btn_VerAbonos1, javax.swing.GroupLayout.PREFERRED_SIZE, 185, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton2)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jButton2)
                    .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jLabel13)
                        .addComponent(btn_VerAbonos1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(16, 16, 16))
        );

        jButton1.setBackground(new java.awt.Color(255, 204, 0));
        jButton1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jButton1.setText("Exportar a excel");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, 1061, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(lbl_cant_clientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 64, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(lbl_cant_clientes, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 38, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    public void AbonarCliente() {
//        jd_abonar_creditos_cliente frm = new jd_abonar_creditos_cliente(null, closable);
//
//        int fila = jtabla.getSelectedRow();
//        if (fila < 0) {
//            JOptionPane.showMessageDialog(this, "Seleccione un registro");
//        } else {
//            String cedula_cliente = "" + jtabla.getValueAt(fila, 3);
//
//            ResultSet rs = DB_consultas_R_D.getTabla("select c.id, c.cedula, c.nombre, c.id as id_cliente from contactos c, facturas_cabeceras f where f.id_contacto=c.id and c.cedula='" + cedula_cliente + "'");
//
//            try {
//                while (rs.next()) {
//                    jd_abonar_creditos_cliente.lbl_cliente.setText(rs.getString("nombre"));
//                    jd_abonar_creditos_cliente.lbl_cedula.setText(rs.getString("cedula"));
//                    jd_abonar_creditos_cliente.lbl_id_cliente.setText(rs.getString("id_cliente"));
//
//                }
//                rs.close();
//
//            } catch (SQLException ex) {
//                Logger.getLogger(frm_Creditos.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            String dias_vence = "";
//            String consulta = "select ca.id as id_crap, f.id as id_factura, c.nombre as cliente,c.cedula, f.fecha, f.total as total_factura, (select sum(abono) from abonos where id_creditos_apartados=ca.id) as abono_total, \n"
//                    + "(f.total-(select sum(abono) from abonos where id_creditos_apartados=ca.id)) as saldo, ca.fecha_plazo_final as fecha_fin, f.tipo_factura, (current_date-ca.fecha_plazo_final) as dias_vence  \n"
//                    + "from facturas_cabeceras f, creditos_apartados ca, contactos c \n"
//                    + "where ca.id_factura=f.id and f.id_contacto=c.id and f.estado='1' and (f.total-(select sum(abono) from abonos where id_creditos_apartados=ca.id))>0 and c.cedula='" + cedula_cliente + "' "
//                    + "order by ca.fecha_plazo_final asc";
//
////            System.out.println(consulta);
//            rs = DB_consultas_R_D.getTabla(consulta);
//
//            try {
//                for (int i = 0; i < jd_abonar_creditos_cliente.jtabla_facturas.getRowCount(); i++) {
//                    jd_abonar_creditos_cliente.modelo_facturas.removeRow(i);
//                    i -= 1;
//                }
//            } catch (Exception e) {
//            }
//            try {
//                for (int i = 0; i < jd_abonar_creditos_cliente.jtabla_abonos.getRowCount(); i++) {
//                    jd_abonar_creditos_cliente.modelo_abonos.removeRow(i);
//                    i -= 1;
//                }
//            } catch (Exception e) {
//            }
//            jd_abonar_creditos_cliente.modelo_facturas.setColumnIdentifiers(new Object[]{"ID factura", "Total Factura", "Abono", "Saldo", "Fecha Creación", "Fecha Vencimiento", "Dias vencidos"});
//            try {
//                while (rs.next()) {
//                    dias_vence = rs.getString("dias_vence");
//
//                    jd_abonar_creditos_cliente.modelo_facturas.addRow(new Object[]{rs.getString("id_factura"), metodos.formateador().format(rs.getDouble("total_factura")),
//                        metodos.formateador().format(rs.getDouble("abono_total")), metodos.formateador().format(rs.getDouble("saldo")),
//                        sdf.format(rs.getDate("fecha")), sdf.format(rs.getDate("fecha_fin")), dias_vence});
//                }
//                rs.close();
//
//                TamanosTablaAbonos();
//            } catch (Exception e) {
//                System.out.println(e);
//            }
//            jd_abonar_creditos_cliente.sumar_totales();
//            jd_abonar_creditos_cliente.TamanosTablaAbonos(jd_abonar_creditos_cliente.jtabla_facturas);
//            frm.show();
//            jd_abonar_creditos_cliente.txt_abono.requestFocus();
//        }
    }

    public int consultarId(String id) {
        ResultSet rs = DB_consultas_R_D.getTabla("select count(id) as id from clientes where id = " + id + "");
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                return Integer.parseInt(rs.getString("id"));
            }
            rs.close();
            // asigna el modelo a la tabla
            jtabla.setModel(modelo);
        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
        return 0;
    }

    public void validar_numeros(java.awt.event.KeyEvent evt, char car) {
        if ((car < '0' || car > '9')) {
            evt.consume();
        }
    }

    public void VerAbonos() {
//        try {
//            for (int i = 0; i < jd_crear_abono_credito.modelo_abono.getRowCount(); i++) {
//                jd_crear_abono_credito.modelo_abono.removeRow(i);
//                i -= 1;
//            }
//        } catch (Exception e) {
//        }
//        jd_crear_abono_credito frm = new jd_crear_abono_credito(null, closable);
//
//        int fila = jtabla.getSelectedRow();
//
//        if (fila < 0) {
//            JOptionPane.showMessageDialog(this, "Seleccione un registro");
//        } else {
//            String id = jtabla.getValueAt(fila, 0).toString();
//
//            String consulta = "select *, s.id_cliente as id_cliente, u.nombre as usuario from creditos c, servicios s, users u "
//                    + "where c.id_servicio=s.id and c.id_user=u.id and c.id=" + id;
//
////            System.out.println(consulta);
//            ResultSet rs = DB_consultas_R_D.getTabla(consulta);
//            try {
//                while (rs.next()) {
//                    jd_crear_abono_credito.lbl_user.setText(rs.getString("usuario"));
//                    jd_crear_abono_credito.txt_descuento.setText(formatea.format(rs.getDouble("descuento")));
//                    jd_crear_abono_credito.lbl_id_cliente.setText(rs.getString("id_cliente"));
//                }
//                rs.close();
//                jd_crear_abono_credito.lbl_id_credito.setText(jtabla.getValueAt(fila, 0).toString());
//                jd_crear_abono_credito.lbl_id_servicio.setText(jtabla.getValueAt(fila, 1).toString());
//                jd_crear_abono_credito.lbl_cliente.setText(jtabla.getValueAt(fila, 2).toString());
//                jd_crear_abono_credito.lbl_total_credito.setText(jtabla.getValueAt(fila, 4).toString());
//                jd_crear_abono_credito.lbl_total_abonos.setText(jtabla.getValueAt(fila, 5).toString());
//                jd_crear_abono_credito.lbl_total_saldo.setText(jtabla.getValueAt(fila, 6).toString());
//                jd_crear_abono_credito.lbl_fecha_credito.setText(jtabla.getValueAt(fila, 7).toString());
//            } catch (SQLException ex) {
//                Logger.getLogger(jd_crear_abono_credito.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            jd_crear_abono_credito.formulario = "crear";
//
//            jd_crear_abono_credito.modelo_abono.setColumnIdentifiers(new Object[]{"ID", "Usuario", "Abono", "fecha", "Fondo", "Tipo"});

//            consulta = "select a.id, u.nombre as usuario, a.abono, a.fecha, coalesce(f.nombre,'-----') as fondo, a.id_anticipo \n"
//                    + "from abonos_creditos a left join fondos f  on a.id_fondo=f.id, users u \n"
//                    + "where a.id_user=u.id and a.id_credito=" + id + " order by id";
//
////            System.out.println(consulta);
//            rs = DB_consultas_R_D.getTabla(consulta);

//            try {
//                while (rs.next()) {
//                    String tipo = "Abono";
//                    if (rs.getInt("id_anticipo") > 0) {
//                        tipo = "Anticipo";
//                    }
//
//                    jd_crear_abono_credito.modelo_abono.addRow(new Object[]{rs.getString("id"), rs.getString("usuario"), "" + formatea.format(rs.getDouble("abono")),
//                        sdf.format(rs.getDate("fecha")), rs.getString("fondo"), tipo});
//                }
//                rs.close();
//                jd_crear_abono_credito.jtabla_abonos.setModel(jd_crear_abono_credito.modelo_abono);
//            } catch (SQLException ex) {
//                Logger.getLogger(jd_crear_abono_credito.class.getName()).log(Level.SEVERE, null, ex);
//            }
//            frm.show();
//        }

    }

    private void jpm_AbonarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jpm_AbonarActionPerformed
        VerAbonos();
    }//GEN-LAST:event_jpm_AbonarActionPerformed

    private void lpm_verFaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_lpm_verFaActionPerformed
//        Facturas_cabeceras f = new Facturas_cabeceras();
//        f.cargar_facturas(jtabla);
    }//GEN-LAST:event_lpm_verFaActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        this.dispose();
    }//GEN-LAST:event_jButton2ActionPerformed

    private void btn_VerAbonosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_VerAbonosActionPerformed
        VerAbonos();
    }//GEN-LAST:event_btn_VerAbonosActionPerformed

    private void btn_VerFacturaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_VerFacturaActionPerformed
        int fila = jtabla.getSelectedRow();
        String id = "" + jtabla.getValueAt(fila, 1);

        DB_Ordenes db = new DB_Ordenes();
        db.VerOrden(id);
     }//GEN-LAST:event_btn_VerFacturaActionPerformed

    private void btn_actualizarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_actualizarActionPerformed
        mostrar();
    }//GEN-LAST:event_btn_actualizarActionPerformed

    private void btn_crearActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_crearActionPerformed
        VerAbonos();
    }//GEN-LAST:event_btn_crearActionPerformed

    private void btn_VerAbonos1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_VerAbonos1ActionPerformed
        mostrar();
    }//GEN-LAST:event_btn_VerAbonos1ActionPerformed

    private void txt_FiltroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_FiltroKeyTyped
//        txt_Filtro.addKeyListener(new KeyAdapter() {
//            public void keyReleased(final KeyEvent e) {
//                String cadena = (txt_Filtro.getText());
//                repaint();
//                trsFiltro.setRowFilter(RowFilter.regexFilter("(?i)" + txt_Filtro.getText(), 2));
//
//            }
//        });
//        trsFiltro = new TableRowSorter(jtabla.getModel());
//        jtabla.setRowSorter(trsFiltro);
    }//GEN-LAST:event_txt_FiltroKeyTyped

    private void txt_FiltroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txt_FiltroKeyPressed
        char num = evt.getKeyChar();
        if ((num == KeyEvent.VK_DELETE)) {
            txt_Filtro.setText("");
        }
        if ((num == KeyEvent.VK_ENTER)) {
            btn_totalActionPerformed(null);
        }
    }//GEN-LAST:event_txt_FiltroKeyPressed

    private void btn_totalActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_totalActionPerformed
        double total_facturas = 0;
        double total_abonos = 0;
        double total_saldo = 0;
        double total_pendiente = 0;
        double total_vencido = 0;
        for (int i = 0; i < this.jtabla.getRowCount(); i++) {
            total_facturas += Double.parseDouble(metodos.EliminaCaracteres(this.jtabla.getValueAt(i, 4).toString(), "."));
            total_abonos += Double.parseDouble(metodos.EliminaCaracteres(this.jtabla.getValueAt(i, 5).toString(), "."));
            total_saldo += Double.parseDouble(metodos.EliminaCaracteres(this.jtabla.getValueAt(i, 6).toString(), "."));
            if (this.jtabla.getValueAt(i, 9).toString().equals("Pendiente")) {
                total_pendiente += Double.parseDouble(metodos.EliminaCaracteres(this.jtabla.getValueAt(i, 6).toString(), "."));
            } else {
                total_vencido += Double.parseDouble(metodos.EliminaCaracteres(this.jtabla.getValueAt(i, 6).toString(), "."));
            }
        }
        lbl_suma_facturas.setText(formatea.format(total_facturas) + "");
        lbl_suma_abonos.setText(formatea.format(total_abonos) + "");
        lbl_suma_saldo.setText(formatea.format(total_saldo) + "");
        lbl_pendiente.setText(formatea.format(total_pendiente) + "");
        lbl_vencido.setText(formatea.format(total_vencido) + "");
    }//GEN-LAST:event_btn_totalActionPerformed

    private void btn_crear1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_crear1ActionPerformed
        AbonarCliente();
    }//GEN-LAST:event_btn_crear1ActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        ExportarExcel obj;
        try {
            obj = new ExportarExcel();
            obj.exportarExcel(jtabla);
        } catch (IOException ex) {
            System.out.println("" + ex);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frm_Creditos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frm_Creditos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frm_Creditos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frm_Creditos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frm_Creditos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_VerAbonos;
    private javax.swing.JButton btn_VerAbonos1;
    private javax.swing.JButton btn_VerFactura;
    public static javax.swing.JButton btn_actualizar;
    private javax.swing.JButton btn_crear;
    private javax.swing.JButton btn_crear1;
    private javax.swing.JButton btn_total;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JLabel jlabel;
    private javax.swing.JLabel jlabel1;
    private javax.swing.JLabel jlabel2;
    private javax.swing.JLabel jlabel3;
    private javax.swing.JLabel jlabel4;
    private javax.swing.JMenuItem jpm_Abonar;
    private javax.swing.JPopupMenu jpop_crap;
    private javax.swing.JTable jtabla;
    private javax.swing.JPanel lbl_cant_clientes;
    private javax.swing.JLabel lbl_pendiente;
    private javax.swing.JLabel lbl_suma_abonos;
    private javax.swing.JLabel lbl_suma_facturas;
    private javax.swing.JLabel lbl_suma_saldo;
    private javax.swing.JLabel lbl_vencido;
    private javax.swing.JMenuItem lpm_verFa;
    private javax.swing.JTextField txt_Filtro;
    // End of variables declaration//GEN-END:variables
}

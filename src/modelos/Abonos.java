/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Monkeyelgrande
 */
public class Abonos {

    int id, id_creditos_apartados, id_user, id_fondo;
    double abono;
    String fecha, nombre_fondo;

    public int getId_fondo() {
        return id_fondo;
    }

    public void setId_fondo(int id_fondo) {
        this.id_fondo = id_fondo;
    }

    public String getNombre_fondo() {
        return nombre_fondo;
    }

    public void setNombre_fondo(String nombre_fondo) {
        this.nombre_fondo = nombre_fondo;
    }

    
    
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_creditos_apartados() {
        return id_creditos_apartados;
    }

    public void setId_creditos_apartados(int id_creditos_apartados) {
        this.id_creditos_apartados = id_creditos_apartados;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public double getAbono() {
        return abono;
    }

    public void setAbono(double abono) {
        this.abono = abono;
    }

}

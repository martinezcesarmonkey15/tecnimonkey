/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import Formularios_internos.jd_ver_in_egre;
import ReportesCodigo.jd_Egresos_diarios;
import conexiondb.DB_consultas_R_D;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author Monkeyelgrande
 */
public class Transferencias {

    String fecha, hora, descripcion, nombre_user, nombre_cuenta;
    int id, id_user, id_fondo_origen, id_fondo_destino;
    double total;
    DecimalFormat formatea = new DecimalFormat("###,###.##");

    public int getId_fondo_origen() {
        return id_fondo_origen;
    }

    public void setId_fondo_origen(int id_fondo_origen) {
        this.id_fondo_origen = id_fondo_origen;
    }

    public int getId_fondo_destino() {
        return id_fondo_destino;
    }

    public void setId_fondo_destino(int id_fondo_destino) {
        this.id_fondo_destino = id_fondo_destino;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getNombre_user() {
        return nombre_user;
    }

    public void setNombre_user(String nombre_user) {
        this.nombre_user = nombre_user;
    }

    public String getNombre_cuenta() {
        return nombre_cuenta;
    }

    public void setNombre_cuenta(String nombre_cuenta) {
        this.nombre_cuenta = nombre_cuenta;
    }

    public static Transferencias traer_ingreso(String id) {
        Transferencias g = new Transferencias();

        ResultSet rs = DB_consultas_R_D.getTabla("select i.id, u.nombre as user, c.nombre, i.descripcion, i.total, i.fecha, i.hora "
                + "from ingresos i, users u, contactos c "
                + "where i.id_user=u.id and i.id_contacto=c.id and i.id=" + id);

        try {
            while (rs.next()) {
                g.setId(rs.getInt("id"));
                g.setNombre_user(rs.getString("user"));
                g.setNombre_cuenta(rs.getString("nombre"));
                g.setDescripcion(rs.getString("descripcion"));
                g.setTotal(rs.getDouble("total"));
                g.setFecha(rs.getString("fecha"));
                g.setHora(rs.getString("hora"));

            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
        }

        return g;
    }

}

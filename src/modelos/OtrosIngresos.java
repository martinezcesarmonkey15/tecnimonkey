/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import conexiondb.DB_consultas_R_D;
import java.sql.ResultSet;

/**
 *
 * @author Monkeyelgrande
 */
public class OtrosIngresos {

    String fecha, hora, descripcion, nombre_contacto, cedula_contacto, dir_contacto, telefono_contacto, nombre_user, nombre_cuenta, nombre_fondo;
    int id, id_user, id_cuenta, id_contacto, id_fondo;
    double total;

    public String getNombre_fondo() {
        return nombre_fondo;
    }

    public void setNombre_fondo(String nombre_fondo) {
        this.nombre_fondo = nombre_fondo;
    }

    public int getId_fondo() {
        return id_fondo;
    }

    public void setId_fondo(int id_fondo) {
        this.id_fondo = id_fondo;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre_contacto() {
        return nombre_contacto;
    }

    public void setNombre_contacto(String nombre_contacto) {
        this.nombre_contacto = nombre_contacto;
    }

    public String getCedula_contacto() {
        return cedula_contacto;
    }

    public void setCedula_contacto(String cedula_contacto) {
        this.cedula_contacto = cedula_contacto;
    }

    public String getDir_contacto() {
        return dir_contacto;
    }

    public void setDir_contacto(String dir_contacto) {
        this.dir_contacto = dir_contacto;
    }

    public String getTelefono_contacto() {
        return telefono_contacto;
    }

    public void setTelefono_contacto(String telefono_contacto) {
        this.telefono_contacto = telefono_contacto;
    }

    public String getNombre_user() {
        return nombre_user;
    }

    public void setNombre_user(String nombre_user) {
        this.nombre_user = nombre_user;
    }

    public String getNombre_cuenta() {
        return nombre_cuenta;
    }

    public void setNombre_cuenta(String nombre_cuenta) {
        this.nombre_cuenta = nombre_cuenta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_cuenta() {
        return id_cuenta;
    }

    public void setId_cuenta(int id_cuenta) {
        this.id_cuenta = id_cuenta;
    }

    public int getId_contacto() {
        return id_contacto;
    }

    public void setId_contacto(int id_contacto) {
        this.id_contacto = id_contacto;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public static OtrosIngresos traer_ingreso(String id) {
        OtrosIngresos otros = new OtrosIngresos();

        ResultSet rs = DB_consultas_R_D.getTabla("select o.id, u.nombre as user, c.nombre as contacto, c.id as id_contacto,c.cedula,"
                + "c.contacto as telefono, c.direccion, o.descripcion, o.total, o.fecha, o.hora, f.nombre as fondo "
                + "from otros_ingresos o, users u, contactos c, fondos f "
                + "where o.id_user=u.id and o.id_contacto=c.id and o.id_fondo =f.id and o.id=" + id);

        try {
            while (rs.next()) {
                otros.setId(rs.getInt("id"));
                otros.setNombre_user(rs.getString("user"));
                otros.setDescripcion(rs.getString("descripcion"));
                otros.setTotal(rs.getDouble("total"));
                otros.setFecha(rs.getString("fecha"));
                otros.setHora(rs.getString("hora"));
                otros.setNombre_contacto(rs.getString("contacto"));
                otros.setCedula_contacto(rs.getString("cedula"));
                otros.setTelefono_contacto(rs.getString("telefono"));
                otros.setDir_contacto(rs.getString("direccion"));
                otros.setId_contacto(Integer.parseInt(rs.getString("id_contacto")));
                otros.setNombre_fondo(rs.getString("fondo"));
            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return otros;
    }

}

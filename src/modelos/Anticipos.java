/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import conexiondb.DB_consultas_R_D;
import java.sql.ResultSet;

/**
 *
 * @author Monkeyelgrande
 */
public class Anticipos {

    String fecha, hora, descripcion, nombre_contacto, cedula_contacto, dir_contacto, telefono_contacto, nombre_user, nombre_cuenta, nombre_fondo;
    int id, id_user, id_contacto, id_fondo;
    double total;

    public String getNombre_fondo() {
        return nombre_fondo;
    }

    public void setNombre_fondo(String nombre_fondo) {
        this.nombre_fondo = nombre_fondo;
    }

    public int getId_fondo() {
        return id_fondo;
    }

    public void setId_fondo(int id_fondo) {
        this.id_fondo = id_fondo;
    }

    
    
    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre_contacto() {
        return nombre_contacto;
    }

    public void setNombre_contacto(String nombre_contacto) {
        this.nombre_contacto = nombre_contacto;
    }

    public String getCedula_contacto() {
        return cedula_contacto;
    }

    public void setCedula_contacto(String cedula_contacto) {
        this.cedula_contacto = cedula_contacto;
    }

    public String getDir_contacto() {
        return dir_contacto;
    }

    public void setDir_contacto(String dir_contacto) {
        this.dir_contacto = dir_contacto;
    }

    public String getTelefono_contacto() {
        return telefono_contacto;
    }

    public void setTelefono_contacto(String telefono_contacto) {
        this.telefono_contacto = telefono_contacto;
    }

    public String getNombre_user() {
        return nombre_user;
    }

    public void setNombre_user(String nombre_user) {
        this.nombre_user = nombre_user;
    }

    public String getNombre_cuenta() {
        return nombre_cuenta;
    }

    public void setNombre_cuenta(String nombre_cuenta) {
        this.nombre_cuenta = nombre_cuenta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_contacto() {
        return id_contacto;
    }

    public void setId_contacto(int id_contacto) {
        this.id_contacto = id_contacto;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public static Anticipos traer_ingreso(String id) {
        Anticipos anticipo = new Anticipos();

        ResultSet rs = DB_consultas_R_D.getTabla("select o.id, u.nombre as user, c.nombre as contacto, c.id as id_contacto,c.cedula,"
                + "c.contacto as telefono, c.direccion, o.descripcion, o.total, o.fecha, o.hora, f.nombre as fondo "
                + "from anticipos o, users u, contactos c, fondos f "
                + "where o.id_user=u.id and o.id_contacto=c.id and o.id_fondo=f.id and o.id=" + id);

        try {
            while (rs.next()) {
                anticipo.setId(rs.getInt("id"));
                anticipo.setNombre_user(rs.getString("user"));
                anticipo.setDescripcion(rs.getString("descripcion"));
                anticipo.setTotal(rs.getDouble("total"));
                anticipo.setFecha(rs.getString("fecha"));
                anticipo.setHora(rs.getString("hora"));
                anticipo.setNombre_contacto(rs.getString("contacto"));
                anticipo.setCedula_contacto(rs.getString("cedula"));
                anticipo.setTelefono_contacto(rs.getString("telefono"));
                anticipo.setDir_contacto(rs.getString("direccion"));
                anticipo.setId_contacto(Integer.parseInt(rs.getString("id_contacto")));
                anticipo.setNombre_fondo(rs.getString("fondo"));

            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return anticipo;
    }

    public static double consultar_anticipo_favor(int id_proveedor) {
        double cantidad = 0;
        String consulta = "select *,coalesce((select sum(total) from pagos_facturas where id_anticipo=a.id),0) as total_pagos_facturas, \n"
                + "(a.total - coalesce((select sum(total) from pagos_facturas where id_anticipo=a.id),0)) as anticipo_favor \n"
                + "from anticipos a where id_contacto= " + id_proveedor + " and a.total > coalesce((select sum(total) from pagos_facturas where id_anticipo=a.id),0);";

//        System.out.println(consulta);

        ResultSet rs = DB_consultas_R_D.getTabla(consulta);
        try {
            while (rs.next()) {
                cantidad = rs.getDouble("anticipo_favor");
            }
        } catch (Exception e) {
            System.out.println(e);
        }
        return cantidad;
    }

}

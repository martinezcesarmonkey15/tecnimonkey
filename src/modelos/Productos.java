/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import conexiondb.DB_consultas_R_D;
import static java.awt.image.ImageObserver.WIDTH;
import java.sql.ResultSet;
import java.text.SimpleDateFormat;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Monkeyelgrande
 */
public class Productos {

    int id,servicio;
    String codigo_barras, descripcion;
    double precio_costo, precio_venta;

    public Productos() {
    }

    public Productos(int id, String descripcion, String codigo_barras) {
        this.id = id;
        this.descripcion = descripcion;
        this.codigo_barras = codigo_barras;
    }

    public int getServicio() {
        return servicio;
    }

    public void setServicio(int servicio) {
        this.servicio = servicio;
    }

    public String getCodigo_barras() {
        return codigo_barras;
    }

    public void setCodigo_barras(String codigo_barras) {
        this.codigo_barras = codigo_barras;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio_costo() {
        return precio_costo;
    }

    public void setPrecio_costo(double precio_costo) {
        this.precio_costo = precio_costo;
    }

    public double getPrecio_venta() {
        return precio_venta;
    }

    public void setPrecio_venta(double precio_venta) {
        this.precio_venta = precio_venta;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    /*
    * cuando el valor SumaResta esta en true se realiza la suma, y cuando es false se realizar una resta en el stock
     */
    public void mostrarProductos_descripcion(JComboBox<Productos> jbox) {
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        modeloCombo.addElement(new Productos(0, "", ""));
        try {
            ResultSet rs = DB_consultas_R_D.getTabla("select id,descripcion,codigo_barras from productos");
            while (rs.next()) {
                modeloCombo.addElement(new Productos(rs.getInt("id"), rs.getString("descripcion"), rs.getString("codigo_barras")));
                jbox.setModel(modeloCombo);
            }
        } catch (Exception e) {
        }
        AutoCompleteDecorator.decorate(jbox);
    }

    public double consulta_promedio_ponderado(String id, double cantidad) {
        double promedio_ponderado = 0, suma_cantidad = 0, suma_precio_entrada = 0;
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");

        try {

            String consulta = "select * from ingresos_mercancias_detalle i, ingresos_mercancias_cabecera ic "
                    + "where i.id_ingreso_cabecera=ic.id and i.id_producto=" + id + " and ic.fecha <= CURRENT_DATE order by ic.fecha desc;";
//            System.out.println(consulta);
            ResultSet rs = DB_consultas_R_D.getTabla(consulta);

            try {
                while (rs.next()) {
                    suma_cantidad += rs.getDouble("cantidad");
                    if (suma_cantidad >= cantidad) {
                        suma_precio_entrada += rs.getDouble("precio_entrada") * (rs.getDouble("cantidad") - (suma_cantidad - cantidad));
                        promedio_ponderado = suma_precio_entrada / cantidad;
                        break;
                    } else {
                        suma_precio_entrada += rs.getDouble("precio_entrada") * rs.getDouble("cantidad");
                    }
                }
                rs.close();
            } catch (Exception e) {
                System.out.println(e);
            }
        } catch (Exception e) {
            JOptionPane.showMessageDialog(null, "error calculando el promedio ponderado" + e, "Alerta", WIDTH);
        }

        return promedio_ponderado;
    }

    @Override
    public String toString() {
        return descripcion;
    }

}

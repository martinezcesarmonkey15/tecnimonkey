/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Monkeyelgrande
 */
public class Prestamos {

    String fecha, descripcion, nombre_contacto, cedula_contacto, dir_contacto, telefono_contacto, nombre_user, hora, nombre_fondo;
    int id_cuenta;
//    DecimalFormat formatea = new DecimalFormat("###,###.##");

    public String getNombre_fondo() {
        return nombre_fondo;
    }

    public void setNombre_fondo(String nombre_fondo) {
        this.nombre_fondo = nombre_fondo;
    }

    public int getId_cuenta() {
        return id_cuenta;
    }

    public void setId_cuenta(int id_cuenta) {
        this.id_cuenta = id_cuenta;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }
    int id, id_user, id_contacto, caja;
    double total;

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombre_contacto() {
        return nombre_contacto;
    }

    public void setNombre_contacto(String nombre_contacto) {
        this.nombre_contacto = nombre_contacto;
    }

    public String getCedula_contacto() {
        return cedula_contacto;
    }

    public void setCedula_contacto(String cedula_contacto) {
        this.cedula_contacto = cedula_contacto;
    }

    public String getDir_contacto() {
        return dir_contacto;
    }

    public void setDir_contacto(String dir_contacto) {
        this.dir_contacto = dir_contacto;
    }

    public String getTelefono_contacto() {
        return telefono_contacto;
    }

    public void setTelefono_contacto(String telefono_contacto) {
        this.telefono_contacto = telefono_contacto;
    }

    public String getNombre_user() {
        return nombre_user;
    }

    public void setNombre_user(String nombre_user) {
        this.nombre_user = nombre_user;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_contacto() {
        return id_contacto;
    }

    public void setId_contacto(int id_contacto) {
        this.id_contacto = id_contacto;
    }

    public int getCaja() {
        return caja;
    }

    public void setCaja(int caja) {
        this.caja = caja;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

}

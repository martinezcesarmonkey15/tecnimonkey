/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import Formularios_internos.jd_ver_in_egre;
import ReportesCodigo.jd_Egresos_diarios;
import conexiondb.DB_consultas_R_D;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import javax.swing.JTable;

/**
 *
 * @author Monkeyelgrande
 */
public class Egresos {

    String fecha, hora, descripcion, nombre_contacto, cedula_contacto, dir_contacto, telefono_contacto, nombre_user, nombre_cuenta;
    int id, id_user, id_cuenta, id_contacto, caja;
    double total;
    DecimalFormat formatea = new DecimalFormat("###,###.##");

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getHora() {
        return hora;
    }

    public int getCaja() {
        return caja;
    }

    public void setCaja(int caja) {
        this.caja = caja;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_user() {
        return id_user;
    }

    public void setId_user(int id_user) {
        this.id_user = id_user;
    }

    public int getId_cuenta() {
        return id_cuenta;
    }

    public void setId_cuenta(int id_cuenta) {
        this.id_cuenta = id_cuenta;
    }

    public int getId_contacto() {
        return id_contacto;
    }

    public void setId_contacto(int id_contacto) {
        this.id_contacto = id_contacto;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getNombre_contacto() {
        return nombre_contacto;
    }

    public void setNombre_contacto(String nombre_contacto) {
        this.nombre_contacto = nombre_contacto;
    }

    public String getNombre_user() {
        return nombre_user;
    }

    public void setNombre_user(String nombre_user) {
        this.nombre_user = nombre_user;
    }

    public String getNombre_cuenta() {
        return nombre_cuenta;
    }

    public void setNombre_cuenta(String nombre_cuenta) {
        this.nombre_cuenta = nombre_cuenta;
    }

    public String getCedula_contacto() {
        return cedula_contacto;
    }

    public void setCedula_contacto(String cedula_contacto) {
        this.cedula_contacto = cedula_contacto;
    }

    public String getDir_contacto() {
        return dir_contacto;
    }

    public void setDir_contacto(String dir_contacto) {
        this.dir_contacto = dir_contacto;
    }

    public String getTelefono_contacto() {
        return telefono_contacto;
    }

    public void setTelefono_contacto(String telefono_contacto) {
        this.telefono_contacto = telefono_contacto;
    }

    public static Egresos traer_egreso(String id) {
        Egresos g = new Egresos();

        ResultSet rs = DB_consultas_R_D.getTabla("select g.id, u.nombre as user, cu.nombre as cuenta, c.nombre as contacto, c.id as id_contacto,c.cedula,c.contacto as telefono,"
                + "c.direccion, g.descripcion, g.total, g.fecha, g.hora, g.caja from egresos g, users u, contactos c, cuentas cu "
                + "where g.id_user=u.id and g.id_cuenta=cu.id and g.id_contacto=c.id and g.id=" + id);

        try {
            while (rs.next()) {
                g.setId(rs.getInt("id"));
                g.setNombre_user(rs.getString("user"));
                g.setNombre_cuenta(rs.getString("cuenta"));
                g.setDescripcion(rs.getString("descripcion"));
                g.setTotal(rs.getDouble("total"));
                g.setFecha(rs.getString("fecha"));
                g.setHora(rs.getString("hora"));
                g.setCaja(rs.getInt("caja"));
                g.setNombre_contacto(rs.getString("contacto"));
                g.setCedula_contacto(rs.getString("cedula"));
                g.setTelefono_contacto(rs.getString("telefono"));
                g.setDir_contacto(rs.getString("direccion"));
                g.setId_contacto(Integer.parseInt(rs.getString("id_contacto")));

            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
        }

        return g;
    }

    public void VerOtrosEgresos(JTable tabla) {
        jd_ver_in_egre frm = new jd_ver_in_egre();

        int fila = tabla.getSelectedRow();
        if (fila < 0) {
            JOptionPane.showMessageDialog(null, "Seleccione un registro");
        } else {
            String id = "" + tabla.getValueAt(fila, 0);
            ResultSet rs;
            rs = DB_consultas_R_D.getTabla("select u.nombre as user, c.nombre, c.cedula, e.descripcion, e.fecha, e.hora, e.total, cu.nombre as cuenta "
                    + "from cuentas cu, users u, contactos c, egresos e where e.id_cuenta=cu.id and e.id_user=u.id and e.id_contacto=c.id and e.id=" + id);
            try {
                while (rs.next()) {
                    jd_ver_in_egre.lbl_user.setText(rs.getString("user"));
                    jd_ver_in_egre.lbl_nombre_contacto.setText(rs.getString("nombre"));
                    jd_ver_in_egre.lbl_cedula_contacto.setText(rs.getString("cedula"));
                    jd_ver_in_egre.lbl_total.setText("$ " + formatea.format(rs.getDouble("total")));
                    jd_ver_in_egre.lbl_fecha.setText(rs.getString("fecha"));
                    jd_ver_in_egre.lbl_hora.setText(rs.getString("hora"));
                    jd_ver_in_egre.jtxa_descripcion.setText(rs.getString("descripcion"));
                    jd_ver_in_egre.lbl_cuenta_nombre.setVisible(true);
                    jd_ver_in_egre.lbl_cuenta_nombre.setText(rs.getString("cuenta"));
                    jd_ver_in_egre.lbl_cuenta_titulo.setVisible(true);
                }
                rs.close();

            } catch (SQLException ex) {
                Logger.getLogger(jd_Egresos_diarios.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        frm.show();
    }
}

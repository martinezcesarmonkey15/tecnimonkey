/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import conexiondb.DB_consultas_R_D;
import java.sql.ResultSet;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Monkeyelgrande
 */
public class Fondos {

    String nombre;
    int id, predeterminado;

    public Fondos() {
    }

    public Fondos(int id, String nombre) {
        this.nombre = nombre;
        this.id = id;
    }

    public int getPredeterminado() {
        return predeterminado;
    }

    public void setPredeterminado(int predeterminado) {
        this.predeterminado = predeterminado;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static void mostrarFondos(JComboBox<Fondos> jbox) {
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        try {
            ResultSet rs = DB_consultas_R_D.getTabla("select id,nombre from fondos order by predeterminado desc, nombre");
            while (rs.next()) {
                modeloCombo.addElement(new Fondos(rs.getInt("id"), rs.getString("nombre")));
                jbox.setModel(modeloCombo);
            }
        } catch (Exception e) {
        }
        AutoCompleteDecorator.decorate(jbox);
    }

    public static int TraerPredeterminado() {
        int id_fondo = 0;
        try {
            ResultSet rs = DB_consultas_R_D.getTabla("select id from fondos where predeterminado =1");
            while (rs.next()) {

                id_fondo = rs.getInt("id");
            }
        } catch (Exception e) {
        }

        return id_fondo;
    }

    @Override
    public String toString() {
        return nombre;
    }
}

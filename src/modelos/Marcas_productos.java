/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import conexiondb.DB_consultas_R_D;
import java.sql.ResultSet;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Monkeyelgrande
 */
public class Marcas_productos {

    String nombre;
    int id;

    public Marcas_productos() {
    }

    public Marcas_productos(int id, String nombre) {
        this.nombre = nombre;
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void mostrarMarcas(JComboBox<Marcas_productos> jbox_marca) {
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        modeloCombo.addElement(new Marcas_productos(0, ""));

        try {
            ResultSet rs = DB_consultas_R_D.getTabla("select id,nombre from marcas order by nombre");
            while (rs.next()) {
                modeloCombo.addElement(new Marcas_productos(rs.getInt("id"), rs.getString("nombre")));
                jbox_marca.setModel(modeloCombo);
            }
        } catch (Exception e) {
        }
        AutoCompleteDecorator.decorate(jbox_marca);
    }

    @Override
    public String toString() {
        return nombre;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import conexiondb.DB_consultas_R_D;
import java.sql.ResultSet;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Monkeyelgrande
 */
public class Marcas_equipos {

    String nombre;
    int id;

    public Marcas_equipos() {
    }

    public Marcas_equipos(int id, String nombre) {
        this.nombre = nombre;
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static void mostrarMarcas(JComboBox<Marcas_equipos> jbox_marca) {
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        modeloCombo.addElement(new Marcas_equipos(0, ""));

        try {
            ResultSet rs = DB_consultas_R_D.getTabla("select id,nombre from marcas_equipos order by nombre");
            while (rs.next()) {
                modeloCombo.addElement(new Marcas_equipos(rs.getInt("id"), rs.getString("nombre")));
                jbox_marca.setModel(modeloCombo);
            }
        } catch (Exception e) {
        }
        AutoCompleteDecorator.decorate(jbox_marca);
    }

    @Override
    public String toString() {
        return nombre;
    }
}

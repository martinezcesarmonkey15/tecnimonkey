/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

/**
 *
 * @author Monkeyelgrande
 */
public class Ordenes {

    int id, id_equipo, id_cliente, id_tecnico, estado;
    double total;
    String problema, informe, fecha_ingresado, hora_ingresado, fecha_compromiso, fecha_entregado, hora_entregado, serial, password, codigo_factura;

    public Ordenes() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId_equipo() {
        return id_equipo;
    }

    public void setId_equipo(int id_equipo) {
        this.id_equipo = id_equipo;
    }

    public int getId_cliente() {
        return id_cliente;
    }

    public void setId_cliente(int id_cliente) {
        this.id_cliente = id_cliente;
    }

    public int getId_tecnico() {
        return id_tecnico;
    }

    public void setId_tecnico(int id_tecnico) {
        this.id_tecnico = id_tecnico;
    }

    public int getEstado() {
        return estado;
    }

    public void setEstado(int estado) {
        this.estado = estado;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public String getProblema() {
        return problema;
    }

    public void setProblema(String problema) {
        this.problema = problema;
    }

    public String getInforme() {
        return informe;
    }

    public void setInforme(String informe) {
        this.informe = informe;
    }

    public String getFecha_ingresado() {
        return fecha_ingresado;
    }

    public void setFecha_ingresado(String fecha_ingresado) {
        this.fecha_ingresado = fecha_ingresado;
    }

    public String getHora_ingresado() {
        return hora_ingresado;
    }

    public void setHora_ingresado(String hora_ingresado) {
        this.hora_ingresado = hora_ingresado;
    }

    public String getFecha_compromiso() {
        return fecha_compromiso;
    }

    public void setFecha_compromiso(String fecha_compromiso) {
        this.fecha_compromiso = fecha_compromiso;
    }

    public String getFecha_entregado() {
        return fecha_entregado;
    }

    public void setFecha_entregado(String fecha_entregado) {
        this.fecha_entregado = fecha_entregado;
    }

    public String getHora_entregado() {
        return hora_entregado;
    }

    public void setHora_entregado(String hora_entregado) {
        this.hora_entregado = hora_entregado;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCodigo_factura() {
        return codigo_factura;
    }

    public void setCodigo_factura(String codigo_factura) {
        this.codigo_factura = codigo_factura;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelos;

import conexiondb.DB_consultas_R_D;
import java.sql.ResultSet;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;

/**
 *
 * @author Monkeyelgrande
 */
public class Equipos {

    String modelo, serial;
    int id, id_tipo_equipo, id_marca;

    public Equipos() {
    }

    public Equipos(int id, String modelo) {
        this.id = id;
        this.modelo = modelo;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public int getId_tipo_equipo() {
        return id_tipo_equipo;
    }

    public void setId_tipo_equipo(int id_tipo_equipo) {
        this.id_tipo_equipo = id_tipo_equipo;
    }

    public int getId_marca() {
        return id_marca;
    }

    public void setId_marca(int id_marca) {
        this.id_marca = id_marca;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static String TraerNombreMarcaConIdModelo(int id_modelo) {
        String nombre = "";
        try {
            ResultSet rs = DB_consultas_R_D.getTabla("select ma.nombre from equipos e, marcas_equipos ma where e.id_marca=ma.id and e.id=" + id_modelo);
            while (rs.next()) {
                nombre = rs.getString("nombre");

            }
            rs.close();

        } catch (Exception e) {
        }
        return nombre;
    }

    public static String TraerNombreTipoConIdModelo(int id_modelo) {
        String nombre = "";
        try {
            String consulta = "select t.nombre from equipos e, tipos_equipos t where e.id_tipo_equipo=t.id and e.id=" + id_modelo;
            ResultSet rs = DB_consultas_R_D.getTabla(consulta);
            while (rs.next()) {
                nombre = rs.getString("nombre");

            }
            rs.close();

        } catch (Exception e) {
        }
        return nombre;
    }

    public static void mostrarModelosXTipoYMarca(JComboBox<Equipos> jbox_marca, int id_tipo, int id_marca) {
        DefaultComboBoxModel modeloCombo = new DefaultComboBoxModel();
        modeloCombo.addElement(new Equipos(0, ""));

        String tipo = "id_tipo_equipo=" + id_tipo + " ";

        String marca = "id_marca=" + id_marca + " ";

        String consulta = "select id,modelo from equipos where " + tipo + " and " + marca + " order by modelo";

        if (id_tipo == 0 && id_marca == 0) {
            consulta = "select id,modelo from equipos where id_tipo_equipo=0 and id_marca=0 order by modelo";
        }

        if (id_tipo > 0 && id_marca == 0) {
            consulta = "select id,modelo from equipos where " + tipo + " order by modelo";
        }

        if (id_tipo == 0 && id_marca > 0) {
            consulta = "select id,modelo from equipos where " + marca + " order by modelo";
        }

//        System.out.println(consulta);
        try {
            ResultSet rs = DB_consultas_R_D.getTabla(consulta);
            while (rs.next()) {
                modeloCombo.addElement(new Equipos(rs.getInt("id"), rs.getString("modelo")));
            }
            jbox_marca.setModel(modeloCombo);
            rs.close();
        } catch (Exception e) {
        }
        AutoCompleteDecorator.decorate(jbox_marca);
    }

    @Override
    public String toString() {
        return modelo;
    }
}

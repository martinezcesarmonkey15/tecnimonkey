/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Transferencias;

/**
 *
 * @author Monkeyelgrande
 */
public class DB_transferencias {

    public int Guardar(Transferencias trans) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO transferencias (id,id_user,id_fondo_origen,id_fondo_destino,descripcion,total,fecha,hora) "
                + "VALUES (" + trans.getId() + "," + trans.getId_user() + "," + trans.getId_fondo_origen()+ "," + trans.getId_fondo_destino()+ ",'" + trans.getDescripcion()
                + "'," + trans.getTotal() + "," + trans.getFecha() + ",'" + trans.getHora() + "');";

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(Transferencias trans) {
        int resultado = 0;
        Connection con = null;
        String SQL = "";
        SQL = "UPDATE ingresos set "
                + "id_user=" + trans.getId_user() + ", "
                + "id_fondo_origen=" + trans.getId_fondo_origen() + ", "
                + "id_fondo_destino=" + trans.getId_fondo_destino()+ ", "
                + "descripcion='" + trans.getDescripcion() + "', "
                + "total=" + trans.getTotal() + ", "
                + "fecha=" + trans.getFecha() + ", "
                + "hora='" + trans.getHora() + "' "
                + "where id=" + trans.getId();

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Equipos;

/**
 *
 * @author Monkeyelgrande
 */
public class DB_Equipos {

    public int Guardar(Equipos equ) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO equipos (id,id_marca,id_tipo_equipo,modelo) "
                + "VALUES (?, ?, ?, ?)";
        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            psql.setInt(1, equ.getId());
            psql.setInt(2, equ.getId_marca());
            psql.setInt(3, equ.getId_tipo_equipo());
            psql.setString(4, equ.getModelo());

            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(Equipos modelo) {
        int resultado = 0;
        Connection con = null;

        String SQL = "UPDATE modelos set "
                + "modelo='" + modelo.getModelo() + "' "
                + "id_marca=" + modelo.getId_marca() + ", "
                + "id_tipo_equipo=" + modelo.getId_tipo_equipo() + " "
                + "where id=" + modelo.getId();
        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            System.out.println(e);
        }
        return resultado;
    }

    
}

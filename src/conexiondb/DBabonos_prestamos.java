/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Abonos_prestamos;
import modelos.Cuentas;

/**
 *
 * @author Monkeyelgrande
 */
public class DBabonos_prestamos {

    public int Guardar(Abonos_prestamos abonos) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO abonos_prestamos (id,id_prestamo,id_user,id_fondo,abono,fecha,hora) "
                + "VALUES (" + abonos.getId() + "," + abonos.getId_prestamo() + "," + abonos.getId_user() + "," + abonos.getId_fondo() + "," + abonos.getAbono() + ",'" + abonos.getFecha() + "','" + abonos.getHora() + "')";
        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);

            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Anticipos_Servicios;

/**
 *
 * @author Monkeyelgrande
 */
public class DB_Anticipos_Servicios {

    public int Guardar(Anticipos_Servicios anticipo) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO anticipos_servicios (id,id_user,id_contacto,id_fondo,descripcion,total,fecha,hora) "
                + "VALUES (" + anticipo.getId() + "," + anticipo.getId_user() + "," + anticipo.getId_contacto()+ "," + anticipo.getId_fondo()
                + ",'" + anticipo.getDescripcion() + "'," + anticipo.getTotal() + "," + anticipo.getFecha() + ",'" + anticipo.getHora() + "')";

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(Anticipos_Servicios anticipo) {
        int resultado = 0;
        Connection con = null;
        String SQL = "UPDATE anticipos_servicios set "
                + "id_user=" + anticipo.getId_user() + ", "
                + "id_contacto=" + anticipo.getId_contacto() + ", "
                + "id_fondo=" + anticipo.getId_fondo()+ ", "
                + "descripcion='" + anticipo.getDescripcion() + "', "
                + "total=" + anticipo.getTotal() + ", "
                + "fecha=" + anticipo.getFecha() + ", "
                + "hora='" + anticipo.getHora() + "' "
                + "where id=" + anticipo.getId();

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }


}

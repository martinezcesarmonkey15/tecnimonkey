/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Prestamos;

/**
 *
 * @author Monkeyelgrande
 */
public class DBPrestamos {

    public int Guardar(Prestamos prestamo) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO prestamos (id,id_user,id_contacto,descripcion,total,fecha,hora,caja,id_cuenta) "
                + "VALUES (" + prestamo.getId() + "," + prestamo.getId_user() + "," + prestamo.getId_contacto() 
                + ",'" + prestamo.getDescripcion() + "'," + prestamo.getTotal() + "," + prestamo.getFecha() + ",'" + prestamo.getHora() + "', " + prestamo.getCaja() + ", "
                + prestamo.getId_cuenta() + ")";

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(Prestamos prestamo) {
        int resultado = 0;
        Connection con = null;
        String SQL = "";
        SQL = "UPDATE prestamos set "
                + "id_user=" + prestamo.getId_user() + ", "
                + "id_contacto=" + prestamo.getId_contacto() + ", "
                + "descripcion='" + prestamo.getDescripcion() + "', "
                + "total=" + prestamo.getTotal() + ", "
                + "fecha=" + prestamo.getFecha() + ", "
                + "hora='" + prestamo.getHora() + "', "
                + "caja=" + prestamo.getCaja() + ", "
                + "id_cuenta=" + prestamo.getId_cuenta() + " "
                + "where id=" + prestamo.getId();

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }
}

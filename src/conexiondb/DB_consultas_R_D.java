/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.awt.Desktop;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.*;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

/**
 *
 * @author Monkeyelgrande
 */
public class DB_consultas_R_D {

    public static String database_name;
    public static String ip;

    public static String url = "";
    public static String usuario = "postgres";
    public static String contrasenia = "monkey";

    public static double SumarCampoDoubleConSQL(String consulta) {
        String cadena = consulta;
        ResultSet rs = getTabla(cadena);
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                return (rs.getDouble("sum"));
            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
        return 0;
    }

    public static void Abrir_Archivo(String archivo) {
        try {
            File objetofile = new File(archivo);
            Desktop.getDesktop().open(objetofile);
        } catch (IOException ex) {
            System.out.println(ex);
        }
    }

    public static void Eliminar_Archivo(String archivo) {
        File objetofile = new File(archivo);
        objetofile.delete();
    }

    public static void consulta_database_name(String archivo) throws FileNotFoundException, IOException {
        String cadena;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        while ((cadena = b.readLine()) != null) {
//            System.out.println(cadena);
            database_name = cadena;
        }
        b.close();
    }

    public static void consulta_database_ip(String archivo) throws FileNotFoundException, IOException {
        String ip_consultada;
        FileReader f = new FileReader(archivo);
        BufferedReader b = new BufferedReader(f);
        while ((ip_consultada = b.readLine()) != null) {
//            System.out.println(ip_consultada);
            ip = ip_consultada;
        }
        b.close();
    }

    public static Connection getConexion() {

        try {
            consulta_database_name(new File("").getAbsolutePath() + "/src/database_name.txt");
            consulta_database_ip(new File("").getAbsolutePath() + "/src/ip.txt");
            url = "jdbc:postgresql://" + ip + ":5432/" + database_name;
//            System.out.println(url);
        } catch (Exception e) {
            System.out.println("Error en getConexion: " + e);
        }

        Connection cn = null;
        try {
            Class.forName("org.postgresql.Driver");
            cn = DriverManager.getConnection(url, usuario, contrasenia);
        } catch (Exception e) {
            System.out.println(String.valueOf(e));
            JOptionPane.showMessageDialog(null, "Error de conexxion a la base de datos:\n " + e);
        }
        return cn;
    }

    public static ResultSet getTabla(String Consulta) {
        Connection cn = getConexion();
        Statement st;
        ResultSet datos = null;
        try {
            st = cn.createStatement();
            datos = st.executeQuery(Consulta);
            cn.close();
        } catch (Exception e) {
            System.out.print(e.toString());
        }

        return datos;
    }

    public static boolean eliminar(String tabla, String id) {

        Connection con = null;
        String SSQL = "delete from " + tabla + " where id =  '" + id + "' ";
        try {
            con = getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            psql.executeUpdate();
            psql.close();
            con.close();
        } catch (SQLException e) {
            JOptionPane.showMessageDialog(null, "No se puede elimiar: \n" + e);
            return false;
        }
        return true;
    }

    public static int consultarId(String id, String tabla) {
        String cadena = "select count(id) as id from " + tabla + " where id = " + id;
        ResultSet rs = getTabla(cadena);
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                return Integer.parseInt(rs.getString("id"));
            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
        return 0;
    }

    public static int TraerIdUser(String user_name) {
        String cadena = "select id  from users where user_name= '" + user_name + "'";
        ResultSet rs = getTabla(cadena);
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                return Integer.parseInt(rs.getString("id"));
            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
        return 0;
    }

    public static int TraerIdPerfil(int id_user) {
        String cadena = "select id_perfil from users where id= " + id_user;
        ResultSet rs = getTabla(cadena);
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                return Integer.parseInt(rs.getString("id_perfil"));
            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
        return 0;
    }

    public static int consultar_existencia_campo_String(String campo, String valor, String tabla) {
        String cadena = "select count(" + campo + ") as codigo from " + tabla + " where " + campo + " = '" + valor + "'";
        ResultSet rs = getTabla(cadena);
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                return rs.getInt("codigo");
            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
        return 0;
    }

    public static double consultar_inventario(String codigo_barras) {
        String consulta = "select * from stock where codigo_barras='" + codigo_barras + "'";
//        System.out.println(consulta);
        double resultado = 0;
        ResultSet rs = getTabla(consulta);
        try {
            while (rs.next()) {
                resultado = rs.getDouble("stock");
            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
        return resultado;
    }

    public static boolean cosultar_es_servicio(String codigo_barras) {
        String consulta = "select * from productos where codigo_barras='" + codigo_barras + "' and servicio=1";
//        System.out.println(consulta);
        ResultSet rs = getTabla(consulta);
        try {
            while (rs.next()) {
                if (rs.getDouble("servicio") == 1) {
                    return true;
                }

            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
            return false;
        }
        return false;
    }

    public static int Login(String user_name, String pass) {
        String cadena = "select * from users where user_name='" + user_name + "' and password='" + pass + "'";
        ResultSet rs = getTabla(cadena);
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                return 1;
            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
        return 0;
    }

    public static String cargarId(String tabla) {
        String resultado = "";
        ResultSet rs = DB_consultas_R_D.getTabla("select COALESCE(max(id),0)+1 as id from " + tabla);
        try {
            while (rs.next()) {
                resultado = (rs.getString("id"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        if (resultado == null) {
            resultado = "1";
        }
        return resultado;
    }

    public static int cargarId_return_int(String tabla) {
        int resultado = 0;
        ResultSet rs = DB_consultas_R_D.getTabla("select COALESCE(max(id),0)+1 as id from " + tabla);
        try {
            while (rs.next()) {
                resultado = (rs.getInt("id"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        if (resultado == 0) {
            resultado = 1;
        }
        return resultado;
    }

    public static String Consultar_id_crap(String id_factura) {
        String resultado = "";
        ResultSet rs = DB_consultas_R_D.getTabla("select id from creditos_apartados where id_factura =" + id_factura);
        try {
            while (rs.next()) {
                resultado = (rs.getString("id"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        if (resultado == null) {
            resultado = "1";
        }
        return resultado;
    }

    public static void validar_numeros(java.awt.event.KeyEvent evt, char car) {
        if ((car < '0' || car > '9')) {
            if (car == '.') {
            } else {
                evt.consume();
            }
        }
    }

    public static double consultar_porcentajes(String campo) {
        String cadena = "select " + campo + " as campo from configuraciones";
        ResultSet rs = getTabla(cadena);
        try {
            while (rs.next()) {
                // añade los resultado a al modelo de tabla
                return Double.parseDouble(rs.getString("campo"));
            }
            rs.close();

        } catch (Exception e) {
            System.out.println(e);
            return 0;
        }
        return 0;
    }

    public static String obtener_fecha() {
        String fecha = "";
        Calendar calendario = new GregorianCalendar();
        int dia, mes, ano;

        dia = calendario.get(Calendar.DAY_OF_MONTH);
        mes = calendario.get(Calendar.MARCH) + 1;
        ano = calendario.get(Calendar.YEAR);

        fecha = ano + "-" + mes + "-" + dia;
        return fecha;
    }

    public static String obtener_fecha_dia1() {
        String fecha = "";
        Calendar calendario = new GregorianCalendar();
        int dia, mes, ano;

        dia = 1;
        mes = calendario.get(Calendar.MARCH) + 1;
        ano = calendario.get(Calendar.YEAR);

        fecha = ano + "-" + mes + "-" + dia;
        return fecha;
    }

    public static String obtener_fecha_dia_ultimo() {
        String fecha = "";
        Calendar calendario = new GregorianCalendar();
        int dia, mes, ano;

        dia = calendario.getActualMaximum(Calendar.DAY_OF_MONTH);
        mes = calendario.get(Calendar.MARCH) + 1;
        ano = calendario.get(Calendar.YEAR);

        fecha = ano + "-" + mes + "-" + dia;
        return fecha;
    }

    public static void Actualizar_Campo_String_campo_string(String tabla, String campo, String id, String dato_nuevo) throws SQLException {

        Connection con = null;
        String SQL = "update " + tabla + " set " + campo + "='" + dato_nuevo + "' where " + campo + "=" + id;

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }

    }

    public static String obtener_hora() {
        String fecha = "";
        Calendar calendario = new GregorianCalendar();
        int hora, minutos, segundos;
        hora = calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);

        fecha = hora + ":" + minutos + ":" + segundos;
        return fecha;
    }

    public static int traer_id_con_cod_barras(String codigo_bar) {
        int resultado = 0;
        ResultSet rs = DB_consultas_R_D.getTabla("select id from productos where codigo_barras='" + codigo_bar + "'");
        try {
            while (rs.next()) {
                resultado = (rs.getInt("id"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;

    }

    public static String ImpresoraPredeterminada() {
        String resultado = "";
        ResultSet rs = DB_consultas_R_D.getTabla("select nombre_impresora from configuraciones");
        try {
            while (rs.next()) {
                resultado = (rs.getString("nombre_impresora"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;

    }

    public static int Imprimir_si_no() {
        int resultado = 0;
        ResultSet rs = DB_consultas_R_D.getTabla("select imprimir_factura from configuraciones");
        try {
            while (rs.next()) {
                resultado = (rs.getInt("imprimir_factura"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;

    }

    public static String Tipo_impresora() {
        String resultado = "";
        ResultSet rs = DB_consultas_R_D.getTabla("select tipo_impresora from configuraciones");
        try {
            while (rs.next()) {
                resultado = (rs.getString("tipo_impresora"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;

    }

    public static int productos_repetidos() {
        int resultado = 0;
        ResultSet rs = DB_consultas_R_D.getTabla("select productos_repetidos from configuraciones");
        try {
            while (rs.next()) {
                resultado = (rs.getInt("productos_repetidos"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        return resultado;

    }

    public static String Ruta_Imagenes() {
        String resultado = "";
        ResultSet rs = DB_consultas_R_D.getTabla("select ruta_imagenes from configuraciones");
        try {
            while (rs.next()) {
                resultado = (rs.getString("ruta_imagenes"));
            }
            rs.close();
        } catch (Exception e) {
            System.out.println(e);
        }
        if (resultado == null) {
            resultado = "1";
        }
        return resultado;
    }

    public static String obtener_hora_con_guiones() {
        String fecha = "";
        Calendar calendario = new GregorianCalendar();
        int hora, minutos, segundos;
        hora = calendario.get(Calendar.HOUR_OF_DAY);
        minutos = calendario.get(Calendar.MINUTE);
        segundos = calendario.get(Calendar.SECOND);

        fecha = hora + "-" + minutos + "-" + segundos;
        return fecha;
    }

}

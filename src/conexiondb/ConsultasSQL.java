/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

/**
 *
 * @author Monkeyelgrande
 */
public class ConsultasSQL {

    public static String ConsultaFacturasPorTipo(String tipo) {
        return "select f.id,f.codigo,c.nombre,fecha,monto_descuento+total as subtotal,monto_descuento,total from facturas_cabeceras f, contactos c "
                + "where f.id_contacto=c.id and f.estado='1' and tipo_factura in('" + tipo + "')order by f.fecha desc";
    }

    public static String ConsultaFacturasPorTipoHoy(String tipo) {
        return "select f.id,f.codigo,c.nombre,fecha,monto_descuento+total as subtotal,monto_descuento,total from facturas_cabeceras f, contactos c "
                + "where f.id_contacto=c.id and f.estado='1' and tipo_factura in('" + tipo + "') and f.fecha=CURRENT_DATE";
    }

    public static String ConsultaFacturasAnuladas() {
        return "select f.id,f.codigo,c.nombre,fecha,monto_descuento+total as subtotal,monto_descuento,total, tipo_factura from facturas_cabeceras f, contactos c "
                + "where f.id_contacto=c.id and estado='0' order by f.fecha desc";
    }

    public static String ConsultaFacturasAbonos() {
        String consulta = "with consulta as (\n"
                + "select ca.id as id_crap, f.id as id_factura, c.nombre as cliente, c.cedula, f.total as total_factura, sum(a.abono) as abono_total, (f.total-( sum(a.abono))-coalesce(ca.descuento,0)) as saldo, \n"
                + "ca.fecha_plazo_final as fecha_fin, f.tipo_factura, (current_date-ca.fecha_plazo_final) as dias_vence, ca.descuento  \n"
                + "\n"
                + "from facturas_cabeceras f, creditos_apartados ca, contactos c, abonos a \n"
                + "\n"
                + "where ca.id_factura=f.id and f.id_contacto=c.id and a.id_creditos_apartados=ca.id and f.estado='1' \n"
                + "group by ca.id, f.id, c.nombre, c.cedula, f.total \n"
                + "order by ca.fecha_plazo_final desc\n"
                + ")\n"
                + "select * from consulta where saldo>0 ;";
//        System.out.println(consulta);
        return consulta;
    }

    public static String ConsultaFacturasAbonosConPagado() {
        String consulta = "select ca.id as id_crap, f.id as id_factura, c.nombre as cliente,c.cedula, f.total as total_factura, sum(a.abono) as abono_total, (f.total-(sum(a.abono))-coalesce(ca.descuento,0)) as saldo, \n"
                + "ca.fecha_plazo_final as fecha_fin, f.tipo_factura, (current_date-ca.fecha_plazo_final) as dias_vence, ca.descuento  \n"
                + "from facturas_cabeceras f, creditos_apartados ca, contactos c, abonos a  \n"
                + "where ca.id_factura=f.id and f.id_contacto=c.id and f.estado='1' and a.id_creditos_apartados=ca.id \n"
                + "group by ca.id, f.id, c.nombre, c.cedula, f.total \n"
                + "order by ca.fecha_plazo_final desc";
//        System.out.println(consulta);
        return consulta;
    }

    public static String ConsultaFacturasAbonosCreditos() {
        return "select ca.id as id_crap, f.id as id_factura, c.nombre as cliente, f.total as total_factura, ca.abono as abono_total, "
                + "(f.total-ca.abono) as saldo, ca.fecha_plazo_final as fecha_fin, f.tipo_factura from facturas_cabeceras f, creditos_apartados ca, contactos c "
                + "where ca.id_factura=f.id and f.id_contacto=c.id and f.tipo_factura='Credito' and f.estado='1' and (f.total-ca.abono)>0 order by id_factura";
    }

    public static String ConsultaFacturasAbonosApartados() {
        return "select ca.id as id_crap, f.id as id_factura, c.nombre as cliente, f.total as total_factura, ca.abono as abono_total, "
                + "(f.total-ca.abono) as saldo, ca.fecha_plazo_final as fecha_fin, f.tipo_factura from facturas_cabeceras f, creditos_apartados ca, contactos c "
                + "where ca.id_factura=f.id and f.id_contacto=c.id and f.tipo_factura='Apartado' and f.estado='1' and (f.total-ca.abono)>0 order by id_factura";
    }

    public static String ConsultaFacturasPorFecha_reporte(String fecha) {
        String consulta = "select f.id, f.total as total, c.nombre \n"
                + "from facturas_cabeceras f, contactos c  \n"
                + "where f.id_contacto=c.id and f.tipo_factura='Venta' and f.estado='1' and f.fecha='" + fecha + "' "
                + "group by f.id, c.nombre";
//        System.out.println(consulta);
        return consulta;
    }

    public static String ConsultaFacturasEntreFechas_reporte(String fecha1, String fecha2) {
        return "select f.id, c.nombre, f.total, f.fecha from facturas_cabeceras f, contactos c "
                + "where f.id_contacto=c.id and f.tipo_factura='Venta' and f.estado='1' and f.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by f.fecha";
    }

    public static String ConsultaFacturasEntreFechas_por_usuario(String fecha1, String fecha2, String id_user) {
        return "select f.id, c.nombre, f.total, f.fecha from facturas_cabeceras f, contactos c "
                + "where f.id_contacto=c.id and f.tipo_factura='Venta' and f.estado='1' and f.id_user = " + id_user + " and f.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by f.fecha";
    }

    public static String Consulta_Creditos_Apartados_EntreFechas_por_usuario(String fecha1, String fecha2, String id_user) {
        return "select f.id, c.nombre, f.total, f.fecha from facturas_cabeceras f, contactos c "
                + "where f.id_contacto=c.id and f.tipo_factura <> 'Venta' and f.estado='1' and f.id_user = " + id_user + " and f.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by f.fecha";
    }

    public static String ConsultaEgresosPorFecha_reporte(String fecha) {
        return "select g.id,co.nombre as contacto, c.nombre, g.total from egresos g, cuentas c, contactos co where g.id_contacto=co.id and g.id_cuenta=c.id and g.fecha='" + fecha + "'";
    }

    public static String ConsultaEgresosEntreFecha_reporte(String fecha1, String fecha2) {
        return "select g.id,co.nombre as contacto, c.nombre, g.total, g.fecha from egresos g, cuentas c, contactos co where g.id_contacto=co.id and g.id_cuenta=c.id "
                + "and g.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by g.fecha";
    }

    public static String ConsultaAbonosPorFecha_reporte(String fecha) {

        String consulta = "select f.id, c.nombre, a.abono\n"
                + "from facturas_cabeceras f, contactos c, abonos a, creditos_apartados ca \n"
                + "where f.id_contacto=c.id and a.id_creditos_apartados=ca.id and ca.id_factura=f.id and f.estado='1' and  a.fecha='" + fecha + "'";
        System.out.println(consulta);

        return consulta;
    }

    public static String ConsultaAbonosPrestamosPorFecha_reporte(String fecha) {
        return "select p.id, c.nombre, a.abono from prestamos p, contactos c, abonos_prestamos a "
                + "where a.abono>0 and p.id_contacto=c.id and a.id_prestamo=p.id and a.fecha='" + fecha + "'";
    }

    public static String ConsultaAbonosPrestamosEntreFechas_reporte(String fecha1, String fecha2) {
        return "select p.id, c.nombre, a.abono from prestamos p, contactos c, abonos_prestamos a "
                + "where a.abono>0 and p.id_contacto=c.id and a.id_prestamo=p.id and a.fecha between'" + fecha1 + "' and '" + fecha2 + "'";
    }

    public static String ConsultaAbonosEntreFecha_reporte(String fecha1, String fecha2) {
        return "select f.id, c.nombre, a.abono, a.fecha from facturas_cabeceras f, contactos c, abonos a, creditos_apartados ca "
                + "where f.id_contacto=c.id and a.id_creditos_apartados=ca.id and ca.id_factura=f.id and f.estado='1' and  "
                + "a.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by a.fecha";
    }

    public static String ConsultaOtrosIngresosPorFecha_reporte(String fecha) {
        String consutla = "select o.id, c.nombre, o.total, fo.nombre as fondo "
                + "from otros_ingresos o, contactos c, fondos fo "
                + "where o.id_contacto=c.id and o.id_fondo=fo.id and o.fecha='" + fecha + "'";
        return consutla;
    }

    public static String ConsultaOtrosIngresosEntreFecha_reporte(String fecha1, String fecha2) {
        return "select o.id, c.nombre, o.total, o.fecha from otros_ingresos o, contactos c "
                + "where o.id_contacto=c.id and  o.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by o.fecha";
    }

    public static String ConsultaOtrosIngresosEntreFechaPorContacto_reporte(String fecha1, String fecha2, String contacto) {
        return "select o.id, o.descripcion, o.total, o.fecha from otros_ingresos o "
                + "where o.id_contacto=" + contacto + " and o.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by o.fecha";
    }

    public static String ConsultaVentasContadoEntreFechaPorContacto_reporte(String fecha1, String fecha2, String contacto) {
        return "select f.id, f.codigo, f.total, f.fecha from facturas_cabeceras f "
                + "where f.tipo_factura='Venta' and f.estado='1' and id_contacto=" + contacto + " and f.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by f.fecha";
    }

    public static String ConsultaSalidaProductosEntreFecha_reporte(String fecha1, String fecha2) {
        return "select fc.id as id_factura, p.codigo_barras, p.descripcion, fc.fecha, c.nombre as contacto, fd.cantidad "
                + "from facturas_detalles fd, facturas_cabeceras fc, productos p, contactos c "
                + "where fd.id_cabecera=fc.id and fd.id_producto=p.id and fc.id_contacto=c.id and fc.estado='1' and fc.fecha between '" + fecha1 + "' and '" + fecha2 + "' order by fc.fecha";
    }

    public static String ConsultaSalidaProductosEntreFechaXProducto_reporte(String fecha1, String fecha2, String codigo) {

        return "select fc.id as id_factura, fc.fecha, c.nombre as contacto, fd.cantidad, fd.valor_unitario, fd.subtotal "
                + "from facturas_detalles fd, facturas_cabeceras fc, productos p, contactos c "
                + "where fd.id_cabecera=fc.id and fd.id_producto=p.id and fc.id_contacto=c.id and fc.estado='1' and fd.id_producto=" + codigo + " and "
                + "fc.fecha between '" + fecha1 + "' and '" + fecha2 + "' order by fc.fecha";
    }

    public static String ConsultaSalidaProductosEntreFechaXProductoYContacto_reporte(String fecha1, String fecha2, String codigo_producto, String codigo_contacto) {
        return "select fc.id as id_factura, fc.fecha, c.nombre as contacto, fd.cantidad,fd.subtotal,valor_unitario "
                + "from facturas_detalles fd, facturas_cabeceras fc, productos p, contactos c "
                + "where fd.id_cabecera=fc.id and fd.id_producto=p.id and fc.id_contacto=c.id and fd.id_producto=" + codigo_producto + " and "
                + "fc.estado='1' and c.id =" + codigo_contacto + " and "
                + "fc.fecha between '" + fecha1 + "' and '" + fecha2 + "' order by fc.fecha";
    }

    public static String ConsultaEgresosEntreFechaPorContacto_reporte(String fecha1, String fecha2, String contacto) {
        return "select o.id, o.descripcion, o.total, o.fecha, cu.nombre as cuenta from egresos o, cuentas cu "
                + "where o.id_cuenta=cu.id and o.id_contacto=" + contacto + " and o.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by o.fecha";
    }

    public static String ConsultaEgresosEntreFechaPorContactoYCuenta_reporte(String fecha1, String fecha2, String contacto, String cuenta) {
        return "select o.id, o.descripcion, o.total, o.fecha, cu.nombre as cuenta from egresos o, cuentas cu "
                + "where o.id_cuenta=cu.id and o.id_contacto=" + contacto + " and cu.nombre in (" + cuenta + ") and o.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by o.fecha";
    }

    public static String ConsultaEgresosEntreFechaPorCuenta_reporte(String fecha1, String fecha2, String cuentas) {
        return "select e.id, e.descripcion, e.total, e.fecha, c.nombre, cu.nombre as cuenta from egresos e, contactos c, cuentas cu "
                + "where e.id_contacto=c.id and e.id_cuenta=cu.id and cu.nombre in (" + cuentas + ") and e.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by e.fecha";
    }

    public static String ConsultaOtrosEgresosEntreFecha_reporte(String fecha1, String fecha2) {
        return "select o.id, c.nombre, o.total, o.fecha from otros_egresos o, contactos c "
                + "where o.id_contacto=c.id and  o.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by o.fecha";
    }

    public static String ConsultaDevolucionesPorFecha_reporte(String fecha) {
        return "select d.id,c.nombre,d.total from devoluciones d, facturas_cabeceras f,contactos c "
                + "where d.id_factura=f.id and f.id_contacto=c.id and d.fecha='" + fecha + "'";
    }

    public static String ConsultaPrestamosPorFecha_reporte(String fecha) {
        return "select p.id,c.nombre,p.total from prestamos p, contactos c "
                + "where p.id_contacto=c.id and p.fecha='" + fecha + "'";
    }

    public static String ConsultaAnticiposYPagosPorFecha_reporte(String fecha) {
        return "select * from prestamos p, contactos c "
                + "where p.id_contacto=c.id and p.fecha='" + fecha + "'";
    }

    public static String ConsultaPrestamosEntreFecha_reporte(String fecha1, String fecha2) {
        return "select p.id,c.nombre,p.total from prestamos p, contactos c "
                + "where p.id_contacto=c.id and p.fecha between '" + fecha1 + "' and '" + fecha2 + "'";
    }

    public static String ConsultaDevolucionesEntreFecha_reporte(String fecha1, String fecha2) {
        return "select d.id,c.nombre,d.total, d.fecha from devoluciones d, facturas_cabeceras f,contactos c "
                + "where d.id_factura=f.id and f.id_contacto=c.id and d.fecha between'" + fecha1 + "' and '" + fecha2 + "' order by d.fecha";
    }

    public static String ConsultaClientes() {
        return "select distinct c.id,c.nombre,c.cedula,c.contacto,c.ciudad,c.direccion "
                + "from contactos c, facturas_cabeceras f where f.id_contacto=c.id order by c.id";
    }

    public static String ConsultaProveedores() {
        return "select distinct c.id,c.nombre,c.cedula,c.contacto,c.ciudad,c.direccion "
                + "from contactos c, egresos e where e.id_contacto=c.id order by c.id";
    }

    public static String ConsultaSaldoAnteriorProductos(String fecha, String id_producto) {
        return "select  (COALESCE((select sum(cantidad) from ingresos_mercancias where id_producto=" + id_producto + " and fecha between (select min(fecha) from ingresos_mercancias) and '" + fecha + "'),0)+"
                + "COALESCE((select sum(dd.cantidad) from devoluciones dc, devoluciones_detalles dd where dd.id_producto=" + id_producto + " and dc.fecha between (select min(fecha) from devoluciones) and '" + fecha + "'),0)-"
                + "COALESCE((select sum(fd.cantidad) from facturas_cabeceras fc, facturas_detalles fd where fd.id_cabecera=fc.id and fd.id_producto=" + id_producto + " and fc.estado='1' and fc.tipo_factura in ('Venta','Credito','Apartado') "
                + "and fc.fecha between (select min(fecha) from facturas_cabeceras) and '" + fecha + "'),0)-"
                + "COALESCE((select sum(cantidad) from cambios_eliminar c where c.id_producto=" + id_producto + " and c.fecha between (select min(fecha) from cambios_eliminar) and '" + fecha + "'),0)) as sum";
    }

    public static String ConsultaSaldoAnterior(String fechaAnterior) {
        String consulta = "select \n"
                + "(select COALESCE(sum(total),0) from facturas_cabeceras where estado='1' and tipo_factura='Venta' and fecha between (select min(fecha) from facturas_cabeceras) and '" + fechaAnterior + "') \n"
                + "+ (select COALESCE(sum(a.abono),0) from abonos a, creditos_apartados c, facturas_cabeceras f where a.id_creditos_apartados=c.id and c.id_factura=f.id and f.estado='1' and a.fecha between (select min(fecha) from abonos) and '" + fechaAnterior + "') \n"
                + "+ (select COALESCE(sum(a.abono),0) from abonos_creditos a, creditos c, servicios s where a.id_credito=c.id and c.id_servicio=s.id and a.fecha between (select min(fecha) from abonos_creditos) and '" + fechaAnterior + "') \n"
                + "+ (select COALESCE(sum(total),0) from otros_ingresos where fecha between (select min(fecha) from otros_ingresos) and '" + fechaAnterior + "') \n"
                + "+ (select COALESCE(sum(abono),0) from abonos_prestamos where fecha between (select min(fecha) from abonos_prestamos) and '" + fechaAnterior + "') \n"
                + "+ (select COALESCE(sum(abono),0) from servicios where fecha_ingresado between (select min(fecha_ingresado) from servicios) and '" + fechaAnterior + "') \n"
                + "+ (select COALESCE(sum(total_cancelado),0) from servicios where estado=5 and fecha_entregado between (select min(fecha_entregado) from servicios) and '" + fechaAnterior + "') \n"
                + "- (select COALESCE(sum(total),0) from egresos where fecha between (select min(fecha) from egresos) and '" + fechaAnterior + "') \n"
                + "- (select COALESCE(sum(total),0) from devoluciones where fecha between (select min(fecha) from devoluciones) and '" + fechaAnterior + "') \n"
                + "- (select COALESCE(sum(total),0) from prestamos where fecha between (select min(fecha) from prestamos) and '" + fechaAnterior + "') \n"
                + "- (select COALESCE(sum(total),0) from anticipos where fecha between (select min(fecha) from anticipos) and '" + fechaAnterior + "') \n"
                + "- (select COALESCE(sum(total),0) from pagos_facturas where tipo_pago>0 and fecha between (select min(fecha) from pagos_facturas) and '" + fechaAnterior + "') as sum ";
//        System.out.println(consulta);
        return consulta;
    }

}

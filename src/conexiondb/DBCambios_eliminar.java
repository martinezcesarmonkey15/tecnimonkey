/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Eliminar;

/**
 *
 * @author Monkeyelgrande
 */
public class DBCambios_eliminar {

    public int Guardar(Eliminar gastos) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO cambios_eliminar (id,id_user,id_producto,id_tipo_eliminacion, descripcion,cantidad,fecha,hora) "
                + "VALUES (" + gastos.getId() + "," + gastos.getId_user() + "," + gastos.getId_producto() + "," + gastos.getId_tipo_eliminacion()+ " "
                + ",'" + gastos.getDescripcion() + "'," + gastos.getCantidad() + "," + gastos.getFecha() + ",'" + gastos.getHora() + "')";

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(Eliminar gastos) {
        int resultado = 0;
        Connection con = null;
        String SQL = "UPDATE gastos set "
                + "id_user=" + gastos.getId_user() + ", "
//                + "id_cuenta=" + gastos.getId_cuenta() + ", "
//                + "id_contacto=" + gastos.getId_contacto() + ", "
//                + "descripcion='" + gastos.getDescripcion() + "', "
//                + "total=" + gastos.getTotal() + ", "
                + "fecha=" + gastos.getFecha() + ", "
                + "hora='" + gastos.getHora() + "' "
                + "where id=" + gastos.getId();

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }
}

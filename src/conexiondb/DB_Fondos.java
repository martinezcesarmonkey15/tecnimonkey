/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Fondos;

/**
 *
 * @author Monkeyelgrande
 */
public class DB_Fondos {

    public int Guardar(Fondos cuenta) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO fondos (id,nombre,predeterminado) "
                + "VALUES (" + cuenta.getId() + ", '" + cuenta.getNombre() + "', " + cuenta.getPredeterminado() + ")";

        if (cuenta.getPredeterminado() == 1) {
            SSQL = "update fondos set predeterminado=0; "
                    + "INSERT INTO fondos (id,nombre,predeterminado) "
                    + "VALUES (" + cuenta.getId() + ", '" + cuenta.getNombre() + "', " + cuenta.getPredeterminado() + ")";
        }
        System.out.println(SSQL);
        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);

            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(Fondos cuenta) {
        int resultado = 0;
        Connection con = null;

        String SQL = "UPDATE fondos set "
                + "nombre='" + cuenta.getNombre() + "', "
                + "predeterminado=" + cuenta.getPredeterminado() + " "
                + "where id=" + cuenta.getId();

        if (cuenta.getPredeterminado() == 1) {
            SQL = "update fondos set predeterminado=0; "
                    + "UPDATE fondos set "
                    + "nombre='" + cuenta.getNombre() + "', "
                    + "predeterminado=" + cuenta.getPredeterminado() + " "
                    + "where id=" + cuenta.getId();

        }

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import Formularios_intenos_servicio.jif_crear_orden;
import Metodos.metodos;
import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Ordenes;

/**
 *
 * @author Monkeyelgrande
 */
public class DB_Ordenes {

    public int Guardar(Ordenes service) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO ordenes (id, id_equipo, id_cliente, fecha_ingresado, hora_ingresado, fecha_compromiso, serial, password, problema, estado, id_tecnico, informe) "
                + "VALUES (?,?,?,'" + service.getFecha_ingresado() + "','" + service.getHora_ingresado() + "','" + service.getFecha_compromiso() + "', ?, ?, ?, ?, ?, ?)";
        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            psql.setInt(1, service.getId());
            psql.setInt(2, service.getId_equipo());
            psql.setInt(3, service.getId_cliente());
            psql.setString(4, service.getSerial());
            psql.setString(5, service.getPassword());
            psql.setString(6, service.getProblema());
            psql.setInt(7, service.getEstado());
            psql.setInt(8, service.getId_tecnico());
            psql.setString(9, service.getInforme());

            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(Ordenes ord) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "UPDATE ordenes set "
                + "id_equipo=" + ord.getId_equipo() + ", "
                + "id_cliente=" + ord.getId_cliente() + ", "
                + "fecha_ingresado='" + ord.getFecha_ingresado()+ "', "
                + "hora_ingresado='" + ord.getHora_ingresado()+ "', "
                + "fecha_compromiso='" + ord.getFecha_compromiso()+ "', "
                + "serial='" + ord.getSerial()+ "', "
                + "password='" + ord.getPassword()+ "', "
                + "problema='" + ord.getProblema()+ "', "
                + "id_tecnico=" + ord.getId_tecnico()+ ", "
                + "informe='" + ord.getInforme()+ "' "
                + "where id=" + ord.getId();
        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);

            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public static void VerOrden(String id) {

        jif_crear_orden frm = new jif_crear_orden();

        String consulta = "select o.*, c.nombre as nombre_cliente, c.id as id_cliente, t.id as id_tipo, ma.id as id_marca, \n"
                + "t.nombre as tipo_equipo, ma.nombre as marca, e.modelo, coalesce(o.codigo_factura,'-') \n"
                + "\n"
                + "from ordenes o, contactos c, equipos e, marcas_equipos ma, tipos_equipos t \n"
                + "\n"
                + "where o.id_cliente=c.id and o.id_equipo=e.id \n"
                + "and e.id_tipo_equipo=t.id and e.id_marca=ma.id and o.id=" + id;

        System.out.println(consulta);
        int estado = 0;
        try {
            ResultSet rs = DB_consultas_R_D.getTabla(consulta);
            while (rs.next()) {
                jif_crear_orden.lbl_id_orden.setText(rs.getString("id"));
                jif_crear_orden.id_orden = (rs.getString("id"));
                jif_crear_orden.lbl_id_cliente.setText(rs.getString("id_cliente"));
                jif_crear_orden.txt_nombre_cliente.setText(rs.getString("nombre_cliente"));
                jif_crear_orden.jbox_tipo.setSelectedItem(rs.getString("tipo_equipo"));
                jif_crear_orden.jbox_marca.setSelectedItem(rs.getString("marca"));
                jif_crear_orden.jbox_modelo.setSelectedItem(rs.getString("modelo"));
                jif_crear_orden.id_equipo = rs.getInt("id_equipo");
                jif_crear_orden.id_tipo = rs.getInt("id_tipo");
                jif_crear_orden.id_marca = rs.getInt("id_marca");
                jif_crear_orden.txt_serial.setText(rs.getString("serial"));
                jif_crear_orden.txt_password.setText(rs.getString("password"));
                jif_crear_orden.txt_problema.setText(rs.getString("problema"));
                jif_crear_orden.txt_Codigo_factura.setText(rs.getString("codigo_factura"));
                jif_crear_orden.jdate_fecha.setDate(rs.getDate("fecha_ingresado"));
                jif_crear_orden.jdate_fecha_compromiso.setDate(rs.getDate("fecha_compromiso"));

                jif_crear_orden.txt_Total_servicio.setText(metodos.formateador_dinero().format(rs.getDouble("total_facturacion")));

                estado = rs.getInt("estado");

            }

            // MODELOS DE FOTOS
            try {
                for (int i = 0; i < jif_crear_orden.modelo_fotos.getRowCount(); i++) {
                    jif_crear_orden.modelo_fotos.removeRow(i);
                    i -= 1;
                }
            } catch (Exception m) {
                System.out.println(m);
            }

            consulta = "select * from fotos_servicios where id_servicio=" + id;
            rs = DB_consultas_R_D.getTabla(consulta);
            jif_crear_orden.modelo_fotos.setColumnIdentifiers(new Object[]{"Ruta", "Nombre", "id_foto"});
            int fotos = 1;
            while (rs.next()) {

                jif_crear_orden.modelo_fotos.addRow(new Object[]{DB_consultas_R_D.Ruta_Imagenes() + rs.getString("nombre"), rs.getString("nombre"), rs.getString("id")});
                jif_crear_orden.jtabla_fotos.setModel(jif_crear_orden.modelo_fotos);

                if (fotos == 2) {
                    metodos.foto_a_label(DB_consultas_R_D.Ruta_Imagenes() + rs.getString("nombre"), jif_crear_orden.lbl_foto_2);
                }

                if (fotos == 1) {
                    metodos.foto_a_label(DB_consultas_R_D.Ruta_Imagenes() + rs.getString("nombre"), jif_crear_orden.lbl_foto_1);
                    fotos++;
                }

            }

            // MODELOS DE FACTURADOS
            try {
                for (int i = 0; i < jif_crear_orden.modelo_facturar.getRowCount(); i++) {
                    jif_crear_orden.modelo_facturar.removeRow(i);
                    i -= 1;
                }
            } catch (Exception m) {
            }
            consulta = "select p.id, p.codigo_barras, p.descripcion, sc.cantidad, sc.valor_unitario "
                    + "from servicio_facturaciones sc, servicios s, productos p where sc.id_servicio=s.id and sc.id_producto=p.id and s.id=" + id + " order by s.id desc";
            rs = DB_consultas_R_D.getTabla(consulta);
            jif_crear_orden.modelo_facturar.setColumnIdentifiers(new Object[]{"ID", "CODIGO", "DESCRIPCIÓN", "CANTIDAD", "PRECIO", "TOTAL"});

            while (rs.next()) {

                jif_crear_orden.modelo_facturar.addRow(new Object[]{rs.getInt("id"), rs.getString("codigo_barras"), rs.getString("descripcion"),
                    rs.getDouble("cantidad"), metodos.formateador_dinero().format(rs.getDouble("valor_unitario"))});

                jif_crear_orden.jtabla_facturados.setModel(jif_crear_orden.modelo_facturar);
            }

            if (estado <= 5) {

                jif_crear_orden.txt_serial.setEditable(false);
                jif_crear_orden.btn_buscar_cliente.setEnabled(false);
                jif_crear_orden.btn_crear_marca.setEnabled(false);
                jif_crear_orden.btn_crear_modelo.setEnabled(false);
                jif_crear_orden.btn_crear_tipo_equipo.setEnabled(false);
                jif_crear_orden.btn_crear_cliente.setEnabled(false);
                jif_crear_orden.jbox_tipo.setEnabled(false);
                jif_crear_orden.jbox_marca.setEnabled(false);
                jif_crear_orden.jbox_modelo.setEnabled(false);

                jif_crear_orden.btn_cargar_foto.setEnabled(false);
                jif_crear_orden.btn_eliminar_foto.setEnabled(false);

                jif_crear_orden.btn_add_facturados.setEnabled(false);

                // INGRESADO
                if (estado == 1) {
                    jif_crear_orden.jbox_user_trabajando.setEnabled(false);
                    jif_crear_orden.btn_add_facturados.setEnabled(true);
                }

            }

        } catch (Exception e) {
            System.out.println(e);
        }
        frm.txt_serial.requestFocus();
        frm.btn_guardar.setText("ACTUALIZAR");
        frm.show();
    }

    public static int Traer_Estado(String id) {

        String consulta = "select s.estado"
                + "\n"
                + "from servicios s \n"
                + "\n"
                + "where s.id=" + id;
        int estado = 0;

        try {
            ResultSet rs = DB_consultas_R_D.getTabla(consulta);
            while (rs.next()) {

                estado = rs.getInt("estado");

            }

        } catch (Exception e) {
            System.out.println(e);
        }
        return estado;

    }

    public int Actualizar_Estado(Ordenes obj) {
        int resultado = 0;
        Connection con = null;

        String SQL = "UPDATE ordenes set "
                + "estado=" + obj.getEstado() + " "
                + "where id=" + obj.getId();
        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            System.out.println(e);
        }
        return resultado;
    }

}

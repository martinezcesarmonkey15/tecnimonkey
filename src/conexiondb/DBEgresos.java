/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Egresos;
import modelos.Marcas_productos;

/**
 *
 * @author Monkeyelgrande
 */
public class DBEgresos {

    public int Guardar(Egresos egreso) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO egresos (id,id_user,id_cuenta,id_contacto,descripcion,total,fecha,hora,caja) "
                + "VALUES (" + egreso.getId() + "," + egreso.getId_user() + "," + egreso.getId_cuenta() + "," + egreso.getId_contacto()
                + ",'" + egreso.getDescripcion() + "'," + egreso.getTotal() + "," + egreso.getFecha() + ",'" + egreso.getHora() + "', "+egreso.getCaja()+")";

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(Egresos gastos) {
        int resultado = 0;
        Connection con = null;
        String SQL = "";
        SQL = "UPDATE egresos set "
                + "id_user=" + gastos.getId_user() + ", "
                + "id_cuenta=" + gastos.getId_cuenta() + ", "
                + "id_contacto=" + gastos.getId_contacto() + ", "
                + "descripcion='" + gastos.getDescripcion() + "', "
                + "total=" + gastos.getTotal() + ", "
                + "fecha=" + gastos.getFecha() + ", "
                + "hora='" + gastos.getHora() + "', "
                + "caja=" + gastos.getCaja()+ " "
                + "where id=" + gastos.getId();

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }
}

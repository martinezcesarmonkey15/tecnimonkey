/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.OtrosIngresos;

/**
 *
 * @author Monkeyelgrande
 */
public class DBOtrosIngresos {

    public int Guardar(OtrosIngresos ingresos) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO otros_ingresos (id,id_user,id_contacto,id_fondo,descripcion,total,fecha,hora) "
                + "VALUES (" + ingresos.getId() + "," + ingresos.getId_user() + "," + ingresos.getId_contacto() + "," + ingresos.getId_fondo()
                + ",'" + ingresos.getDescripcion() + "'," + ingresos.getTotal() + "," + ingresos.getFecha() + ",'" + ingresos.getHora() + "')";

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(OtrosIngresos ingresos) {
        int resultado = 0;
        Connection con = null;
        String SQL = "UPDATE otros_ingresos set "
                + "id_user=" + ingresos.getId_user() + ", "
                + "id_contacto=" + ingresos.getId_contacto() + ", "
                + "id_fondo=" + ingresos.getId_fondo()+ ", "
                + "descripcion='" + ingresos.getDescripcion() + "', "
                + "total=" + ingresos.getTotal() + ", "
                + "fecha=" + ingresos.getFecha() + ", "
                + "hora='" + ingresos.getHora() + "' "
                + "where id=" + ingresos.getId();

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }
}

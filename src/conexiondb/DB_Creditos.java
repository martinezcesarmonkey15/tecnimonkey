/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Creditos;

/**
 *
 * @author Monkeyelgrande
 */
public class DB_Creditos {

    public int Guardar(Creditos obj) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO creditos (id,id_user,id_servicio,total,descuento,fecha,hora) "
                + "VALUES (" + obj.getId() + "," + obj.getId_user() + "," + obj.getId_servicio()+ ","
                + obj.getTotal() + "," + obj.getDescuento() + ",'" + obj.getFecha() + "','" + obj.getHora() + "')";

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(Creditos obj) {
        int resultado = 0;
        Connection con = null;
        String SQL = "";
        SQL = "UPDATE creditos set "
                + "id_user=" + obj.getId_user() + ", "
                + "id_servicio=" + obj.getId_servicio()+ ", "
                + "total=" + obj.getTotal() + ", "
                + "descuento=" + obj.getTotal() + ", "
                + "fecha=" + obj.getFecha() + ", "
                + "hora='" + obj.getHora() + "' "
                + "where id=" + obj.getId();

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public void ActualizarDescuento(double descuento, String id) {
        Connection con = null;

        String SQL = "UPDATE creditos set "
                + "descuento=" + descuento + " "
                + "where id=" + id;

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Productos;

/**
 *
 * @author Monkeyelgrande
 */
public class DBproductos {

    public int Guardar(Productos producto) {
        int resultado = 0;
        Connection con = null;
        String SSQL = "INSERT INTO productos (id,codigo_barras,descripcion,precio_costo,precio_venta, servicio) "
                + "VALUES (?,?,?,?,?,?)";
        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SSQL);
            psql.setInt(1, producto.getId());
            psql.setString(2, producto.getCodigo_barras());
            psql.setString(3, producto.getDescripcion());
            psql.setDouble(4, producto.getPrecio_costo());
            psql.setDouble(5, producto.getPrecio_venta());
            psql.setDouble(6, producto.getServicio());

            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar(Productos producto) {
        int resultado = 0;
        Connection con = null;

        String SQL = "UPDATE productos set "
                + "codigo_barras='" + Metodos.metodos.ReemplazarCaracteres(producto.getCodigo_barras(), "'", "''") + "', "
                + "descripcion='" + Metodos.metodos.ReemplazarCaracteres(producto.getDescripcion(), "'", "''") + "', "
                + "precio_costo=" + producto.getPrecio_costo() + ", "
                + "precio_venta=" + producto.getPrecio_venta() + ", "
                + "servicio=" + producto.getServicio() + " "
                + "where id=" + producto.getId();
        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar actualizar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

}

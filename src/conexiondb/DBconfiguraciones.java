/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package conexiondb;

import java.sql.*;
import javax.swing.JOptionPane;
import modelos.Configuraciones;

/**
 *
 * @author Monkeyelgrande
 */
public class DBconfiguraciones {

    public int Guardar(Configuraciones config) {
        int resultado = 0;
        Connection con = null;
        String SQL = "";
        boolean flag = true;
        if (DB_consultas_R_D.consultarId("1", "configuraciones") == 1) {
            SQL = "UPDATE configuraciones set "
                    + "nombre_negocio='" + config.getNombre_negocio() + "', "
                    + "nit_negocio='" + config.getNit_negocio() + "', "
                    + "representante='" + config.getRepresentante()+ "', "
                    + "titulo_factura='" + config.getTitulo_factura()+ "', "
                    + "ciudad='" + config.getCiudad()+ "', "
                    + "eslogan='" + config.getEslogan()+ "', "
                    + "email='" + config.getEmail()+ "', "
                    + "pie_legal='" + config.getPie_legal()+ "', "
                    + "servicios='" + config.getServicios()+ "', "
                    + "contacto_negocio='" + config.getContacto_Negocio() + "', "
                    + "contacto2_negocio='" + config.getContacto2_Negocio() + "', "
                    + "direccion='" + config.getDireccion() + "', "
                    + "utilidad_venta=" + config.getUtilida_venta() + ", "
                    + "utilidad_mayorista=" + config.getUtilida_mayorista() + ", "
                    + "utilidad_credito=" + config.getUtilida_credito() + ", "
                    + "iva=" + config.getIva() + ", "
                    + "nombre_impresora='" + config.getNombre_impresota() + "', "
                    + "tipo_impresora='" + config.getTipo_impresora()+ "', "
                    + "ruta_imagenes='" + config.getRuta_imagenes()+ "', "
                    + "imprimir_factura=" + config.getImprimir_factura() + ", "
                    + "productos_repetidos=" + config.getProductos_repetidos() + " "
                    + "where id=1";
            flag = false;
        } else {
            SQL = "INSERT INTO configuraciones (nombre_negocio,nit_negocio,contacto_negocio,contacto2_negocio,utilidad_venta,"
                    + "utilidad_mayorista,utilidad_credito,iva,direccion,nombre_impresora,tipo_impresora,imprimir_factura,productos_repetidos,representante,ciudad,eslogan,"
                    + "email,pie_legal,servicios,ruta_imagenes) "
                    + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?,?,?,?,?,?,?,?,?,?)";
        }

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            if (flag) {
                psql.setString(1, config.getNombre_negocio());
                psql.setString(2, config.getNit_negocio());
                psql.setString(3, config.getContacto_Negocio());
                psql.setString(4, config.getContacto2_Negocio());
                psql.setInt(5, config.getUtilida_venta());
                psql.setInt(6, config.getUtilida_mayorista());
                psql.setInt(7, config.getUtilida_credito());
                psql.setInt(8, config.getIva());
                psql.setString(9, config.getDireccion());
                psql.setString(10, config.getNombre_impresota());
                psql.setString(11, config.getTipo_impresora());
                psql.setInt(12, config.getImprimir_factura());
                psql.setInt(13, config.getProductos_repetidos());
                psql.setString(14, config.getRepresentante());
                psql.setString(15, config.getCiudad());
                psql.setString(16, config.getEslogan());
                psql.setString(17, config.getEmail());
                psql.setString(18, config.getPie_legal());
                psql.setString(19, config.getServicios());
                psql.setString(20, config.getRuta_imagenes());
            }

            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

    public int Actualizar_Ruta(String ruta) {
        int resultado = 0;
        Connection con = null;
        String SQL = "";
        SQL = "UPDATE configuraciones set "
                + "ruta_backup='" + ruta + "' "
                + "where id=1";
        

        try {
            con = DB_consultas_R_D.getConexion();
            PreparedStatement psql = con.prepareStatement(SQL);
            resultado = psql.executeUpdate();
            psql.close();

        } catch (SQLException e) {

            JOptionPane.showMessageDialog(null, "Error al intentar almacenar la información:\n"
                    + e, "Error en la operación", JOptionPane.ERROR_MESSAGE);
        } finally {
            try {
                if (con != null) {
                    con.close();
                }
            } catch (SQLException ex) {
                JOptionPane.showMessageDialog(null, "Error al intentar cerrar la conexión:\n"
                        + ex, "Error en la operación", JOptionPane.ERROR_MESSAGE);
            }
        }
        return resultado;
    }

}

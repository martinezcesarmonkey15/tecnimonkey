
import javax.swing.JFrame;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;
import com.github.sarxos.webcam.WebcamResolution;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.JButton;
import javax.swing.JPanel;

public class WebcamPanelExample {

    public static void main(String[] args) throws InterruptedException {

        // Configurar la webcam
        Webcam webcam = Webcam.getDefault();
        webcam.setViewSize(WebcamResolution.VGA.getSize());

        // Crear el panel de la webcam
        WebcamPanel panel = new WebcamPanel(webcam);
        panel.setFPSDisplayed(true);
        panel.setDisplayDebugInfo(true);
        panel.setImageSizeDisplayed(true);

        // Crear el botón
        JButton btnCaptura = new JButton("Capturar");

        // Crear un panel contenedor para organizar los componentes
        JPanel contenedor = new JPanel();

        // Configurar el diseño del panel contenedor (FlowLayout)
        contenedor.setLayout(new java.awt.FlowLayout());

        // Agregar el panel de la webcam y el botón al panel contenedor
        contenedor.add(panel);
        contenedor.add(btnCaptura);

        // Configurar el JFrame
        JFrame window = new JFrame("Test webcam panel");
        window.add(contenedor); // Agregar el panel contenedor al JFrame
        window.setResizable(true);
        window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        window.pack();
        window.setVisible(true);

        // Manejar el evento de clic en el botón
        btnCaptura.addActionListener(e -> {
            // Capturar la imagen actual del panel de la cámara
            BufferedImage imagenCapturada = webcam.getImage();

            // Guardar la imagen en un archivo (puedes personalizar la ruta y el formato)
            String name = String.format("captura-%d.jpg", System.currentTimeMillis());

            File archivoImagen = new File(name);
            try {
                ImageIO.write(imagenCapturada, "PNG", archivoImagen);
                System.out.println("Imagen guardada exitosamente en: " + archivoImagen.getAbsolutePath());
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        });

    }
}

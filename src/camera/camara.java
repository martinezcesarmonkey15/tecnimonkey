/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package camera;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamResolution;
import java.awt.Dimension;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;

/**
 *
 * @author M-Work
 */
public class camara {

    public static void main(String[] args) throws IOException {

        Webcam webcam = Webcam.getDefault();

        for (Dimension supportedSize : webcam.getViewSizes()) {
            System.out.println(supportedSize.toString());
        }

        webcam.setViewSize(WebcamResolution.VGA.getSize());
        webcam.open();
        ImageIO.write(webcam.getImage(), "JPG", new File("firstCapture.jpg"));
        webcam.close();
    }
}
